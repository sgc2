#!/usr/bin/perl
#
# Convert an expanded praat script into a single string
# Output a stand alone Praat main program
#
# Use:
# perl praatscript2cprogram.pl script.praat > demoapplication.h
# 
print <<'ENDOFHEADER';
/* C const char string with Praat script
 * Generated automatically
 * For license, see Praat script text
 *
 */

static char32_t myDemoScript [] = U""
ENDOFHEADER
while(<>){
	# Remove comments
	next if /^\s*#/;
	# Remove logging/replay functionality (debugging code)
	next if /^\s*\'(sgc2.logging\$|replaySGC2Log\$)\'/;
	next if /index_regex\(replaySGC2Log\$/;

	# Protect special characters
	s/\\/\\\\/g;
	s/[\n\r\l]/\\n/g;
	s/\"/\\"/g;
	print "\"$_\"\n";
};
print <<'ENDOFFOOTER';
;

ENDOFFOOTER
