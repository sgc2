#!/opt/local/bin/perl

# Copy man page form to man page
`cp ../ManPages/What_s_new_in_SpeakGoodChinese_.txt ../ManPages/What_s_new_in_SpeakGoodChinese_.man`;
open(MANPAGE, ">>../ManPages/What_s_new_in_SpeakGoodChinese_.man");
open(GITLOG, "git log|" || die "git log: $!\n");
while(<GITLOG>) { 
	chomp;
	if(m/^Date\:\s*(.*) +/){
		$d = $1;};
		s/^\s+//g;
		if(s/Feature:\s*//g){
			print MANPAGE "<list_item> \"\\bu $_ ($d)\"\n"
		};
};
