#! praat
#
#
# Hum the correct tone tracks
#
# Needs
# include ToneScript.praat
#
procedure humToneContour humToneContour.pinyin$ humToneContour.register
	# Clean up input
	if humToneContour.pinyin$ <> ""
    	humToneContour.pinyin$ = replace_regex$(humToneContour.pinyin$, "^\s*(.+)\s*$", "\1", 1)
    	humToneContour.pinyin$ = replace_regex$(humToneContour.pinyin$, "5", "0", 0)
		call add_missing_neutral_tones 'humToneContour.pinyin$'
		humToneContour.pinyin$ = add_missing_neutral_tones.pinyin$
	endif

	# Generate reference example
	# Start with a range of 1 octave and a speed factor of 1
	humToneContour.toneRange = 1.0
	humToneContour.speedFactor = 1.0
	call toneScript 'humToneContour.pinyin$' 'humToneContour.register' 'humToneContour.toneRange' 'humToneContour.speedFactor' CorrectPitch

	# Hum Pitch track
	select Pitch 'humToneContour.pinyin$'
	Hum

	# Clean up
	select Pitch 'humToneContour.pinyin$'
	Remove
endproc



