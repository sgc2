#! praat
#
# Construct all tone patterns and look for the one closest to the given one
#

procedure FreeToneRecognition .pinyin$ .test_word$ .exclude$ .upperRegister .freqRange .durScale .startSyllable
    # Clean up input
    if .pinyin$ <> ""
        .pinyin$ = replace_regex$(.pinyin$, "^\s*(.+)\s*$", "\1", 1)
        .pinyin$ = replace_regex$(.pinyin$, "5", "0", 0)
		# Missing neutral tones
		call add_missing_neutral_tones '.pinyin$'
		.pinyin$ = add_missing_neutral_tones.pinyin$
    endif

    .referenceFrequency = 300
    .frequencyFactor = .referenceFrequency / .upperRegister

    .referenceExt$ = "pitch"
    
    # Bias Z-normalized value of the distance difference between smallest and correct
    .biasDistance = 1.1
    if index_regex(config.strict$, "[^0-9]") <= 0
		.tmp = 'config.strict$'
		# Be lenient with longer words
	    if .startSyllable > 0
			.tmp -= 1
	    endif
		if .tmp <= 0
			.biasDistance = 1.7
		elsif .tmp = 1
			.biasDistance = 1.1
		elsif .tmp = 2
			.biasDistance = 0.6
		elsif .tmp = 3
			.biasDistance = 0.3
		endif
    endif
    
    # Debugging
    .keepIntermediates = 0
    .debug = 0

    # Generate reference tones
    call toneScript '.pinyin$' '.upperRegister' '.freqRange' '.durScale' Pitch_'.startSyllable'

    # Convert input to Pitch
    if .test_word$ <> "" and .test_word$ <> "REUSEPITCH"
        Read from file... '.test_word$'
        Rename... Source
    endif
    if .test_word$ <> "REUSEPITCH"
        select Sound Source
		call convert2Pitch 'sgc_ToneProt.minimumPitch' 'sgc_ToneProt.maximumPitch'
		if index(.pinyin$, "3") > 0
			select convert2Pitch.object
			.sourcePitch = Kill octave jumps
			select convert2Pitch.object
			Remove
		else
			.sourcePitch = convert2Pitch.object
		endif
		select .sourcePitch
        Formula... self*'.frequencyFactor'; Normalize Pitch
        Rename... SourcePitch
    endif

    select Pitch SourcePitch

    .countDistance = 0
    .sumDistance = 0
    .sumSqrDistance = 0
    .correctDistance = -1

    .smallestDistance=999999
    sgc_ToneProt.choiceReference$ = "empty"
    select Table ToneList
    .listLength = Get number of rows
    for .i from 1 to .listLength
        select Table ToneList
        .inFile$ = Get value... '.i' Word
        # Broken third tones are still third tones
        # Creaky voice identifies 3rd tone
		.excludeWord$ = .exclude$
		if .excludeWord$ = "" and index_regex(toneProt.creakyThree$, "[^\.]")
			.tmpTones$ = replace_regex$(.inFile$, "[^\d]", "", 0)
			if index_regex(.tmpTones$, toneProt.creakyThree$) <= 0
				.excludeWord$ = .inFile$
			endif
		endif

        if (.excludeWord$ = "" or rindex_regex(.inFile$, .excludeWord$) <= 0) and rindex_regex(.inFile$, "[\d]") > 0
            referenceName$ = .inFile$
            select Pitch '.inFile$'
            plus Pitch SourcePitch
            .dtw = noprogress To DTW... 24 10 yes yes no restriction
            Rename... DTW'.inFile$'
            distance = Get distance (weighted)
            # Clean up
            select DTW DTW'.inFile$'
            .countDistance = .countDistance + 1
            .sumDistance = .sumDistance + distance
            .sumSqrDistance = .sumSqrDistance + distance^2
            
			.inFile$ = replace$(.inFile$, "9", "3", 0)
            if .pinyin$ = .inFile$
                .correctDistance = distance
            endif

            if .debug > 0
                # printline 'distance' - '.inFile$'
            endif

            if distance < .smallestDistance
				# You cannot have a large range and misidentify it as 00
				if not (.freqRange >= 0.5 and index_regex(.inFile$, "0[^0-9]+0") > 0 and index_regex(.test_word$, "0[^0-9]+0") <= 0)
	                .smallestDistance = distance
	                sgc_ToneProt.choiceReference$ = "'.inFile$'"
                endif
            endif


            if .keepIntermediates = 0
                Remove
            endif
        endif
    endfor
    
    if .countDistance > 1
        .meanDistance = .sumDistance / .countDistance
        .varDistance = (.sumSqrDistance - .sumDistance^2/.countDistance)/(.countDistance - 1)
        .stdDistance = sqrt(.varDistance)
        .diffDistance = .correctDistance - .smallestDistance
        .zDistance = .diffDistance/.stdDistance

        if .debug > 0
            printline Match: '.pinyin$' <== 'sgc_ToneProt.choiceReference$' small='.smallestDistance' Z='.zDistance'
        endif

        if .zDistance < .biasDistance
            sgc_ToneProt.choiceReference$ = .pinyin$
            .smallestDistance = .correctDistance
        endif
    endif
    

    # Clean up
    for .i from 1 to .listLength
        select Table ToneList
        .inFile$ = Get value... '.i' Word

        if (.exclude$ = "" or rindex_regex(.inFile$, .exclude$) <= 0) and rindex_regex(.inFile$, "[\d]") > 0 

            # Clean up
            select Pitch '.inFile$'
            if .keepIntermediates = 0
                Remove
            endif
        endif
    endfor

    select Table ToneList
    if .test_word$ <> "" and .test_word$ <> "REUSEPITCH"
        plus Sound Source
    endif
    if .test_word$ <> "" and .test_word$ <> "REUSEPITCH"
        plus Pitch SourcePitch
    endif
    if .keepIntermediates = 0
        Remove
    endif

endproc

