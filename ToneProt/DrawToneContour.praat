#! praat
#
#
# Draw the correct tone tracks
#
# Needs
# include ToneScript.praat
#
procedure drawToneContour drawToneContour.pinyin$ drawToneContour.register
	# Clean up input
	if drawToneContour.pinyin$ <> ""
    	drawToneContour.pinyin$ = replace_regex$(drawToneContour.pinyin$, "^\s*(.+)\s*$", "\1", 1)
    	drawToneContour.pinyin$ = replace_regex$(drawToneContour.pinyin$, "5", "0", 0)

		call add_missing_neutral_tones 'drawToneContour.pinyin$'
		drawToneContour.pinyin$ = add_missing_neutral_tones.pinyin$
	endif

	# Generate reference example
	# Start with a range of 1 octave and a speed factor of 1
	drawToneContour.toneRange = 1.0
	drawToneContour.speedFactor = 1.0
	call toneScript 'drawToneContour.pinyin$' 'drawToneContour.register' 1 1 CorrectPitch
	drawToneContour.freqTop = 1.5 * drawToneContour.register

	# Draw Pitch track
	select Pitch 'drawToneContour.pinyin$'
	demo Select outer viewport... 20 80 40 100
	demo Axes... 0 100 0 100
	demo Line width... 1
	demo Green
	demo Draw... 0 0 0 'drawToneContour.freqTop' 0
	demo Line width... 3
	demo Green
	demo Draw... 0 0 0 'drawToneContour.freqTop' 0
	demo Select outer viewport... 0 100 0 100
	demo Axes... 0 100 0 100

	# Clean up
	select Pitch 'drawToneContour.pinyin$'
	Remove
endproc


procedure drawSourceToneContour drawToneContour.sourcePitch
	if drawToneContour.sourcePitch > 0
		call reset_viewport
		demo Select inner viewport... 20 80 40 100
		demo Axes... 0 100 0 100
	
	    select drawToneContour.sourcePitch
	    demo Red
	    demo Line width... 3
	    demo Draw... 0 0 0 'freqTop' 0
	
	    demo Line width... 1
		call reset_viewport
	endif
endproc
