#! praat
#
#     SpeakGoodChinese: SGC_ToneRecognizer.praat processes student utterances 
#     and generates a report on their tone production
#     
#     Copyright (C) 2007-2010  R.J.J.H. van Son
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Koning, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
# Needs:
# include ToneRecognition.praat
# include ToneScript.praat
# procedure loadTable
#
procedure sgc_ToneProt sgc_ToneProt.currentSound$ sgc_ToneProt.pinyin$ sgc_ToneProt.register sgc_ToneProt.proficiency sgc_ToneProt.language$
	# Remove if included in main program!
	sgc_ToneProt.viewportMargin = 5

 	sgc_ToneProt.precision = 3
	if sgc_ToneProt.proficiency
		sgc_ToneProt.precision = 1.5
	endif
	# Stick to the raw recognition results or not
	sgc_ToneProt.ultraStrict = sgc_ToneProt.proficiency
	
	# Read and select the feedbacktext
	call testLoadTable ToneFeedback_'sgc_ToneProt.language$'
	if testLoadTable.table > 0
		call loadTable ToneFeedback_'sgc_ToneProt.language$'
	else
		call loadTable ToneFeedback_EN
	endif
	Rename... ToneFeedback
	numberOfFeedbackRows = Get number of rows

	# Clean up input
	if sgc_ToneProt.pinyin$ <> ""		
    	sgc_ToneProt.pinyin$ = replace_regex$(sgc_ToneProt.pinyin$, "^\s*(.+)\s*$", "\1", 1)
    	sgc_ToneProt.pinyin$ = replace_regex$(sgc_ToneProt.pinyin$, "5", "0", 0)
		# Missing neutral tones
		call add_missing_neutral_tones 'sgc_ToneProt.pinyin$'
		sgc_ToneProt.pinyin$ = add_missing_neutral_tones.pinyin$
	endif

	# Reduction (lower sgc_ToneProt.register and narrow range) means errors
	# The oposite mostly not. Asymmetry alows more room upward
	# than downward (asymmetry = 2 => highBoundaryFactor ^ 2)
	asymmetry = 2

	# Kill octave jumps: DANGEROUS
	killOctaveJumps = 0

	# Limit pitch range
	sgc_ToneProt.minimumPitch = 50
	sgc_ToneProt.maximumPitch = 500
	if sgc_ToneProt.register > 400
    	sgc_ToneProt.minimumPitch = 60
    	sgc_ToneProt.maximumPitch = 600
	elsif sgc_ToneProt.register > 250
    	sgc_ToneProt.minimumPitch = 50
    	sgc_ToneProt.maximumPitch = 500
	else
    	sgc_ToneProt.minimumPitch = 40
    	sgc_ToneProt.maximumPitch = 400
	endif

	sgc_ToneProt.currentTestWord$ = sgc_ToneProt.pinyin$
	spacing = 0.5
	.interPrecision = sgc_ToneProt.precision;
	
	# Allow more "room" when there is a 3rd tone
	if index(sgc_ToneProt.pinyin$, "3")
		.interPrecision *= 4/3 
	endif
	sgc_ToneProt.precisionFactor = 2^(.interPrecision/12)
	highBoundaryFactor = sgc_ToneProt.precisionFactor ^ asymmetry
	lowBoundaryFactor = 1/sgc_ToneProt.precisionFactor

	# Generate reference example
	# Start with a range of 1 octave and a speed factor of 1
	toneRange = 1.0
	speedFactor = 1.0
	sgc_ToneProt.upperRegisterInput = sgc_ToneProt.register
	call toneScript 'sgc_ToneProt.currentTestWord$' 'sgc_ToneProt.upperRegisterInput' 1 1 CorrectPitch
	# Get range and top
	select Pitch 'sgc_ToneProt.currentTestWord$'
	sgc_ToneProt.durationModel = Get total duration
	maximumModelFzero = Get quantile... 0 0 0.95 Hertz
	minimumModelFzero = Get quantile... 0 0 0.05 Hertz
	if maximumModelFzero = undefined
		maximumModelFzero = 0
	endif
	if minimumModelFzero = undefined
		minimumModelFzero = 0
	endif
	sgc_ToneProt.modelPitchRange = 2
	if minimumModelFzero > 0
    	sgc_ToneProt.modelPitchRange = maximumModelFzero / minimumModelFzero
    else
		sgc_ToneProt.modelPitchRange = 0
	endif

	# Get the sounds
	if fileReadable(sgc_ToneProt.currentSound$)
    	Read from file... 'sgc_ToneProt.currentSound$'
    	Rename... Source
	else
    	select Sound 'sgc_ToneProt.currentSound$'
    	Copy... Source
	endif

	# Calculate pitch
	select Sound Source
	durationSource = Get total duration
	call convert2Pitch 'sgc_ToneProt.minimumPitch' 'sgc_ToneProt.maximumPitch'
	te.recordedPitch = convert2Pitch.object
	select te.recordedPitch
	Rename... SourcePitch
	
	################################################################
	#
	# If there is a third tone, replace creaky voice by low pitch
	# 
	################################################################
	toneProt.creakyThree$ = "" 
	for .s to toneScript.syllableCount
		toneProt.creakyThree$ = toneProt.creakyThree$ + "."
	endfor
	if index(sgc_ToneProt.pinyin$, "3")
		.creakCO = 0.025
		.delta = 0.0000001
		
		# Lowest point for tone 3 is 1 octave and 3st below the top line
		# The cut-off is 0,05 quantile of model + 1/2 semitones
		select te.recordedPitch
		.recordedPitchTier = Down to PitchTier
		
		# Calculate Shimmer&Jitter
		select Sound Source
		.sound = selected("Sound")
		call createJitterShimmerContour .sound
		.jitterShimmer = selected("Sound")
		
		# Determine the low F0 part of the 3rd tone
		# Generate word with voiceless low 3rd tone
		.newPinyin$ = replace$(sgc_ToneProt.pinyin$, "3", "9", 0)
		call generateWord Pitch '.newPinyin$' 'sgc_ToneProt.register'
		select Pitch '.newPinyin$'
		.generatedWord9 = selected("Pitch")
		
		select Pitch 'sgc_ToneProt.pinyin$'
		.generatedWord3 = selected("Pitch")
		
		# Create a tier with low part of the third tone as intervals
		call generateWord TextGrid 'sgc_ToneProt.pinyin$' 'sgc_ToneProt.register'
		select TextGrid 'sgc_ToneProt.pinyin$'
		.pitchTextGrid = selected("TextGrid")
		.lowPresent = Count intervals where: 1, "is equal to", "3"
				
		# DTW of .generatedWord9 with sourcePitch
		if .lowPresent > 0
			select te.recordedPitch
			plus .generatedWord3
			.dtw3 = noprogress To DTW... 24 10 yes yes no restriction
			Rename... DTW'sgc_ToneProt.pinyin$'
			.distance3 = Get distance (weighted)
			select te.recordedPitch
			plus .generatedWord9
			.dtw9 = noprogress To DTW... 24 10 yes yes no restriction
			Rename... DTW'.newPinyin$'
			.distance9 = Get distance (weighted)
			
			select .pitchTextGrid
			if .distance3 <= .distance9
				plus .dtw3
			else
				plus .dtw9
			endif
			.origTextGrid = To TextGrid (warp times)
			.numPitchIntervals = Get number of intervals: 1
			.syllableNum = 0
			for .i to .numPitchIntervals
				select .origTextGrid
				.label$ = Get label of interval: 1, .i
				if index_regex(.label$, "[0-9]")
					.syllableNum += 1
				endif
				if .label$ = "3"
					select .origTextGrid
					.threeStart = Get starting point: 1, .i
					.threeEnd = Get end point: 1, .i
					.lowStart = .threeStart + (.threeEnd - .threeStart)/3
					.lowEnd = .threeStart + 5*(.threeEnd - .threeStart)/6
					select .jitterShimmer
					.mean = Get mean: 0, .lowStart, .lowEnd
					if .mean > .creakCO
						toneProt.creakyThree$ = left$(toneProt.creakyThree$, .syllableNum-1) + "3" + right$(toneProt.creakyThree$, toneScript.syllableCount-.syllableNum);
					endif
				endif
			endfor
			select .dtw3
			plus .dtw9
			Remove
		endif
		# Clean up
		select .jitterShimmer
		plus .pitchTextGrid
		plus .recordedPitchTier
		plus .generatedWord9
		Remove
	endif

	# Remove all pitch points outside a band around the upper sgc_ToneProt.register
	select te.recordedPitch
	upperCutOff = 1.7*sgc_ToneProt.upperRegisterInput
	lowerCutOff = sgc_ToneProt.upperRegisterInput/4
	Formula... if self > 'upperCutOff' then -1 else self endif
	Formula... if self < 'lowerCutOff' then -1 else self endif

	# Get range and top
	select te.recordedPitch
	maximumRecFzero = Get quantile... 0 0 0.95 Hertz
	timeMaximum = Get time of maximum... 0 0 Hertz Parabolic
	minimumRecFzero = Get quantile... 0 0 0.05 Hertz
	timeMinimum = Get time of minimum... 0 0 Hertz Parabolic
	if maximumRecFzero = undefined
    	# Determine what should be told to the student
    	.recognitionText$ =  "'sgc_ToneProt.currentTestWord$': ???"
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback
			.label$ = "Unknown"

        	if .toneOne$ = "NoSound"
            	.feedbackText$ = .toneText$
        	endif
    	endfor

    	#exit Error, nothing recorded
		goto END
	endif
	recPitchRange = 2
	if minimumRecFzero > 0
	   recPitchRange = maximumRecFzero / minimumRecFzero
	endif
	sgc_ToneProt.newUpperRegister = maximumRecFzero / maximumModelFzero * sgc_ToneProt.upperRegisterInput
	sgc_ToneProt.newToneRange = recPitchRange / sgc_ToneProt.modelPitchRange
	if sgc_ToneProt.newUpperRegister = undefined
		sgc_ToneProt.newUpperRegister = sgc_ToneProt.upperRegisterInput
	endif
	if sgc_ToneProt.newToneRange = undefined
		sgc_ToneProt.newToneRange = 1
	endif

	sgc_ToneProt.registerUsed$ = "OK"
	rangeUsed$ = "OK"
	# Advanced speakers must not speak too High, or too "Dramatic"
	# Beginning speakers also not too Low or too Narrow ranges
	if sgc_ToneProt.newUpperRegister > highBoundaryFactor * sgc_ToneProt.upperRegisterInput
	   sgc_ToneProt.newUpperRegister = highBoundaryFactor * sgc_ToneProt.upperRegisterInput
	   sgc_ToneProt.registerUsed$ = "High"
	elsif not sgc_ToneProt.proficiency and sgc_ToneProt.newUpperRegister < lowBoundaryFactor * sgc_ToneProt.upperRegisterInput
	   sgc_ToneProt.newUpperRegister = lowBoundaryFactor * sgc_ToneProt.upperRegisterInput
	   sgc_ToneProt.registerUsed$ = "Low"
	endif
	
	if sgc_ToneProt.newToneRange > highBoundaryFactor
	   sgc_ToneProt.newToneRange = highBoundaryFactor
	   rangeUsed$ = "Wide"
	elsif not sgc_ToneProt.proficiency and sgc_ToneProt.newToneRange < lowBoundaryFactor and not sgc_ToneProt.proficiency
		# Don't do this for advanced speakers
	   sgc_ToneProt.newToneRange = lowBoundaryFactor
	   rangeUsed$ = "Narrow"
	endif

	# Duration 
	if sgc_ToneProt.durationModel > spacing
	   speedFactor = (durationSource - spacing) / (sgc_ToneProt.durationModel - spacing)
	endif

	# Round values
	sgc_ToneProt.newUpperRegister = round(sgc_ToneProt.newUpperRegister)

	# Remove all pitch points outside a band around the upper sgc_ToneProt.register
	select te.recordedPitch
	upperCutOff = 1.5*sgc_ToneProt.newUpperRegister
	lowerCutOff = sgc_ToneProt.newUpperRegister/3
	Formula... if self > 'upperCutOff' then -1 else self endif
	Formula... if self < 'lowerCutOff' then -1 else self endif

	# It is rather dangerous to kill Octave errors, so be careful
	if killOctaveJumps > 0
		select te.recordedPitch
    	Rename... OldSourcePitch
    	Kill octave jumps
    	Rename... SourcePitch
    	te.recordedPitch = selected("Pitch")
    	select Pitch OldSourcePitch
    	Remove
	endif

	# It is good to have the lowest and highest pitch frequencies
	select te.recordedPitch
	timeMaximum = Get time of maximum... 0 0 Hertz Parabolic
	timeMinimum = Get time of minimum... 0 0 Hertz Parabolic

	# Clean up the old example pitch
	select Pitch 'sgc_ToneProt.currentTestWord$'
	Remove

	# Do the tone recognition
	.numSyllables = toneScript.syllableCount
	sgc_ToneProt.choiceReference$ = sgc_ToneProt.currentTestWord$
	.skipSyllables = 0
	while sgc_ToneProt.choiceReference$ = sgc_ToneProt.currentTestWord$ and .skipSyllables < .numSyllables
		call FreeToneRecognition 'sgc_ToneProt.choiceReference$' "REUSEPITCH" "" 'sgc_ToneProt.newUpperRegister' 'sgc_ToneProt.newToneRange' 'speedFactor' '.skipSyllables'
		.skipSyllables += 1
	endwhile
	call toneScript 'sgc_ToneProt.currentTestWord$' 'sgc_ToneProt.upperRegisterInput' 'sgc_ToneProt.newToneRange' 'speedFactor' CorrectPitch
	# Special cases
	originalRecognizedWord$ = sgc_ToneProt.choiceReference$
	if  sgc_ToneProt.ultraStrict = 0
    	# [23]3 is often misidentified as 23, 20 or 30
    	if rindex_regex(sgc_ToneProt.currentTestWord$, "[23][^0-9]+3") > 0
	    	if rindex_regex(sgc_ToneProt.currentTestWord$, "3[^0-9]+3") > 0
				.c = rindex_regex(sgc_ToneProt.currentTestWord$, "3[^0-9]+3") - 1
	        	if rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})[23][^0-9]+[023]") > 0
	            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^(.{'.c'})[23]([^0-9]+)[023]", "\13\23", 1)
	        	endif
	        endif
	    	if rindex_regex(sgc_ToneProt.currentTestWord$, "2[^0-9]+3") > 0
				.c = rindex_regex(sgc_ToneProt.currentTestWord$, "2[^0-9]+3") - 1
	        	if rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})[23][^0-9]+[023]") > 0
	            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "([^0-9]+)[23]([^0-9]+)[023]", "\12\23", 1)
	        	endif
	        endif
	    endif
	    
    	# First syllable: 2<->3 exchanges
    	if rindex_regex(sgc_ToneProt.currentTestWord$, "^[^0-9]+2") > 0
        	if rindex_regex(sgc_ToneProt.choiceReference$, "^[^0-9]+3") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^([^0-9]+)[36]", "\12", 0)
        	endif
    	elsif rindex_regex(sgc_ToneProt.currentTestWord$, "^[^0-9]+3") > 0
        	if rindex_regex(sgc_ToneProt.choiceReference$, "^[^0-9]+2") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^([^0-9]+)2", "\13", 0)
        	endif
    	# A single second tone is often misidentified as a neutral tone, 
    	# A real neutral tone would be too low or too narrow and be discarded
    	elsif rindex_regex(sgc_ToneProt.currentTestWord$, "^[^0-9]+2$") > 0
        	if rindex_regex(sgc_ToneProt.choiceReference$, "^[^0-9]+0$") > 0 and timeMinimum < timeMaximum
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "0", "2", 0)
        	endif
    	# A single fourth tone is often misidentified as a neutral tone, 
    	# A real neutral tone would be too low or too narrow and be discarded
    	elsif rindex_regex(sgc_ToneProt.currentTestWord$, "^[^0-9]+4$") > 0
        	if rindex_regex(sgc_ToneProt.choiceReference$, "^[^0-9]+0$") > 0 and timeMaximum < timeMinimum
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "0", "4", 0)
        	endif
    	endif

    	# 32 <-> 30 Sometimes this is recognized as 00
    	# A recognized 0/2 after a 3 can be a 2/0
    	if rindex_regex(sgc_ToneProt.currentTestWord$, "3[^0-9]+2") > 0
			.c = rindex_regex(sgc_ToneProt.currentTestWord$, "3[^0-9]+2") - 1
			if rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})3[^0-9]+0") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^(.{'.c'}3[^0-9]+)0", "\12", 0)
			elsif rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})0[^0-9]+0") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^(.{'.c'})0([^0-9]+)0", "\13\22", 0)
        	endif
    	elsif rindex_regex(sgc_ToneProt.currentTestWord$, "3[^0-9]+0") > 0
			.c = rindex_regex(sgc_ToneProt.currentTestWord$, "3[^0-9]+0") - 1
			if rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})3[^0-9]+2") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^(.{'.c'}3[^0-9]+)2", "\10", 0)
 			elsif rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})0[^0-9]+0") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^(.{'.c'})0([^0-9]+)0", "\13\20", 0)
			endif
    	endif
    	
     	# 31 <-> 30 
    	# A recognized 0 after a 3 can be a 1
    	if rindex_regex(sgc_ToneProt.currentTestWord$, "3[^0-9]+1") > 0
			.c = rindex_regex(sgc_ToneProt.currentTestWord$, "3[^0-9]+1") - 1
			if rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})3[^0-9]+0") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^(.{'.c'}3[^0-9]+)0", "\11", 0)
        	endif
		endif
    	
    	# 40 <-> 42
    	# A recognized 0 after a 4 can be a 2: 4-0 => 4-2
    	if rindex_regex(sgc_ToneProt.currentTestWord$, "4[^0-9]+2") > 0
			.c = rindex_regex(sgc_ToneProt.currentTestWord$, "4[^0-9]+2") - 1
			if rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})4[^0-9]+0") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^(.{'.c'}4[^0-9]+)0", "\12", 0)
        	endif
    	endif
    	
		# 404 <-> 414
    	# A recognized 0 between two tones 4 can be a 1
    	if rindex_regex(sgc_ToneProt.currentTestWord$, "4[^0-9]+1[^0-9]+4") > 0
			.c = rindex_regex(sgc_ToneProt.currentTestWord$, "4[^0-9]+1[^0-9]+4") - 1
			if rindex_regex(sgc_ToneProt.choiceReference$, "^(.{'.c'})4[^0-9]+0[^0-9]+4") > 0
            	sgc_ToneProt.choiceReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "^(.{'.c'}4[^0-9]+)0([^0-9]+4)", "\11\2", 0)
        	endif
    	endif
    	
	endif
	
	# If wrong, then undo all changes
	if sgc_ToneProt.currentTestWord$ != sgc_ToneProt.choiceReference$
    	sgc_ToneProt.choiceReference$ = originalRecognizedWord$
	endif

	sgc_ToneProt.toneChoiceReference$ = sgc_ToneProt.choiceReference$

	###############################################
	#
	# Report
	#
	###############################################
	result$ = "'tab$''sgc_ToneProt.currentTestWord$''tab$''sgc_ToneProt.choiceReference$''tab$''sgc_ToneProt.newUpperRegister''tab$''sgc_ToneProt.newToneRange''tab$''speedFactor''tab$''sgc_ToneProt.registerUsed$''tab$''rangeUsed$'"
	if sgc_ToneProt.currentTestWord$ = sgc_ToneProt.toneChoiceReference$
	   result$ = "Correct:"+result$
	else
	   result$ = "Wrong:"+result$
	endif

	# Initialize result texts
	.recognitionText$ =  "'sgc_ToneProt.currentTestWord$': "
	.choiceText$ = replace_regex$(sgc_ToneProt.choiceReference$, "6", "\?", 0)
	.feedbackText$ = "----"

	# Separate tone from pronunciation errors
	currentToneWord$ = replace_regex$(sgc_ToneProt.currentTestWord$, "[a-z]+", "\*", 0)
	choiceToneReference$ = replace_regex$(sgc_ToneProt.choiceReference$, "[a-z]+", "\*", 0)

	# Determine what should be told to the student
	if sgc_ToneProt.registerUsed$ = "Low"
    	.recognitionText$ = .recognitionText$ + "???"
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback

        	if .toneOne$ = "Low"
            	.feedbackText$ = .toneText$
				.label$ = .toneOne$
        	endif
    	endfor
	elsif rangeUsed$ = "Narrow"
    	.recognitionText$ = .recognitionText$ + "???"
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback

        	if .toneOne$ = "Narrow"
            	.feedbackText$ = .toneText$
				.label$ = .toneOne$
        	endif
    	endfor
	elsif sgc_ToneProt.registerUsed$ = "High"
    	.recognitionText$ = .recognitionText$ + .choiceText$
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback

        	if .toneOne$ = "High"
            	.feedbackText$ = .toneText$
				.label$ = .toneOne$
        	endif
    	endfor
	elsif rangeUsed$ = "Wide"
    	.recognitionText$ = .recognitionText$ + .choiceText$
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback

        	if .toneOne$ = "Wide"
            	.feedbackText$ = .toneText$
				.label$ = .toneOne$
        	endif
    	endfor
	# Bad tones, first handle first syllable
	elsif rindex_regex(sgc_ToneProt.choiceReference$, "^[a-zA-Z]+6") > 0
    	.recognitionText$ = .recognitionText$ + .choiceText$
    	# First syllable
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback

        	# 
        	.feedbackText$ = ""
        	if .toneOne$ = "6"
            	.recognitionText$ = .recognitionText$ + " ('.toneText$')"
				.label$ = .toneOne$
        	elsif rindex_regex(sgc_ToneProt.currentTestWord$, "^[a-zA-Z]+'.toneOne$'") > 0 and .toneTwo$ = "-"
            	.feedbackText$ = .feedbackText$ + .toneText$ + " "
        	endif
    	endfor
	# Bad tones, then handle second syllable
	elsif rindex_regex(sgc_ToneProt.choiceReference$, "[a-zA-Z]+6$") > 0
    	.recognitionText$ = .recognitionText$ + .choiceText$
    	# Last syllable
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback

        	# 
        	.feedbackText$ = ""
        	if .toneOne$ = "6"
            	.recognitionText$ = .recognitionText$ + " ('.toneText$')"
				.label$ = .toneOne$
        	elsif rindex_regex(sgc_ToneProt.currentTestWord$, "[a-zA-Z]+'.toneOne$'$") > 0 and .toneTwo$ = "-"
            	.feedbackText$ = .feedbackText$ + .toneText$ + " "
        	endif
    	endfor
	# Just plain wrong tones
	elsif currentToneWord$ <> choiceToneReference$
    	.recognitionText$ = .recognitionText$ + .choiceText$
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback

        	if rindex_regex(sgc_ToneProt.currentTestWord$, "^[a-zA-Z]+'.toneOne$'$") > 0 and .toneTwo$ = "-"
            	.feedbackText$ = .toneText$
        	elsif rindex_regex(sgc_ToneProt.currentTestWord$, "^[a-zA-Z]+'.toneOne$'[a-zA-Z]+'.toneTwo$'$") > 0
            	.feedbackText$ = .toneText$
        	elsif .toneOne$ = "Wrong"
            	.recognitionText$ = .recognitionText$ + " ('.toneText$')"
				.label$ = .toneOne$
        	endif
    	endfor
	# Correct
	else
    	.recognitionText$ = .recognitionText$ + .choiceText$
    	for i from 1 to numberOfFeedbackRows
        	select Table ToneFeedback
        	.toneOne$ = Get value... 'i' T1
        	.toneTwo$ = Get value... 'i' T2
        	.toneText$ = Get value... 'i' Feedback

        	if .toneOne$ = "Correct"
            	.feedbackText$ = .toneText$
				.label$ = .toneOne$
        	endif
    	endfor
	endif

	label END

	# Write out result
	Create Table with column names... Feedback 3 Text
	Set string value... 1 Text '.recognitionText$'
	Set string value... 2 Text '.feedbackText$'
	Set string value... 3 Text '.label$'

	# Clean up
	select Table ToneFeedback
	Remove

	# Show pitch tracks
    freqTop = 1.5 * sgc_ToneProt.upperRegisterInput

	# Replace recorded sound with new sound
	if not fileReadable(sgc_ToneProt.currentSound$)
    	select Sound 'sgc_ToneProt.currentSound$'
		Remove
		select Sound Source
    	Copy... 'sgc_ToneProt.currentSound$'
	endif


	# Clean up
	select Sound Source
	plus Pitch 'sgc_ToneProt.currentTestWord$'
	Remove
endproc


# Use ~0.03 cutoff
procedure createJitterShimmerContour .sound
	select .sound
	.duration = Get total duration
	.jitshimSound = Create Sound: "JitterShimmer", 0, .duration, 100, "0"
	
	# Create pointprocess
	select .sound
	.pointProc = noprogress To PointProcess (periodic, cc): 60, 350
	
	.dT = 0.01
	.w_half = 0.075/2
	.t = 0.0
	.sampleNum = 1
	while .t < .duration - .dT
		select .pointProc
		.jitter = Get jitter (local): .t-.w_half, .t+.w_half, 0.0001, 0.02, 1.3
		if .jitter = undefined
			.jitter = 0
		endif
		select .sound
		plus .pointProc
		.shimmer = Get shimmer (local): .t-.w_half, .t+.w_half, 0.0001, 0.02, 1.3, 1.6
		if .shimmer = undefined
			.shimmer = 0
		endif
		select .jitshimSound
		Set value at sample number: 0, .sampleNum, 10 * .jitter * .shimmer
	
		.t += .dT
		.sampleNum += 1
	endwhile
	
	select .pointProc
	Remove
	
	select .jitshimSound
endproc

procedure createShimmerCPPcontour .sound
	call createCPPContour .sound
	.cpp = selected("Sound")
	
	select .sound
	.duration = Get total duration
	.shimmerCPPSound = Create Sound: "ShimmerCPP", 0, .duration, 100, "0"
	
	# Create pointprocess
	select .sound
	.pointProc = noprogress To PointProcess (periodic, cc): 60, 350
	
	.dT = 0.01
	.w_half = 0.075/2
	.t = 0.0
	.sampleNum = 1
	while .t < .duration - .dT
		select .sound
		plus .pointProc
		.shimmer = Get shimmer (local): .t-.w_half, .t+.w_half, 0.0001, 0.02, 1.3, 1.6
		if .shimmer = undefined
			.shimmer = 0
		endif
		select .cpp
		.cppValue = Get value at time: 0, .t, "Sinc70"
		if .cppValue = undefined or .cppValue < 1
			.cppValue = 1
		endif
		select .shimmerCPPSound
		Set value at sample number: 0, .sampleNum, 10* .shimmer / .cppValue
	
		.t += .dT
		.sampleNum += 1
	endwhile
	
	select .pointProc
	plus .cpp
	Remove
	
	select .shimmerCPPSound
	
endproc

# Use ~? cutoff
procedure createShimmerContour .sound
	select .sound
	.duration = Get total duration
	.shimmerSound = Create Sound: "Shimmer", 0, .duration, 100, "0"
	
	# Create pointprocess
	select .sound
	.pointProc = To PointProcess (periodic, cc): 60, 350
	
	.dT = 0.01
	.w_half = 0.075/2
	.t = 0.0
	.sampleNum = 1
	while .t < .duration - .dT
		select .sound
		plus .pointProc
		.shimmer = Get shimmer (local): .t-.w_half, .t+.w_half, 0.0001, 0.02, 1.3, 1.6
		if .shimmer = undefined
			.shimmer = 0
		endif
		select .shimmerSound
		Set value at sample number: 0, .sampleNum, .shimmer
	
		.t += .dT
		.sampleNum += 1
	endwhile
	
	select .pointProc
	Remove
	
	select .shimmerSound
endproc


# Use ~? cutoff
procedure createCPPContour .sound
	select .sound
	.dT = 0.01
	
	# Create pointprocess
	select .sound
	.duration = Get total duration
	.pcg = To PowerCepstrogram: 60, .dT, 5000, 50
	.cppTable = To Table (peak prominence): 60, 330, 0.05, "Parabolic", 0.001, 0, "Exponential decay", "Robust"
	.nRows = Get number of rows
	.lastTime = Get value: .nRows, "time"
	.lastTime += .dT
	while .lastTime < .duration - .dT
		Append row
		.nRows += 1
		Set numeric value: .nRows, "cpp", 0
		.lastTime += .dT
	endwhile
	.firstTime = Get value: 1, "time"
	.firstTime -= .dT
	while .firstTime >= .dT/2
		Insert row: 1
		Set numeric value: 1, "cpp", 0
		.firstTime -= .dT
	endwhile
	Remove column: "quefrency"
	Remove column: "f0"
	Remove column: "rnr"
	Remove column: "time"
	.cppTable2 = Transpose
	.cppMatrix = Down to Matrix
	.cppSound = To Sound (slice): 1
	Override sampling frequency: 1/.dT
	Shift times to: "start time", 0
	Rename: "CPP"
	
	select .pcg
	plus .cppTable
	plus .cppTable2
	plus .cppMatrix
	Remove
	
	select .cppSound
endproc


