# The rules for constructing the tone contours.
#
# This procedure works because in Praat, there is no namespace
# separation. All variables behave as if they are global
# (which they are).
#
# toneScript.toneSyllable is the tone number on the current syllable
# 1-4, 0=neutral, 6=Dutch (garbage) intonation, 9=3, but with an unvoiced low segment
#
# These procedures set the following pareameters
# and use them to create a Pitch Tier:
#
# toneScript.toneFactor:  Duration scale factor for current tone
# toneRules.startPoint:  Start of the tone
# toneScript.endPoint:    end of the tone
# toneRules.lowestPoint: bottom of tone 3
# toneScript.point: The time of the next pitch value in the contour
#        ONLY USE AS: toneScript.point = toneScript.point + <fraction> * toneScript.voicedDuration
#
# The following values are given by the calling routine
# DO NOT ALTER THEM
# 
# toneScript.toneSyllable: tone number on the current syllable
# toneScript.nextTone: tone number of next syllable or -1
# toneScript.prevTone: tone number of previous syllable or -1
# toneScript.lastFrequency: end-frequency of the previous syllable
#
# toneScript.topLine: the frequency of the first tone
# toneScript.frequency_Range: Range of tone four (1 toneScript.octave)
# toneScript.voicedDuration: Duration of voiced part of syllable
# 

# Procedure to scale the duration of the current syllable
procedure toneDuration
	if toneScript.toneSyllable = 0
		.zeroToneFactor = 0.5
        if toneScript.prevTone = 2
            .zeroToneFactor = 0.8 * .zeroToneFactor
        elsif toneScript.prevTone = 3 or toneScript.prevTone = 9
            .zeroToneFactor = 1.1 * .zeroToneFactor
        elsif toneScript.prevTone = 4
            .zeroToneFactor = 0.8 * .zeroToneFactor
        endif
        toneScript.toneFactor = .zeroToneFactor * toneScript.toneFactor
	elsif toneScript.toneSyllable = 2
		toneScript.toneFactor = 0.8
	elsif toneScript.toneSyllable = 3 or toneScript.toneSyllable = 9
		toneScript.toneFactor = 1.1
    elsif toneScript.toneSyllable = 4
		toneScript.toneFactor = 0.8
	endif
    
	# Next tone 0, then lengthen first syllable
	if toneScript.nextTone = 0
		toneScript.toneFactor = toneScript.toneFactor * 1.2
	endif
endproc

# DO NOT CHANGE toneScript.toneFactor BELOW THIS POINT

# Rules to create a tone
#
# Do not mess with the 'Add point...' commands
# unless you know what you are doing
# The 'toneScript.point =' defines the time of the next pitch value
#
# start * ?Semit is a fall
# start / ?Semit is a rise
#
procedure toneRules
    #
    # Tone toneRules.levels 1-5
    # Defined relative to the topline and the frequency range
    toneRules.levelFive = toneScript.topLine
    toneRules.levelOne = toneScript.topLine * toneScript.frequency_Range
    toneRules.levelThree = toneScript.topLine * sqrt(toneScript.frequency_Range)
    toneRules.levelTwo = toneScript.topLine * sqrt(sqrt(toneScript.frequency_Range))
    toneRules.levelFour = toneRules.levelOne / sqrt(sqrt(toneScript.frequency_Range))

    # First tone
	if toneScript.toneSyllable = 1
        # Just a straight line
        toneRules.startPoint = toneRules.levelFive
        toneScript.endPoint = toneRules.levelFive
        
        # Two first tones, make them a little different
        if toneScript.prevTone = 1
            toneRules.startPoint = toneRules.startPoint * 0.999
            toneScript.endPoint = toneScript.endPoint * 0.999
        endif
        
        # Write toneScript.points
        Add point... 'toneScript.point' 'toneRules.startPoint'
        toneScript.point = toneScript.point + toneScript.voicedDuration
        Add point... 'toneScript.point' 'toneScript.endPoint'
    # Second tone
	elsif toneScript.toneSyllable = 2
        # Start halfway of the range - 1 semitone
        toneRules.startPoint = toneRules.levelThree * toneScript.oneSemit
        # End 1 semitones above the first tone
        toneScript.endPoint = toneRules.levelFive / toneScript.oneSemit
        # Special case: 2 followed by 1, stop short of the top-line
        # ie, 5 semitones above the start
        if toneScript.nextTone = 1
            toneScript.endPoint = toneRules.startPoint / toneScript.fiveSemit
	    endif
	    # Go lower if previous tone is 1
	    if toneScript.prevTone = 1
	        toneRules.startPoint = toneRules.startPoint * toneScript.oneSemit
        elsif toneScript.prevTone = 4 or toneScript.prevTone = 3 or toneScript.prevTone = 9
            # Special case: 2 following 4 or 3
            # Go 1 semitone up
	        toneRules.startPoint = toneScript.lastFrequency / toneScript.oneSemit
            toneScript.endPoint = toneRules.levelFive
	    elsif toneScript.prevTone = 2
        # Two consecutive tone 2, start 1 semitone higher
		    toneRules.startPoint = toneRules.startPoint / toneScript.oneSemit
        endif
        # Define a midtoneScript.point at 1/3 of the duration
        toneRules.midPoint = toneRules.startPoint
             
        # Write toneScript.points
        Add point... 'toneScript.point' 'toneRules.startPoint'
        # Next toneScript.point a 1/3th of duration
        toneScript.point = toneScript.point + (toneScript.voicedDuration)/3
        Add point... 'toneScript.point' 'toneRules.midPoint'
        # Remaining duration
        toneScript.point = toneScript.point + (toneScript.voicedDuration)*2/3
        Add point... 'toneScript.point' 'toneScript.endPoint'
    # Third tone
	elsif toneScript.toneSyllable = 3 or toneScript.toneSyllable = 9
        # Halfway the range
        toneRules.startPoint = toneRules.levelThree
        toneRules.lowestPoint = toneRules.levelOne * toneScript.threeSemit
        # Protect pitch against "underflow"
        if toneRules.lowestPoint < toneScript.absoluteMinimum
            toneRules.lowestPoint = toneScript.absoluteMinimum
        endif
        # First syllable
        if toneScript.nextTone < 0
            toneScript.endPoint = toneRules.startPoint
        # Anticipate rise in next tone
        elsif toneScript.nextTone = 1 or toneScript.nextTone = 4
            toneRules.lowestPoint = toneRules.levelOne / toneScript.twoSemit
            toneScript.endPoint = toneRules.startPoint
        # Anticipate rise in next tone and stay low
        elsif toneScript.nextTone = 2
            toneRules.lowestPoint = toneRules.levelOne / toneScript.twoSemit
            toneScript.endPoint = toneRules.lowestPoint
        # Last one was low, don't go so much lower
        elsif toneScript.prevTone = 4
            toneRules.lowestPoint = toneRules.levelOne * toneScript.oneSemit
        # Anticipate rise in next tone and stay low
        elsif toneScript.nextTone = 0
            toneRules.lowestPoint = toneRules.levelOne
            toneScript.endPoint = toneRules.lowestPoint / toneScript.sixSemit
        else
            toneScript.endPoint = toneRules.startPoint
        endif
    
        # Write toneScript.points
     	Add point... 'toneScript.point' 'toneRules.startPoint'
        # Go 1/3 of the duration down
	    toneScript.point = toneScript.point + (toneScript.voicedDuration)*2/6
       	Add point... 'toneScript.point' 'toneRules.lowestPoint'
       	
	    
		# Third tone with voice break
	    if toneScript.toneSyllable = 9
			# voiceless break
			.delta = 'toneScript.point' + 0.001
			Add point... '.delta' 0
		endif
		
	    # Go half the duration low
		toneScript.point = toneScript.point + (toneScript.voicedDuration)*3/6
		
		# Third tone with voice break
	    if toneScript.toneSyllable = 9
			# voiceless break
			# voiceless break
			.delta = 'toneScript.point' - 0.001
			Add point... '.delta' 0
		endif
		
		# Add final point of low (voiced)
	    Add point... 'toneScript.point' 'toneRules.lowestPoint'
        
        # Return in 1/6th of the duration
	    toneScript.point = toneScript.point + (toneScript.voicedDuration)*1/6
        # Go on
      	Add point... 'toneScript.point' 'toneScript.endPoint'

    # Fourth tone
	elsif toneScript.toneSyllable = 4
        # Start higher than tone 1 (by 2 semitones)
        toneRules.startPoint = toneRules.levelFive / toneScript.twoSemit
        # Go down the full range
        toneScript.endPoint = toneRules.startPoint * toneScript.frequency_Range
        
        
        # SPECIAL: Fall in following neutral tone
	    if toneScript.nextTone = 0
	        toneScript.endPoint = toneScript.endPoint / toneScript.threeSemit
        endif
     
        # Write toneScript.points
        Add point... 'toneScript.point' 'toneRules.startPoint'
        # A plateau for 1/3th
        toneScript.point = toneScript.point + toneScript.voicedDuration*1/3
        Add point... 'toneScript.point' 'toneRules.startPoint'
        # Go down the rest
        toneScript.point = toneScript.point + toneScript.voicedDuration*2/3
        Add point... 'toneScript.point' 'toneScript.endPoint'
    # Neutral tone
	elsif toneScript.toneSyllable = 0
        if toneScript.lastFrequency > 0
            toneRules.startPoint = toneScript.lastFrequency
        else
            toneRules.startPoint = toneRules.levelThree / toneScript.oneSemit
        endif

        if toneScript.prevTone = 1
            toneRules.startPoint = toneScript.lastFrequency * toneScript.twoSemit 
        elsif toneScript.prevTone = 2
            toneRules.startPoint = toneScript.lastFrequency
        elsif toneScript.prevTone = 3 or toneScript.prevTone = 9
            toneRules.startPoint = toneScript.lastFrequency / toneScript.oneSemit
        elsif toneScript.prevTone = 4
            toneRules.startPoint = toneScript.lastFrequency * toneScript.oneSemit
        elsif toneScript.lastFrequency > 0
            toneRules.startPoint = toneScript.lastFrequency * toneScript.oneSemit
        endif

        # Catch all errors
        if toneRules.startPoint <= 0
            toneRules.startPoint = toneRules.levelThree / toneScript.oneSemit
        endif
            
       # Add spreading and some small or large de/inclination
        if toneScript.prevTone = 1
        	toneRules.midPoint = toneRules.startPoint * toneScript.frequency_Range / toneScript.oneSemit
            toneScript.endPoint = toneRules.midPoint * toneScript.oneSemit
        elsif toneScript.prevTone = 2
            # 
        	toneRules.midPoint = toneRules.startPoint * toneScript.fiveSemit
            toneScript.endPoint = toneRules.midPoint * toneScript.twoSemit
        elsif toneScript.prevTone = 3 or toneScript.prevTone = 9
        	toneRules.midPoint = toneRules.startPoint / toneScript.twoSemit
            toneScript.endPoint = toneRules.midPoint
        elsif toneScript.prevTone = 4
        	toneRules.midPoint = toneRules.startPoint * toneScript.threeSemit
            toneScript.endPoint = toneRules.midPoint / toneScript.oneSemit
        else
        	toneRules.midPoint = toneRules.startPoint * toneScript.oneSemit
            toneScript.endPoint = toneRules.midPoint
        endif
                
        # Add a very short break to force (disabled, maybe should be removed)
		if 0
	        if toneScript.point = undefined
				toneScript.point = 0
	        endif
	        
	        Add point... 'toneScript.point' 0
			toneScript.point = toneScript.point + 1/toneRules.startPoint
	        Add point... 'toneScript.point' 0
			toneScript.point = toneScript.point + toneScript.delta
		endif
        
        # Write toneScript.points first 2/3 then decaying 1/3
        Add point... 'toneScript.point' 'toneRules.startPoint'
		toneScript.point = toneScript.point + (toneScript.voicedDuration - 1/toneRules.startPoint)*2/3
	    Add point... 'toneScript.point' 'toneRules.midPoint'
		toneScript.point = toneScript.point + (toneScript.voicedDuration - 1/toneRules.startPoint)*1/3
	    Add point... 'toneScript.point' 'toneScript.endPoint'
    # Dutch intonation
	else
        # Start halfway of the range
        toneRules.startPoint = toneRules.levelThree
        # Or continue from last Dutch "tone"
        if toneScript.prevTone = 6
            toneRules.startPoint = toneScript.lastFrequency
        endif
        # Add declination
        toneScript.endPoint = toneRules.startPoint * toneScript.oneSemit
        
        # Write toneScript.points
        Add point... 'toneScript.point' 'toneRules.startPoint'
        toneScript.point = toneScript.point + toneScript.voicedDuration
        Add point... 'toneScript.point' 'toneScript.endPoint'
	endif
endproc
