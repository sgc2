#! praat
#
#     SpeakGoodChinese: toneScript.praat generates synthetic tone contours
#     for Mandarin Chinese
#     Copyright (C) 2007  R.J.J.H. van Son
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Konink, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
# form Enter pinyin and tone 1 frequency
# 	word toneScript.inputWord ba1ba1
# 	positive toneScript.upperRegister_(Hz) 300
#     real toneScript.range_Factor 1
#     real toneScript.durationScale 1
#     optionmenu toneScript.generate 1
#         option Pitch
#         option Sound
#         option CorrectPitch
#         option CorrectSound
# endform

# Get the rules of the tones
# include ToneRules.praat

#call toneScript 'toneScript.inputWord$' 'toneScript.upperRegister' 'toneScript.range_Factor' 'toneScript.durationScale' 'toneScript.generate$'

procedure toneScript toneScript.inputWord$ toneScript.upperRegister toneScript.range_Factor toneScript.durationScale toneScript.generate$
	# To supress the ToneList, change to 0
	toneScript.createToneList = 1
	if rindex_regex(toneScript.generate$, "Correct") > 0
		toneScript.createToneList = 0
	endif
	
	# First tone to check
	.skipSyll$ = "()"
	.startSyll = 0
	if index_regex(toneScript.generate$, "_[\d]+$") > 0
		.pos = index_regex(toneScript.generate$, "_[\d]+$")
		.left$ = left$(toneScript.generate$, .pos)
		.startSyll = extractNumber(toneScript.generate$, .left$) - 1
		if .startSyll <> undefined and .startSyll > 0
			.skipSyll$ = "([^\d]+[\d]+){'.startSyll'}"
		else
			.startSyll = 0
		endif
	endif

	# Limit lowest tone
	toneScript.absoluteMinimum = 80

	toneScript.prevTone = -1
	toneScript.nextTone = -1

	toneScript.point = 0
	toneScript.lastFrequency = 0

	# Clean up input
	if toneScript.inputWord$ <> ""
    	toneScript.inputWord$ = replace_regex$(toneScript.inputWord$, "^\s*(.+)\s*$", "\1", 1)
    	toneScript.inputWord$ = replace_regex$(toneScript.inputWord$, "5", "0", 0)
		# Missing neutral tones
		call add_missing_neutral_tones 'toneScript.inputWord$'
		toneScript.inputWord$ = add_missing_neutral_tones.pinyin$
	endif

	# Add a tone movement. The current time toneScript.point is 'toneScript.point'
	toneScript.delta = 0.0000001
	if toneScript.durationScale <= 0
    	toneScript.durationScale = 1.0
	endif
	toneScript.segmentDuration = 0.150
	toneScript.fixedDuration = 0.12

	#
	# Movements
	# start * ?Semit is a fall
	# start / ?Semit is a rise
	# 1/(12 semitones)
	toneScript.octave = 0.5
	# 1/(9 semitones)
	toneScript.nineSemit = 0.594603557501361
	# 1/(6 semitones)
	toneScript.sixSemit = 0.707106781186547
	# 1/(3 semitones) down
	toneScript.threeSemit = 0.840896415253715
	# 1/(2 semitones) down
	toneScript.twoSemit = 0.890898718140339
	# 1/(1 semitones) down
	toneScript.oneSemit = 0.943874313
	# 1/(4 semitones) down
	toneScript.fourSemit = toneScript.twoSemit * toneScript.twoSemit
	# 1/(5 semitones) down
	toneScript.fiveSemit = toneScript.threeSemit * toneScript.twoSemit

	toneScript.frequency_Range = toneScript.octave
	if toneScript.range_Factor > 0
    	toneScript.frequency_Range =  toneScript.frequency_Range * toneScript.range_Factor
	endif

	# Previous end frequency
	toneScript.lastFrequency = 0
	# Split input into syllables
	toneScript.margin = 0.25

	# Get a list of items
	if toneScript.createToneList = 1
    	Create Table with column names... ToneList 36 Word

    	for .i from 1 to 36
	    	select Table ToneList
        	Set string value... '.i' Word ------EMPTY
    	endfor
	endif

	toneScript.syllableCount = length(replace_regex$(toneScript.inputWord$, "[^\d]+([\d]+)", "1", 0))
	if toneScript.syllableCount < .startSyll + 1
		.skipSyll$ = "()"
	endif
	toneScript.wordNumber = 0
	toneScript.lowerBound = 0
	if rindex(toneScript.generate$, "Correct") <= 0
    	for toneScript.first from toneScript.lowerBound to 4
	    	toneScript.currentWord$ = replace_regex$(toneScript.inputWord$, "^('.skipSyll$')([^\d]+)([\d]+)(.*)$", "\1\3'toneScript.first'\5", 1)
	    	for toneScript.second from 0 to 4
		    	if (toneScript.first <> 5 and toneScript.second <> 5) and (toneScript.syllableCount > 1 or toneScript.second == 1)
			    	toneScript.currentWord$ = replace_regex$(toneScript.currentWord$, "^('.skipSyll$')([^\d]+)([\d]+)([^\d]+)([\d]+)(.*)$", "\1\3'toneScript.first'\5'toneScript.second'\7", 1)
               	# Write name in list
                	toneScript.wordNumber = toneScript.wordNumber+1
                	if toneScript.createToneList = 1
	                	select Table ToneList
                    	toneScript.listLength = Get number of rows
                    	toneScript.listLength = toneScript.listLength + 1
                    	for toneScript.currLength from toneScript.listLength to toneScript.wordNumber
                        	Append row
                        	Set string value... 'toneScript.currLength' Word ------EMPTY
                    	endfor
                    	Set string value... 'toneScript.wordNumber' Word 'toneScript.currentWord$'
                	endif
                	# Actually, generate something
					call generateWord 'toneScript.generate$' 'toneScript.currentWord$' 'toneScript.upperRegister'
		    	endif
	    	endfor
    	endfor
    	
    	# 6,6
    	#toneScript.first = 6
	    #toneScript.currentWord$ = replace_regex$(toneScript.inputWord$, "^('.skipSyll$')([^\d]+)([\d]+)(.*)$", "\1\3'toneScript.first'\5", 1)
	    #toneScript.second = 6
		#if toneScript.syllableCount > 1
		#	toneScript.currentWord$ = replace_regex$(toneScript.currentWord$, "^('.skipSyll$')([^\d]+)([\d]+)([^\d]+)([\d]+)(.*)$", "\1\3'toneScript.first'\5'toneScript.second'\7", 1)
        #    # Write name in list
        #    toneScript.wordNumber = toneScript.wordNumber+1
        #    if toneScript.createToneList = 1
		#		select Table ToneList
        #        toneScript.listLength = Get number of rows
        #        toneScript.listLength = toneScript.listLength + 1
        #        for toneScript.currLength from toneScript.listLength to toneScript.wordNumber
		#			Append row
		#			Set string value... 'toneScript.currLength' Word ------EMPTY
        #        endfor
        #       Set string value... 'toneScript.wordNumber' Word 'toneScript.currentWord$'
        #    endif
		#endif
		
    	# "broken" third tones
    	if index_regex(toneScript.inputWord$, "3") > 0
		    toneScript.currentWord$ = replace$(toneScript.inputWord$, "3", "9", 0)
	        # Write name in list
	        toneScript.wordNumber = toneScript.wordNumber+1
            if toneScript.createToneList = 1
				select Table ToneList
                toneScript.listLength = Get number of rows
                toneScript.listLength = toneScript.listLength + 1
                Set string value... 'toneScript.wordNumber' Word 'toneScript.currentWord$'
            endif
			# Actually, generate something
			call generateWord 'toneScript.generate$' 'toneScript.currentWord$' 'toneScript.upperRegister'
		endif
		
    	# 6 tones
    	if toneScript.syllableCount > 1
		    toneScript.currentWord$ = replace_regex$(toneScript.inputWord$, "[\d+]", "6", 0)
	        # Write name in list
	        toneScript.wordNumber = toneScript.wordNumber+1
            if toneScript.createToneList = 1
				select Table ToneList
                toneScript.listLength = Get number of rows
                toneScript.listLength = toneScript.listLength + 1
                Set string value... 'toneScript.wordNumber' Word 'toneScript.currentWord$'
            endif
			# Actually, generate something
			call generateWord 'toneScript.generate$' 'toneScript.currentWord$' 'toneScript.upperRegister'
		endif
	else
    	call generateWord 'toneScript.generate$' 'toneScript.inputWord$' 'toneScript.upperRegister'
	endif
endproc

procedure extractTone .syllable$
	toneScript.toneSyllable = -1
	.toneScript.currentToneText$ = replace_regex$(.syllable$, "^[^\d]+([\d]+)(.*)$", "\1", 0)
	toneScript.toneSyllable = extractNumber(.toneScript.currentToneText$, "")
endproc

procedure convertVoicing toneScript.voicingSyllable$
	# Remove tones
	toneScript.voicingSyllable$ = replace_regex$(toneScript.voicingSyllable$, "^([^\d]+)[\d]+", "\1", 0)
	# Convert voiced consonants
	toneScript.voicingSyllable$ = replace_regex$(toneScript.voicingSyllable$, "(ng|[wrlmny])", "C", 0)
	# Convert unvoiced consonants
	toneScript.voicingSyllable$ = replace_regex$(toneScript.voicingSyllable$, "(sh|ch|zh|[fsxhktpgqdbzcj])", "U", 0)
	# Convert vowels
	toneScript.voicingSyllable$ = replace_regex$(toneScript.voicingSyllable$, "([aiuoe\XFC])", "V", 0)
endproc

procedure addToneMovement .syllable$ toneScript.topLine toneScript.prevTone toneScript.nextTone
	# Get tone
	toneScript.toneSyllable = -1
	call extractTone '.syllable$'
    if toneScript.toneSyllable = 3 and toneScript.nextTone = 3
        toneScript.toneSyllable = 2
    endif
    if toneScript.toneSyllable = 9 and toneScript.nextTone = 9
        toneScript.toneSyllable = 2
    endif

	# Get voicing pattern
	toneScript.voicingSyllable$ = ""
	call convertVoicing '.syllable$'

	# Account for tones in duration
	toneScript.toneFactor = 1
    # Scale the duration of the current syllable
    call toneDuration
	toneScript.toneFactor = toneScript.toneFactor * toneScript.durationScale

	# Unvoiced part
	toneScript.unvoicedDuration = 0
	if rindex_regex(toneScript.voicingSyllable$, "U") = 1
		toneScript.point = toneScript.point + toneScript.delta
        Add point... 'toneScript.point' 0
		toneScript.point = toneScript.point + toneScript.segmentDuration * toneScript.toneFactor
        Add point... 'toneScript.point' 0
        toneScript.unvoicedDuration = toneScript.segmentDuration * toneScript.toneFactor
	endif
	# Voiced part
	toneScript.voiceLength$ = replace_regex$(toneScript.voicingSyllable$, "U*([CV]+)U*", "\1", 0)
	toneScript.voicedLength = length(toneScript.voiceLength$)
	toneScript.voicedDuration = toneScript.toneFactor * (toneScript.segmentDuration*toneScript.voicedLength + toneScript.fixedDuration)
	toneScript.point = toneScript.point + toneScript.delta

    # Write contour of each tone
    # Note that tones are influenced by the previous (tone 0) and next (tone 3)
    # tones. Tone 6 is the Dutch intonation
    # sqrt(toneScript.frequency_Range) is the mid toneScript.point
    if toneScript.topLine * toneScript.frequency_Range < toneScript.absoluteMinimum
        toneScript.frequency_Range = toneScript.absoluteMinimum / toneScript.topLine
    endif

    call toneRules
	
    toneScript.lastFrequency = toneScript.endPoint

endproc

procedure wordToTones .wordInput$ toneScript.highPitch
	.currentRest$ = .wordInput$;
	toneScript.syllableCount = 0
	.length = 2 * toneScript.margin

    # Split syllables
    toneScript.prevTone = -1
	while rindex_regex(.currentRest$, "^[^\d]+[\d]+") > 0
        toneScript.syllableCount += 1
        syllable'toneScript.syllableCount'$ = replace_regex$(.currentRest$, "^([^\d]+[\d]+)(.*)$", "\1", 1)
		toneScript.currentSyllable$ = syllable'toneScript.syllableCount'$

		# For next round
		.currentRest$ = replace_regex$(.currentRest$, "^([^\d]+[\d]+)(.*)$", "\2", 1)
		# Get next syllable
		.nextSyllable$ = replace_regex$(.currentRest$, "^([^\d]+[\d]+)(.*)$", "\1", 1)
		call extractTone '.nextSyllable$'
		toneScript.nextTone = toneScript.toneSyllable

		# Get the current tone
		call extractTone 'toneScript.currentSyllable$'
		toneScript.toneSyllable'toneScript.syllableCount' = toneScript.toneSyllable
		toneScript.currentTone = toneScript.toneSyllable'toneScript.syllableCount'

		# Get the Voicing pattern
		call convertVoicing 'toneScript.currentSyllable$'
		voicingSyllable'toneScript.syllableCount'$ = toneScript.voicingSyllable$
		currentVoicing$ = voicingSyllable'toneScript.syllableCount'$

		# Calculate new .length
	    # Account for tones in duration
	    toneScript.toneFactor = 1
        # Scale the duration of the current syllable
        call toneDuration
	    toneScript.toneFactor = toneScript.toneFactor * toneScript.durationScale

		.length = .length + toneScript.toneFactor * (length(voicingSyllable'toneScript.syllableCount'$) * (toneScript.segmentDuration + toneScript.delta) + toneScript.fixedDuration)

		# Safety valve
		if toneScript.syllableCount > 2000
   			exit
		endif
		toneScript.prevTone = toneScript.currentTone
	endwhile

	# Create tone pattern
	toneScript.pitchTier = Create PitchTier: .wordInput$, 0, .length
	toneScript.pitchTierWord$ = .wordInput$
	# Create TextGrid with syllables
	toneScript.textGrid = Create TextGrid: 0, .length, "Tones", ""
	Rename: .wordInput$
	.int = 0;

	# Add start toneScript.margin
	select toneScript.pitchTier
	toneScript.lastFrequency = 0
    toneScript.point = 0
	Add point... 'toneScript.point' 0
	toneScript.point = toneScript.margin
	Add point... 'toneScript.point' 0
	
    # Add interval to TextGrid
	select toneScript.textGrid
	Insert boundary: 1, toneScript.point
	.int += 1
	Set interval text: 1, .int, ""

    toneScript.lastTone = -1
    toneScript.followTone = -1
	for .i from 1 to toneScript.syllableCount
		select toneScript.pitchTier
		toneScript.currentSyllable$ = syllable'.i'$
        toneScript.currentTone = toneScript.toneSyllable'.i'
        toneScript.followTone = -1
        if .i < toneScript.syllableCount
            .j = .i+1
            toneScript.followTone = toneScript.toneSyllable'.j'
        endif
		.lastPoint = toneScript.point
		call addToneMovement 'toneScript.currentSyllable$' 'toneScript.highPitch' 'toneScript.lastTone' 'toneScript.followTone'

        toneScript.lastTone = toneScript.currentTone
        
        # Add interval to TextGrid
        .writeTone = toneScript.currentTone
        # 
        if (toneScript.currentTone = 3 or toneScript.currentTone = 9) and toneScript.currentTone = toneScript.followTone
			.writeTone = 2
        endif
		select toneScript.textGrid
		if toneScript.unvoicedDuration > 0
			Insert boundary: 1, .lastPoint + toneScript.unvoicedDuration
			.int += 1
			Set interval text: 1, .int, "U"
		endif
		Insert boundary: 1, toneScript.point
		.int += 1
		Set interval text: 1, .int, "'.writeTone'"
	endfor

	# Add end toneScript.margin
	select toneScript.pitchTier
	toneScript.point = toneScript.point + toneScript.delta
	Add point... 'toneScript.point' 0
	toneScript.point = toneScript.point + toneScript.margin
	Add point... 'toneScript.point' 0
endproc

procedure generateWord toneScript.whatToGenerate$ toneScript.theWord$ toneScript.upperRegister

	# First generate model contour
	call wordToTones 'toneScript.theWord$' 'toneScript.upperRegister'
	
	if index(toneScript.whatToGenerate$, "TextGrid") <= 0
		if variableExists("toneScript.textGrid") and toneScript.textGrid > 0
			select toneScript.textGrid
			Remove
		endif
	endif
	
	# Generate pitch
	if toneScript.pitchTierWord$ = toneScript.theWord$
		select toneScript.pitchTier
	else
		select PitchTier 'toneScript.theWord$'
	endif
    toneScript.pitch = noprogress To Pitch... 0.0125 60.0 600.0
	Rename... theOrigWord
	Smooth... 10
	Rename... 'toneScript.theWord$'
	select Pitch theOrigWord
	Remove

	# Then look if "real" model exists, and use that
	if index_regex(config.strict$, "[^0-9]") > 0
		config.strict$ = "1"	
	endif
	.userLevel = 'config.strict$'
 	if 0 and .userLevel >= sgc.highestLevel
...    and (fileReadable("'preferencesAppDir$'/pitchmodels/'toneScript.theWord$'.Pitch")
...	        or fileReadable("'preferencesAppDir$'/pitchmodels/'toneScript.theWord$'.wav"))
		# Get mean of generated contour
		select toneScript.pitch
		Rename... GeneratedContour
		toneScript.generatedMean = do ("Get mean...", 0, 0, "Hertz")
		toneScript.generatedMaximum = do ("Get maximum...", 0, 0, "Hertz", "Parabolic")
		Remove
		if fileReadable("'preferencesAppDir$'/pitchmodels/'toneScript.theWord$'.Pitch")
			Read from file... 'preferencesAppDir$'/pitchmodels/'toneScript.theWord$'.Pitch
		else
			.modelSound = Read from file... 'preferencesAppDir$'/pitchmodels/'toneScript.theWord$'.wav
	        select .modelSound
	        # Third tones get really low
	        if index_regex(toneScript.theWord$, "3") > 0
				call convert2Pitch 20 600
				.modelPitch = Kill octave jumps
				select convert2Pitch.object
				Remove
			else
				call convert2Pitch 60 600
				.modelPitch = convert2Pitch.object
			endif
			select .modelPitch
			Rename... ModelPitch

			select .modelSound
			Remove
			select .modelPitch
		endif
		Rename... 'toneScript.theWord$'
		toneScript.mean = do ("Get mean...", 0, 0, "Hertz")
		toneScript.maximum = do ("Get maximum...", 0, 0, "Hertz", "Parabolic")
		toneScript.shiftFreq = toneScript.generatedMean - toneScript.mean
		# toneScript.shiftFreq = toneScript.generatedMaximum - toneScript.maximum
		Formula... self + toneScript.shiftFreq
	endif
	
    # Generate sound if wanted
    select Pitch 'toneScript.theWord$'
    if rindex_regex(toneScript.whatToGenerate$, "Sound") > 0
	    noprogress To Sound (hum)
    endif

    # Clean up
    select PitchTier 'toneScript.theWord$'
    if rindex_regex(toneScript.whatToGenerate$, "(Sound|TextGrid)") > 0
        plus Pitch 'toneScript.theWord$'
    endif
    Remove
endproc

procedure convert2Pitch .minimumPitch .maximumPitch
	#.object = noprogress To Pitch (ac)... 0.01 '.minimumPitch' 25 yes 0.05 0.3 0.01 0.6 0.14 '.maximumPitch'
	.object = noprogress To Pitch (cc)... 0.005 '.minimumPitch' 25 yes 0.03 0.50 0.045 0.35 0.14 '.maximumPitch'
endproc

# Not everyone add all the zeros for the neutral tones. Here we try to guess where
# they would belong.
procedure add_missing_neutral_tones .pinyin$
	# Missing neutral tones
	# Missing last tone
	.pinyin$ = replace_regex$(.pinyin$, "([^0-9])$", "\10", 0)
	# Easy cases V [^n]
	.pinyin$ = replace_regex$(.pinyin$, "([euioa]+)([^0-9neuioar])", "\10\2", 0)
	# Complex case V r V
	.pinyin$ = replace_regex$(.pinyin$, "([euioa]+)(r[euioa]+)", "\10\2", 0)
	# Complex case V r C
	.pinyin$ = replace_regex$(.pinyin$, "([euioa]+r)([^0-9euioa]+)", "\10\2", 0)
	# Vng cases
	.pinyin$ = replace_regex$(.pinyin$, "([euioa]+ng)([^0-9])", "\10\2", 0)
	# VnC cases C != g
	.pinyin$ = replace_regex$(.pinyin$, "([euioa]+n)([^0-9geuioa])", "\10\2", 0)
	# VnV cases -> Maximal onset
	.pinyin$ = replace_regex$(.pinyin$, "([euioa])(n[euioa])", "\10\2", 0)
endproc
