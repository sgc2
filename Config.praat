#
# SpeakGoodChinese 2.0
# 
# Praat script handling configuration page
#
#     SpeakGoodChinese: Config.praat loads the code needed for the 
#     settings and the Settings page of SpeakGoodChinese.
#     
#     Copyright (C) 2007-2010  R.J.J.H. van Son and 2010 the Netherlands Cancer Institute
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Koning, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 

###############################################################
#
# Button Drawing Routines
#
###############################################################

procedure DrawCredits .color$ .x .y .size
	.size *= 0.71
	.lineheight = 2*.size
	call adjustFontSizeOnHeight 'defaultFont$' 24 '.lineheight'
	.currentFontSize = adjustFontSizeOnHeight.newFontSize
    demo Paint circle... {0.2,0.2,0.8} '.x' '.y' '.size'
	demo Colour... White
	demo Text special... '.x' centre '.y' half Times '.currentFontSize' 0 i
	demoShow()
	demo Colour... Black
	call set_font_size 'defaultFontSize'	
endproc

procedure DrawDeleteList .color$ .x .y .size
    .y += 2*.size
    .leftX = .x - 10
    .rightX = .x + 10
    .botY = .y+1
    .topY = .botY + 5
	call set_font_size 12
    demo Red
    .displayWordList$ = replace_regex$(wordlistName$, "[_]", " ", 0)

	# Adapt wide of text
	.maxWidth = (.rightX - .leftX)
    .currentFontSize = defaultFontSize
	call adjustFontSizeOnWidth 'defaultFont$' '.currentFontSize' '.maxWidth' '.displayWordList$'
	.currentFontSize = adjustFontSizeOnWidth.newFontSize
	if adjustFontSizeOnWidth.diff > 0
		.rightX += adjustFontSizeOnWidth.diff/2
		.leftX -= adjustFontSizeOnWidth.diff/2
	endif
	call set_font_size '.currentFontSize'
    demo Paint rectangle... White '.leftX' '.rightX' '.botY' '.topY'

	demo Text... '.x' Centre '.botY' Bottom '.displayWordList$'
    demo Black
	demoShow()
	call set_font_size 'defaultFontSize'
endproc

procedure DrawManual .color$ .x .y .size
	.size *= 0.71
	.lineheight = 2*.size
	call adjustFontSizeOnHeight 'defaultFont$' 24 '.lineheight'
	.currentFontSize = adjustFontSizeOnHeight.newFontSize
    demo Paint circle... {0.2,0.2,0.8} '.x' '.y' '.size'
	demo Colour... White
	demo Text special... '.x' centre '.y' half Times '.currentFontSize' 0 M
	demoShow()
	demo Colour... Black
	call set_font_size 'defaultFontSize'	
endproc

procedure DrawSaveAudio .color$ .x .y .size
    .size /= 2
	.y += .size
    if .color$ = "White"
    	.color$ = "White"
    elsif .color$ = "Blue"
    	.color$ = "{0.5,0.5,1}"
    else
    	.color$ = "Blue"
    endif
    demo Paint circle... '.color$' '.x' '.y' '.size'
endproc

###############################################################
#
# Obligatory button Drawing Routines
# 
# These MUST be defined
#
###############################################################

procedure DrawReturn .color$ .x .y .size
    call DrawConfig '.color$' '.x' '.y' '.size'
endproc

# Set the correct button states after redrawing the window
procedure setConfigMainPage
    call Draw_button 'config$' +SaveAudio 'sgc.saveAudioOn'
    if sgc2.synthesizer < 0 or config.synthesis$ = "" or config.synthesis$ = "_DISABLED_"
		call Draw_button 'config$' Voice -1
	else
		call Draw_button 'config$' Voice 2
	endif
endproc

###############################################################
#
# Button Processing Routines
#
###############################################################

procedure processConfigShuffleLists .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "ShuffleLists"
	config.shuffleLists = not config.shuffleLists
	.displayButton = 2*config.shuffleLists
    call Draw_button '.table$' '.label$' '.displayButton'
endproc

procedure processConfigAdaptiveLists .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "AdaptiveLists"
	config.adaptiveLists = not config.adaptiveLists
	.displayButton = 2*config.adaptiveLists
    call Draw_button '.table$' '.label$' '.displayButton'
endproc

procedure processConfigUseSoundExample .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "UseSoundExample"
	config.useSoundExample = not config.useSoundExample
	.displayButton = 2*config.useSoundExample
    call Draw_button '.table$' '.label$' '.displayButton'
endproc
        
procedure processConfigSynthesis .tts$ .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Synthesis_'.tts$'"
	if sgc2.synthesizer > 0 or (speakCommandFile$ <> "" and fileReadable(speakCommandFile$))
		if config.synthesis$ = ""
			config.synthesis$ = .tts$
			.displayButton = 2
		else
			config.synthesis$ = ""
			.displayButton = 0
		endif
	else
		config.synthesis$ = "_DISABLED_"
		.displayButton = -1
	endif
    call Draw_button '.table$' '.label$' '.displayButton'
    
    # Disable or enable voices button!
    .voiceButton = .displayButton
    if .voiceButton <= 0
		.voiceButton = -1
	endif
    call Draw_button Config Voice '.voiceButton'
endproc
        
procedure processConfigStrict .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Strict"
	.tmp = 'config.strict$'
	.tmp += 1
	if .tmp > sgc.highestLevel
		.tmp = 0
	endif
	config.strict$ = "'.tmp'"
	if .tmp > 0
		.displayButton = 2
	else
		.displayButton = 0
	endif
	# Change TTS for Strict!
	call set_up_TTS
    call Draw_button '.table$' '.label$' '.displayButton'
endproc

procedure processConfigVoice .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Voice"
	.tmp = config.voice
	.tmp += 1
	if .tmp > sgc.lastVoice
		.tmp = 0
	endif
	.displayButton = 2
	if sgc2.synthesizer <= 0 or config.synthesis$ = "" or config.synthesis$ = "_DISABLED_"
		.displayButton = -1
	else
		config.voice = .tmp
		# Change TTS Voice
		call set_TTS_parameters
		call set_up_TTS
	endif
    call Draw_button '.table$' '.label$' '.displayButton'
endproc

procedure processConfigDisplayPinyin .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "DisplayPinyin"
	config.displayPinyin = not config.displayPinyin
	.displayButton = 2*config.displayPinyin
    call Draw_button '.table$' '.label$' '.displayButton'
endproc
		
procedure processConfigDisplayChar .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "DisplayChar"
	config.displayChar = not config.displayChar
	.displayButton = 2*config.displayChar
    call Draw_button '.table$' '.label$' '.displayButton'
endproc

procedure processConfigDisplayTrans .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "DisplayTrans"
	config.displayTrans = not config.displayTrans
	.displayButton = 2*config.displayTrans
    call Draw_button '.table$' '.label$' '.displayButton'
endproc

procedure processConfigDisplayNumbers .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "DisplayNumbers"
	config.displayNumbers = not config.displayNumbers
	.displayButton = 2*config.displayNumbers
    call Draw_button '.table$' '.label$' '.displayButton'
endproc

procedure processConfigLanguage .language$ .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Language_'.language$'"
	call processLanguageCodes '.table$' '.label$'
endproc
        
procedure processConfigShowBackground .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "ShowBackground"
	config.showBackground = not config.showBackground
	.displayButton = 2*config.showBackground
    call Draw_button '.table$' '.label$' '.displayButton'
endproc

procedure processConfigInput .input$ .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Input_'.input$'"
    call Draw_button '.table$' Input_'config.input$' 0
	config.input$ = .input$
    call Draw_button '.table$' Input_'config.input$' 2
endproc

procedure processConfigRegister .register .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Register_'.register'"
	call setRegisterFromLabel '.table$' '.label$'
endproc
        
procedure setRegisterFromLabel .table$ .label$
    call Draw_button '.table$' Register_'config.register' 0
    call Draw_button '.table$' '.label$' 2
    # Someone might have to use more than 3 chars for the config.register code
    .numChars = length(.label$) - length("Register_")
	.registerText$ = right$(.label$, .numChars)
	config.register = '.registerText$'
endproc

procedure processConfigDeleteWordlist .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "DeleteWordlist"

    # Do not process undeletable word lists, only those stored in the 
    # preferencesDirectory$ can be deleted
    if fileReadable("'sgc2wordlists$'/'wordlistName$'") or fileReadable("'sgc2wordlists$'/'wordlistName$'/wordlist.txt") or	fileReadable("'sgc2wordlists$'/'wordlistName$'/wordlist.Table")
		call findLabel '.table$' !'.label$'
		if findLabel.row > 0
        	select Table '.table$'
        	.alertText$ = Get value... 'findLabel.row' Text
        	.confirmKey$ = Get value... 'findLabel.row' Key
        	.popupText$ = Get value... 'findLabel.row' Helptext
        else
            exit Cannot find delete directive: '.table$' !'.label$'
        endif
        call write_text_popup 'defaultFont$' 14 '.popupText$'
        call Draw_button '.table$' '.label$' 2
        alertText$ = .alertText$
        call Draw_button '.table$' '.label$' 3
        alertText$ = ""
	
		# Wait for confirmation
		demoWaitForInput()
        if demoInput(.confirmKey$)
        	.deleteWordListDir$ = wordlistName$
        	call load_word_list "'localWordlistDir$'" 1
        	call removeWordlist '.deleteWordListDir$'
			call load_word_list "'localWordlistDir$'" 0
    	endif
		call Draw_button '.table$' '.label$' 0
		call Draw_config_page
    endif
endproc

wordlistTag$ = "Wordlist name"        
procedure processConfigInstallWordlist .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "InstallWordlist"
    call Draw_button '.table$' '.label$' 1
	if windows
		# Do not use the automatic sgc list option, ask for a wordlist NAME
		# Get help text
		call findLabel '.table$' '.label$'
		.row = findLabel.row
		select Table '.table$'
		.openDialogue$ = Get value... '.row' Helptext
		call convert_praat_to_latin1 '.openDialogue$'
		.openDialogue$ = convert_praat_to_latin1.text$
		.wordlistButton$ = replace$(wordlistTag$, " ", "_", 0)
		.wordlistButton$ = replace_regex$(.wordlistButton$, "^.", "\l&", 0)
		beginPause(.openDialogue$)
			sentence (wordlistTag$, "")
		clicked = endPause ("Cancel", "Open", 2, 1)
		.wordlist_Name$ = ""
		if clicked = 2
			.wordlist_Name$ = '.wordlistButton$'$
		endif
		call install_wordlists_by_name '.wordlist_Name$'
	else
    	call sgc2wordlist 'homeDirectory$'
    	call sgc2wordlist 'homeDirectory$'/Downloads
    	call sgc2wordlist 'homeDirectory$'/Documents
    	call sgc2wordlist 'homeDirectory$'/My Documents
    	call sgc2wordlist 'homeDirectory$'/My Documents/Downloads
    	call sgc2wordlist 'homeDirectory$'/Desktop
    	call sgc2wordlist 'preferencesAppDir$'
	endif
	call load_word_list "'localWordlistDir$'" 0
    call Draw_button '.table$' '.label$' 0
	call Draw_config_page
endproc

procedure processConfigOpenWordlist .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "OpenWordlist"
    call Draw_button '.table$' '.label$' 1

	# Get help text
	call findLabel '.table$' '.label$'
	.row = findLabel.row
	select Table '.table$'
	.openDialogue$ = Get value... '.row' Helptext
	call convert_praat_to_latin1 '.openDialogue$'
	.openDialogue$ = convert_praat_to_latin1.text$

	.wordlist_Name$ = chooseReadFile$ (.openDialogue$)
	call install_wordlists_by_name '.wordlist_Name$'
	call load_word_list "'localWordlistDir$'" 0
    call Draw_button '.table$' '.label$' 0
	call Draw_config_page
endproc

procedure processConfigExportWordlist .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "ExportWordlist"
	
    call Draw_button '.table$' '.label$' 1
	
	# Get help text
	call findLabel '.table$' '.label$'
	.row = findLabel.row
	select Table '.table$'
	.openDialogue$ = Get value... '.row' Helptext
	call convert_praat_to_latin1 '.openDialogue$'
	.openDialogue$ = convert_praat_to_latin1.text$
	
	.newWordlistName$ = "'wordlist$'_SGC2"

	# Ensure the right extension: ".tsv"
	while .newWordlistName$ <> "" and not endsWith(.newWordlistName$, ".tsv")
		.newWordlistName$ = .newWordlistName$ + ".tsv"
		.newWordlistName$ = chooseWriteFile$ (.openDialogue$, .newWordlistName$)
		
		.tableName$ = replace_regex$(.newWordlistName$, "^.*[/\\:]([^/\\:]+)\.(tsv|Table)$", "\1", 0)
		call testLoadTable '.tableName$'
		if testLoadTable.table > 0
			call loadTable '.tableName$'
			.tmpTable = selected()
			Save as tab-separated file: .newWordlistName$
			Remove
			.newWordlistName$ = ""
		endif
		
	endwhile
	if .newWordlistName$ <> ""
		select sgc.currentWordlist
		.tmpSaveWordlist = Copy: "SaveWordlist"
		# Get the table sorted
		.lessonCol = Get column index: "Lesson"
		if .lessonCol > 0
			Sort rows... Lesson Pinyin
		else
			Sort rows... Pinyin
		endif
		
		# Remove all words that are deselected
		.showCol = Get column index: "Show"
		if .showCol > 0
			.numRows = Get number of rows
			.row = .numRows
			while .row > 0
				.show$ = Get value: .row, "Show"
				.sound$ = Get value: .row, sgcwordlist.audio$
				if .show$ = "-"
					.numRows = Get number of rows
					# Do NOT remove all the rows
					if .numRows > 1
						Remove row: .row
					endif
				elsif .sound$ <> "" and .sound$ <> "-" and .sound$ <> "?"
					# Put path before sound examples
					# If there is no path, add the current wordlist path
					if index_regex(.sound$, "^(/|~/|[A-Z]:\\)") <= 0
						.sound$ = localWordlistDir$+"/"+wordlistName$+"/"+.sound$
						Set string value: .row, sgcwordlist.audio$, .sound$
					endif
				endif
				.row -= 1
			endwhile
			Remove column: "Show"
		endif
		Write to table file... '.newWordlistName$'
		Remove
	endif
	
    call Draw_button '.table$' '.label$' 0
	call Draw_config_page
endproc

procedure processConfigPerfSummary .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "PerfSummary"
	
	call testLoadTable SummaryToneEvaluation
	if testLoadTable.table > 0
		call Draw_button '.table$' '.label$' 1
		call loadTable SummaryToneEvaluation
		call write_tabbed_table SummaryToneEvaluation Evaluation_'config.language$'
		demoWaitForInput()
		select Table SummaryToneEvaluation
		Remove

		call Draw_button '.table$' '.label$' 0
		call Draw_config_page
	endif
endproc

# Wipe current performance table and initialize a new one
procedure processConfigClearSummary .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "ClearSummary"
	
    call Draw_button '.table$' '.label$' 1
	
	if not sgc.saveAudioOn
		select Table '.table$'
		call findLabel '.table$' !'.label$'
		if findLabel.row > 0
			.alertText$ = Get value... 'findLabel.row' Text
			.confirmKey$ = Get value... 'findLabel.row' Key
			.popupText$ = Get value... 'findLabel.row' Helptext
			
	        call write_text_popup 'defaultFont$' 14 '.popupText$'
	        call Draw_button '.table$' '.label$' 2
	        alertText$ = .alertText$
	        call Draw_button '.table$' '.label$' 3
	        alertText$ = ""
		
			# Wait for confirmation
			demoWaitForInput()
	        if demoInput(.confirmKey$)
				sgc.savePerf$ = ""
				call initialize_toneevaluation_tables
			endif
		endif
	endif
	
    call Draw_button '.table$' '.label$' 'sgc.saveAudioOn'
	call Draw_config_page
	
endproc

procedure processConfigListPerf .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "ListPerf"
    call Draw_button '.table$' '.label$' 1
	
	select sgc2.performanceTable
	View & Edit
	demoWaitForInput()
	
    call Draw_button '.table$' '.label$' 0
	call Draw_config_page
endproc

procedure processConfigOpenPerf .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "OpenPerf"
    call Draw_button '.table$' '.label$' 1
	
	if not sgc.saveAudioOn 
		# Get help text
		call findLabel '.table$' !'.label$'
		.row = findLabel.row
		select Table '.table$'
		.openDialogue$ = Get value... '.row' Helptext
		call convert_praat_to_latin1 '.openDialogue$'
		.openDialogue$ = convert_praat_to_latin1.text$
		if variableExists("eval.performance$")
			.defaultName$= "'eval.performance$'.tsv"
		else
			.defaultName$= "Performance.tsv"
		endif
		if sgc.savePerf$ <> ""
			.defaultName$= sgc.savePerf$
		endif
		.notChoosen = 1
		while .notChoosen > 0
			.notChoosen = 0
			.performance_Name$ = chooseReadFile$ (.openDialogue$)
			while .performance_Name$ <> "" and index_regex(.performance_Name$, "\.(tsv|TSV)$") <= 0
				.performance_Name$ = chooseReadFile$ (.openDialogue$)			
			endwhile
			if .performance_Name$ <> "" and fileReadable(.performance_Name$)
				sgc.savePerf$ = .performance_Name$
				
				# Set SaveAudio directory if it is not currently "in use"
				if not sgc.saveAudioOn
					sgc.saveAudio$ = replace_regex$(sgc.savePerf$, "[/\\][^/\\]+$", "", 0)
					.fileName$ = right$(sgc.savePerf$, length(sgc.savePerf$) - index_regex(sgc.savePerf$, "[/\\][^/\\]+$"))
					.dirName$ = replace_regex$(.fileName$, "(?i\.tsv$)", "", 0)
					if fileReadable("'sgc.saveAudio$'/'.dirName$'/'.fileName$'")
						sgc.saveAudio$ = "'sgc.saveAudio$'/'.dirName$'"
					else
						.audioDir = Create Strings as directory list: "AudioDir", "'sgc.saveAudio$'/*"
						.numStrings = Get number of strings
						for .d to .numStrings
							.subdir$ = Get string: .d
							if fileReadable("'sgc.saveAudio$'/'.subdir$'/'.dirName$'/'.fileName$'")
								sgc.saveAudio$ = "'sgc.saveAudio$'/'.subdir$'/'.dirName$'"
							endif
						endfor
					endif
					# We are not sure yet that this is actually an audio directory
					config.audioName$ = ""
				endif
				
				# Initialize the table
				call initialize_toneevaluation_tables
				.notChoosen = initialize_toneevaluation_tables.invalidperformanceTable
			endif
		endwhile
	endif
	
    call Draw_button '.table$' '.label$' 'sgc.saveAudioOn'
	call Draw_config_page
endproc

procedure processConfigSavePerf .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "SavePerf"
    call Draw_button '.table$' '.label$' 1
	
	if not sgc.saveAudioOn 
		# Get help text
		call findLabel '.table$' '.label$'
		.row = findLabel.row
		select Table '.table$'
		.openDialogue$ = Get value... '.row' Helptext
		call convert_praat_to_latin1 '.openDialogue$'
		.openDialogue$ = convert_praat_to_latin1.text$
		if variableExists("eval.performance$")
			.defaultName$= "'eval.performance$'.tsv"
		else
			.defaultName$= "Performance.tsv"
		endif
		if sgc.savePerf$ <> ""
			.defaultName$= sgc.savePerf$
		endif
		.performance_Name$ = chooseWriteFile$ (.openDialogue$, .defaultName$)
		# Ensure the right extension: ".tsv"
		while .performance_Name$ <> "" and not endsWith(.performance_Name$, ".tsv")
			.performance_Name$ = .performance_Name$ + ".tsv"
			.performance_Name$ = chooseWriteFile$ (.openDialogue$, .performance_Name$)
		endwhile
		if .performance_Name$ <> ""
			select sgc2.performanceTable
			Write to table file... '.performance_Name$'
			sgc.savePerf$ = .performance_Name$
		endif
	endif
	
    call Draw_button '.table$' '.label$' 'sgc.saveAudioOn'
	call Draw_config_page
endproc

procedure processConfigManual .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Manual"
	call Draw_button '.table$' '.label$' 1
	if fileReadable("ManPages/SpeakGoodChinese_2.man")
		Read from file... ManPages/SpeakGoodChinese_2.man
	else
		Go to manual page... SpeakGoodChinese 2
	endif
	# Wait until the manual is put to the background
	demoWaitForInput()
    call Draw_button '.table$' '.label$' 0
    demo Erase all
    call Draw_config_page
endproc

procedure processConfigSaveAudio .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "SaveAudio"
    call Draw_button '.table$' '.label$' 1
	
	if sgc.saveAudioOn = 0
		# Get help text
		call findLabel '.table$' '.label$'
		.row = findLabel.row
		select Table '.table$'
		.openDialogue$ = Get value... '.row' Helptext
		call convert_praat_to_latin1 '.openDialogue$'
		.openDialogue$ = convert_praat_to_latin1.text$
		.defaultName$= ""
		sgc.saveAudio$ = chooseDirectory$ (.openDialogue$)
		if sgc.saveAudio$ <> ""
			sgc.saveAudioOn = 1
			.currentPathName$ = replace_regex$(sgc.saveAudio$, "[^/\\]*[/\\]?$", "", 0)
			.currentDirName$ = replace$(sgc.saveAudio$, .currentPathName$, "", 0)
			config.savePerf = -1
			config.openPerf = -1
			config.clearSummary = -1
			config.audioName$ = .currentDirName$

			# Clear performance table and open/create sgc.savePerf$
			sgc.savePerf$ = "'sgc.saveAudio$'/'.currentDirName$'.tsv"
			call initialize_toneevaluation_tables
			# Write empty table
			call update_toneevaluation_file
			if sgc.savePerf$ <> "" and initialiseSGC2.toneevaluation_table$ <> ""
				select sgc2.performanceTable
				Write to table file: sgc.savePerf$
			endif
		else
			config.audioName$ = ""
		endif
	else
		sgc.saveAudioOn = 0
		config.savePerf = 0
		config.openPerf = 0
		config.clearSummary = 0
		# Store current performance table
		call update_toneevaluation_file
	endif
	
    call Draw_button '.table$' '.label$' 'sgc.saveAudioOn'
	call Draw_config_page
endproc

###############################################################
#
# Obligatory button Processing Routines
# 
# These MUST be defined
#
###############################################################

procedure processConfigReturn .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Return"
	call Draw_button '.table$' '.label$' 1
	call write_preferences ""
endproc

procedure processConfigRefresh .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Refresh"
	call Draw_config_page
endproc

procedure processConfigCredits .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Credits"
	call Draw_button '.table$' '.label$' 1
	call write_text_table Credits_'config.language$'
	demoWaitForInput()
    call Draw_button '.table$' '.label$' 0
    demo Erase all
    call Draw_config_page
endproc

procedure processConfigHelp .clickX .clickY .pressed$
	.table$ = "Config"
	.label$ = "Help"
	call help_loop	'.table$' Draw_config_page
endproc

###############################################################
#
# Miscelaneous supporting code
#
###############################################################
procedure install_wordlists_by_name .wordlist_Name$
	if .wordlist_Name$ <> ""
		if index_regex(.wordlist_Name$, "(?iwordlist|LICENSE)\.")
			.wordlist_Name$ = replace_regex$(.wordlist_Name$, "[/\\](?iwordlist|LICENSE)\.([^/\\]+)$", "", 0)
		elsif index_regex(.wordlist_Name$, "(?i\.(wav|flac|iaf[fc]|mp3))")
			.wordlist_Name$ = replace_regex$(.wordlist_Name$, "[/\\][^/\\]+$", "", 0)
		endif
		if index_regex(.wordlist_Name$, "[/\\]")
			.sourceDir$ = left$(.wordlist_Name$, rindex_regex(.wordlist_Name$, "[/\\]") -1)
			.file$ = right$(.wordlist_Name$, length(.wordlist_Name$) - rindex_regex(.wordlist_Name$, "[/\\]"))
			call readWordlist "'.sourceDir$'" '.file$'
		else
			.start = 1
			if index(.wordlist_Name$, ".")
				.start = 4
			endif
			.extension1$ = ".sgc"
			.extension2$ = ".Table"
			.extension3$ = ".txt"
			.extension4$ = ".tsv"
			.extension5$ = ""
			for .e from .start to 5
				.currentExtension$ = .extension'.e'$
    			call readWordlist "'homeDirectory$'" '.wordlist_Name$''.currentExtension$'
    			call readWordlist "'homeDirectory$'/Downloads" '.wordlist_Name$''.currentExtension$'
    			call readWordlist "'homeDirectory$'/Documents" '.wordlist_Name$''.currentExtension$'
    			call readWordlist "'homeDirectory$'/My Documents/Downloads" '.wordlist_Name$''.currentExtension$'
    			call readWordlist "'homeDirectory$'/My Documents" '.wordlist_Name$''.currentExtension$'
    			call readWordlist "'homeDirectory$'/Desktop" '.wordlist_Name$''.currentExtension$'
    			call readWordlist "'preferencesAppDir$'" '.wordlist_Name$''.currentExtension$'
			endfor
		endif
	endif
endproc

# Word lists: It is a VERY good idea to make sure that word-lists
# have unique names.
procedure load_word_list .localdir$ .relnumber
	call clean_up_sound
	if sgc.allWordLists > 0
		select sgc.allWordLists
		Remove
		sgc.allWordLists = -1
	endif
    
    # Remove old word list
    if wordlist$ <> ""
		select sgc.currentWordlist
		Remove
		call wipeArea 'wipeWordlistArea$'
		wordlist$ = ""
		sgc.currentWordlist = -1
		sgc.currentWord = 1
    endif
    
	# Create Table that will recieve the wordlists and directories
	sgc.allWordLists = Create Table with column names... AllWordLists 0 Name Directory
	.sgc.currentWordlistRow = 0
	
	# First the global word lists
	if fileReadable(globalwordlists$) or fileReadable("'globalwordlists$'/directory.txt")
    	Create Strings as directory list... WordList 'globalwordlists$'
    	.numLists = Get number of strings
        for .i to .numLists
            select Strings WordList
            .currentName$ = Get string... '.i'
			if .currentName$ <> "directory.txt"
				select sgc.allWordLists
				.listExist = Search column: "Name", .currentName$
				if not .listExist
					Append row
					.sgc.currentWordlistRow = Get number of rows
					Set string value... '.sgc.currentWordlistRow' Name '.currentName$'
			    	.currentDirectory$ = globalwordlists$+"/"+.currentName$
					Set string value... '.sgc.currentWordlistRow' Directory '.currentDirectory$'
				endif
			endif
        endfor
        select Strings WordList
        Remove
	endif
	
	# Now the preferences word lists
	if fileReadable(sgc2wordlists$) or fileReadable("'sgc2wordlists$'/directory.txt")
    	Create Strings as directory list... WordList 'sgc2wordlists$'
    	.numLists = Get number of strings
        for .i to .numLists
			select Strings WordList
            .currentName$ = Get string... '.i'
			if .currentName$ <> "directory.txt"
				select sgc.allWordLists
				.listExist = Search column: "Name", .currentName$
				if not .listExist
					Append row
					.sgc.currentWordlistRow = Get number of rows
					Set string value... '.sgc.currentWordlistRow' Name '.currentName$'
			    	.currentDirectory$ = sgc2wordlists$+"/"+.currentName$
					Set string value... '.sgc.currentWordlistRow' Directory '.currentDirectory$'
				endif
			endif
        endfor
        select Strings WordList
        Remove
	endif

	# End with the word lists in the distribution
	call CreateCreateWordlists
	select Table CreateWordlists
    .numLists = Get number of rows
	for .i to .numLists
		select Table CreateWordlists
		.currentName$ = Get value... '.i' Name
		if .currentName$ <> "directory.txt"
			select sgc.allWordLists
			.listExist = Search column: "Name", .currentName$
			if not .listExist
				.procedureName$ = replace_regex$(.currentName$, "[^a-zA-Z0-9_\-\.]", "_", 0)
				select sgc.allWordLists
				Append row
				.sgc.currentWordlistRow = Get number of rows
				Set string value... '.sgc.currentWordlistRow' Name '.currentName$'
				.currentDirectory$ = "*call Create'.procedureName$'"
				Set string value... '.sgc.currentWordlistRow' Directory '.currentDirectory$'
			endif
		endif
	endfor
	select Table CreateWordlists
	Remove
	
	# Finally, the local word lists
	if fileReadable(.localdir$) or fileReadable("'.localdir$'/directory.txt")
    	Create Strings as directory list... WordList '.localdir$'
    	.numLists = Get number of strings
    	for .i to .numLists
            select Strings WordList
            .currentName$ = Get string... '.i'
			if .currentName$ <> "directory.txt"
				select sgc.allWordLists
				.listExist = Search column: "Name", .currentName$
				if not .listExist
					Append row
					.sgc.currentWordlistRow = Get number of rows
					Set string value... '.sgc.currentWordlistRow' Name '.currentName$'
			    	.currentDirectory$ = .localdir$+"/"+.currentName$
					Set string value... '.sgc.currentWordlistRow' Directory '.currentDirectory$'
				endif
			endif
    	endfor
        select Strings WordList
        Remove
    endif

	# Get the position of the current word list
	select sgc.allWordLists
	.currentNumber = 1
    .numLists = Get number of rows

	if wordlistName$ <> ""
		select sgc.allWordLists
		.currentNumber = Search column... Name 'wordlistName$'
		if .currentNumber <= 0
			.currentNumber = 1
		endif
	endif

    wordlistNum = .currentNumber + .relnumber
    if wordlistNum > .numLists
        wordlistNum = 1
	elsif wordlistNum < 1 and .numLists > 0
		wordlistNum = .numLists
    endif
    select sgc.allWordLists
    wordlistName$ = Get value... 'wordlistNum' Name
	.dirWordlistName$ = Get value... 'wordlistNum' Directory
    .dirString$ = replace_regex$(.dirWordlistName$, "[ ]", "&", 0)
	
	# Read in full tables
	if fileReadable("'.dirString$'/wordlist.Table")
		call readTable '.dirString$'/wordlist.Table
    	if readTable.tableID > 0
			Rename... 'wordlistName$'
			# Praat wil change the name if it feels like it
			wordlist$ = selected$("Table")
			# Add a Sound column if it is not present
			.soundIndex = Get column index: sgcwordlist.audio$
			if .soundIndex <= 0
				Append column: sgcwordlist.audio$
			endif
		else
			.numLists = 0
			goto EMERGENCYEXIT
		endif
	# Handle (legacy) simple word lists
	elsif fileReadable("'.dirString$'/wordlist.txt")
		Create Table with column names: wordlistName$, 0, "'sgcwordlist.word$' 'sgcwordlist.graph$' 'sgcwordlist.audio$' 'sgcwordlist.transl$'"
    	wordlist$ = selected$("Table")
		Read Strings from raw text file... '.dirString$'/wordlist.txt
		Rename... RawWordList
		.numWordStrings = Get number of strings
		for .i to .numWordStrings
			select Strings RawWordList
			.currentFile$ = "-"
			.currentChar$ = "-"
			.currentTrans$ = "-"
			.currentLine$ = Get string... '.i'
			# Remove leading whitespace
			.currentLine$ = replace_regex$(.currentLine$, "^[ \t]+", "", 0)
			.separatorIndex = index_regex(.currentLine$, "[ \t;\-]")
			if .separatorIndex <= 0
				.separatorIndex = length(.currentLine$) + 1
			endif
			.currentPinyin$ = left$(.currentLine$, .separatorIndex-1)
			.currentLine$ = right$(.currentLine$, length(.currentLine$) - .separatorIndex)
			# There is more on the line, but we do not know what
			if length(.currentLine$) > 0
				while length(.currentLine$) > 0
					.separatorIndex = index_regex(.currentLine$, "[\t;]")
					if .separatorIndex <= 0
						.separatorIndex = length(.currentLine$)+1
					endif
					.currentItem$ = left$(.currentLine$, .separatorIndex-1)
					.currentLine$ = right$(.currentLine$, length(.currentLine$) - .separatorIndex )
					if index_regex(.currentItem$, ".(spx|flac|wav|mp3)")
						# Audio
						.currentFile$ = .currentItem$
					elsif index_regex(.currentItem$, "[a-zA-Z0-9]") > 0
						# Translation
						.currentTrans$ = .currentItem$
					elsif index_regex(.currentItem$, "[^ \t\r\l]") > 0 && index_regex(.currentItem$, "[a-zA-Z0-9\-]") <= 0
						# Characters
						.currentChar$ = .currentItem$
					endif
				endwhile
			endif
			if .currentFile$ = "-"
				if fileReadable("'.dirString$'/'.currentPinyin$'.spx")
					.currentFile$ = .currentPinyin$+".spx"
				elsif fileReadable("'.dirString$'/'.currentPinyin$'.flac")
					.currentFile$ = .currentPinyin$+".flac"
				elsif fileReadable("'.dirString$'/'.currentPinyin$'.wav")
					.currentFile$ = .currentPinyin$+".wav"
				elsif fileReadable("'.dirString$'/'.currentPinyin$'.mp3")
					.currentFile$ = .currentPinyin$+".mp3"
				endif
			endif
			select Table 'wordlist$'
			Append row
			Set string value: .i, sgcwordlist.word$, .currentPinyin$
			Set string value: .i, sgcwordlist.audio$, .currentFile$
			Set string value: .i, sgcwordlist.graph$, .currentChar$
			Set string value: .i, sgcwordlist.transl$, .currentTrans$
		endfor
		select Strings RawWordList
		Remove
		
		select Table 'wordlist$'
	elsif fileReadable(.dirString$) or fileReadable("'.dirString$'/directory.txt")
		Create Table with column names... "'wordlistName$'" 0 Pinyin Sound
    	wordlist$ = selected$("Table")
		Create Strings as file list... RawWordList '.dirString$'/*
		.numWordStrings = Get number of strings
		.i = 0
		for .j to .numWordStrings
			select Strings RawWordList
			.currentLine$ = Get string... '.j'
			.currentFile$ = extractWord$(.currentLine$, "")
			if index_regex(.currentFile$, "\.(spx|flac|wav|mp3)")
				.currentPinyin$ = left$(.currentFile$, index(.currentFile$, ".")-1)
				select Table 'wordlist$'
				Append row
				.i += 1
				Set string value: .i, sgcwordlist.word$, .currentPinyin$
				Set string value: .i, sgcwordlist.audio$, .currentFile$
			endif
		endfor
		select Strings RawWordList
		Remove
		
		select Table 'wordlist$'
	elsif startsWith(.dirString$, "*call ")
		.callProcedure$ = right$(.dirString$, length(.dirString$)-1)
		'.callProcedure$'
		wordlist$ = selected$("Table")
	else
		# Nothing, get out
		.numLists = 0
		goto EMERGENCYEXIT
	endif

	# Can this wordlist be deleted?
	if fileReadable("'sgc2wordlists$'/'wordlistName$'") or fileReadable("'sgc2wordlists$'/'wordlistName$'/wordlist.txt") or fileReadable("'sgc2wordlists$'/'wordlistName$'/wordlist.Table")
		config.deleteWordlist = 0
	else
		config.deleteWordlist = -1		
	endif

	# Check first column name and add Character, Translation, Sound and Show columns if missing
	if wordlist$ <> ""
		select Table 'wordlist$'
		# Hack to correct odd behavior of tables with unicode characters
		.firstColumn$ = Get column label: 1
		if index_regex(.firstColumn$, "^[^!-~]") > 0
			.firstColumn$ = replace_regex$(.firstColumn$, "^[^!-~]", "", 0)
			Set column label (index): 1, .firstColumn$
		endif
		
    	sgc.numberOfWords = Get number of rows
		.characterColumn = Get column index... Character
		if not .characterColumn
			Append column... Character
			for .i to sgc.numberOfWords
				Set string value: .i, sgcwordlist.graph$, "-"
			endfor
		endif
		.translationColumn = Get column index... Translation
		if not .translationColumn
			Append column... Translation
			for .i to sgc.numberOfWords
				Set string value: .i, sgcwordlist.transl$, "-"
			endfor
		endif
		.translationColumn = Get column index... Sound
		if not .translationColumn
			Append column... Sound
			for .i to sgc.numberOfWords
				Set string value... '.i' Sound -
			endfor
		endif
		.showColumn = Get column index... Show
		if not .showColumn
			Append column... Show
			for .i to sgc.numberOfWords
				Set string value... '.i' Show +
			endfor
		endif
	endif
	
	# Remove all rows without Pinyin
	.numRows = Get number of rows
	for i to .numRows
		.rowNum = .numRows - i + 1
		.pinyinValue$ = Get value... '.rowNum' Pinyin
		.currLength = Get number of rows
		if not index_regex(.pinyinValue$, "^[a-zA-Z]")
			if .currLength > 1
				Remove row: .rowNum
			else
				Set string value: .rowNum, sgcwordlist.word$, "bu4"
			endif
		endif
	endfor
	.numRows = Get number of rows
	sgc.numberOfWords = Get number of rows
	
	# Shuffle words if requested
    if config.shuffleLists
        Randomize rows
    endif

	# Determine number of words actually shown
	sgc.currentWord = 0
	sgc.currentWordNum = 1
	sgc.numberOfDisplayedWords = 0	
	for .i to .numRows
		.show$ = Get value: .i, "Show"
		if .show$ = "+"
			if sgc.currentWord < 1
				sgc.currentWord = .i
			endif
			sgc.numberOfDisplayedWords += 1
		endif
	endfor
	
	# Clean up
	label EMERGENCYEXIT
    #select sgc.allWordLists
    #Remove
	
    # There were no Word Lists
    label NOWORDLISTS
    if .numLists <= 0
        wordlistName$ = wordlistName$+" No Word Lists available"
        wordlistNum = 1
		Create Table with column names: wordlistName$, 1, "'sgcwordlist.word$' 'sgcwordlist.graph$' 'sgcwordlist.audio$' 'sgcwordlist.transl$'"
    	wordlist$ = selected$("Table")
        .i = 0
		Append row
		.i += 1
		Set string value: .i, sgcwordlist.word$, "ni3hao3"
		Set string value: .i, sgcwordlist.graph$, "你好"
		Set string value: .i, sgcwordlist.transl$, "hello"
		Set string value: .i, sgcwordlist.audio$, "-"
		Append row
		.i += 1
		Set string value: .i, sgcwordlist.word$, "xie4xie0"
		Set string value: .i, sgcwordlist.graph$, "谢谢"
		Set string value: .i, sgcwordlist.transl$, "thanks"
		Set string value: .i, sgcwordlist.audio$, "-"
		Append row
 		.i += 1
		Set string value: .i, sgcwordlist.word$, "zai4jian4"
		Set string value: .i, sgcwordlist.graph$, "再见"
		Set string value: .i, sgcwordlist.transl$, "goodbye"
		Set string value: .i, sgcwordlist.audio$, "-"
		# Get rid of empty first row
		Remove row: 1
		
		# Set default values
        sgc.numberOfWords = Get number of rows
		sgc.currentWord = 1
		sgc.numberOfDisplayedWords = Get number of rows
    endif
	
	select Table 'wordlist$'
	sgc.currentWordlist = selected()
	call set_window_title 'buttons$' 'wordlistName$'
endproc

procedure read_wordlist .wordlistName$ .dirString$
	# Read in full tables
	if fileReadable("'.dirString$'/wordlist.Table")
		call readTable '.dirString$'/wordlist.Table
		.wordlistID = selected ()
    	if .wordlistID > 0
			Rename... '.wordlistName$'
			# Praat wil change the name if it feels like it
			.wordlist$ = selected$("Table")
			# Add a Sound column if it is not present
			.soundIndex = Get column index: sgcwordlist.audio$
			if .soundIndex <= 0
				Append column: sgcwordlist.audio$
			endif
		else
			goto EMERGENCYEXITWL
		endif
	# Handle (legacy) simple word lists
	elsif fileReadable("'.dirString$'/wordlist.txt")
		.wordlistID = Create Table with column names: wordlistName$, 0, "'sgcwordlist.word$' 'sgcwordlist.graph$' 'sgcwordlist.audio$' 'sgcwordlist.transl$'"
		.wordlist$ = selected$("Table")
		Read Strings from raw text file... '.dirString$'/wordlist.txt
		Rename... RawWordList
		.numWordStrings = Get number of strings
		for .i to .numWordStrings
			select Strings RawWordList
			.currentFile$ = "-"
			.currentChar$ = "-"
			.currentTrans$ = "-"
			.currentLine$ = Get string... '.i'
			# Remove leading whitespace
			.currentLine$ = replace_regex$(.currentLine$, "^[ \t]+", "", 0)
			.separatorIndex = index_regex(.currentLine$, "[ \t;\-]")
			if .separatorIndex <= 0
				.separatorIndex = length(.currentLine$) + 1
			endif
			.currentPinyin$ = left$(.currentLine$, .separatorIndex-1)
			.currentLine$ = right$(.currentLine$, length(.currentLine$) - .separatorIndex)
			# There is more on the line, but we do not know what
			if length(.currentLine$) > 0
				while length(.currentLine$) > 0
					.separatorIndex = index_regex(.currentLine$, "[\t;]")
					if .separatorIndex <= 0
						.separatorIndex = length(.currentLine$)+1
					endif
					.currentItem$ = left$(.currentLine$, .separatorIndex-1)
					.currentLine$ = right$(.currentLine$, length(.currentLine$) - .separatorIndex )
					if index_regex(.currentItem$, ".(spx|flac|wav|mp3)")
						# Audio
						.currentFile$ = .currentItem$
					elsif index_regex(.currentItem$, "[a-zA-Z0-9]") > 0
						# Translation
						.currentTrans$ = .currentItem$
					elsif index_regex(.currentItem$, "[^ \t\r\l]") > 0 && index_regex(.currentItem$, "[a-zA-Z0-9\-]") <= 0
						# Characters
						.currentChar$ = .currentItem$
					endif
				endwhile
			endif
			if .currentFile$ = "-"
				if fileReadable("'.dirString$'/'.currentPinyin$'.spx")
					.currentFile$ = .currentPinyin$+".spx"
				elsif fileReadable("'.dirString$'/'.currentPinyin$'.flac")
					.currentFile$ = .currentPinyin$+".flac"
				elsif fileReadable("'.dirString$'/'.currentPinyin$'.wav")
					.currentFile$ = .currentPinyin$+".wav"
				elsif fileReadable("'.dirString$'/'.currentPinyin$'.mp3")
					.currentFile$ = .currentPinyin$+".mp3"
				endif
			endif
			select .wordlistID
			Append row
			Set string value: .i, sgcwordlist.word$, .currentPinyin$
			Set string value: .i, sgcwordlist.audio$, .currentFile$
			Set string value: .i, sgcwordlist.graph$, .currentChar$
			Set string value: .i, sgcwordlist.transl$, .currentTrans$
		endfor
		select Strings RawWordList
		Remove
		
		select .wordlistID
	elsif fileReadable(.dirString$) or fileReadable("'.dirString$'/directory.txt")
		.wordlistID = Create Table with column names... "'.wordlistName$'" 0 Pinyin Sound
    	.wordlist$ = selected$("Table")
		.tmp = Create Strings as file list... RawWordList '.dirString$'/*
		.numWordStrings = Get number of strings
		.i = 0
		for .j to .numWordStrings
			select .tmp
			.currentLine$ = Get string... '.j'
			.currentFile$ = extractWord$(.currentLine$, "")
			if index_regex(.currentFile$, "\.(spx|flac|wav|mp3)")
				.currentPinyin$ = left$(.currentFile$, index(.currentFile$, ".")-1)
				select sgc.currentWordlist
				Append row
				.i += 1
				Set string value: .i, sgcwordlist.word$, .currentPinyin$
				Set string value: .i, sgcwordlist.audio$, .currentFile$
			endif
		endfor
		select .tmp
		Remove
		
		select sgc.currentWordlist
	elsif startsWith(.dirString$, "*call ")
		.callProcedure$ = right$(.dirString$, length(.dirString$)-1)
		'.callProcedure$'
		.wordlistID = selected()
		.wordlist$ = selected$("Table")
	else
		# Nothing, get out
		goto EMERGENCYEXITWL
	endif
	
	# Add the path to the file Sound file names
	select .wordlistID
	.numRows = Get number of rows
	for .w to .numRows
		.soundfile$ = Get value: .w, sgcwordlist.audio$
		if index_regex(.soundfile$, "[/\\]") <= 0
			if index_regex(.dirString$, "[/\\]$")
				.soundfile$ = .dirString$+.soundfile$
			elsif index_regex(.dirString$, "[\\]")
				.soundfile$ = .dirString$+"\\"+.soundfile$
			else
				.soundfile$ = .dirString$+"/"+.soundfile$
			endif
			Set string value: .w, sgcwordlist.audio$, .soundfile$ 
		endif
	endfor
	
	label EMERGENCYEXITWL

endproc

procedure merge_into_wordlist .newTableID .lessonPostfix$
	select sgc.currentWordlist
	.oldLessonColumn = Get column index: "Lesson"

	select .newTableID
	.numRows = Get number of rows
	.lessonColumn = Get column index: "Lesson"
	.characterColumn = Get column index: sgcwordlist.graph$
	.soundColumn = Get column index: sgcwordlist.audio$
	.translationColumn = Get column index: sgcwordlist.transl$
	for .w to .numRows
		.currentFile$ = "-"
		.currentChar$ = "-"
		.currentTrans$ = "-"
		.currentLesson$ = "-"
		
		select .newTableID
		.currentPinyin$ = Get value: .w, sgcwordlist.word$
		if .soundColumn > 0
			.currentFile$ = Get value: .w, sgcwordlist.audio$
		endif
		if .characterColumn > 0
			.currentChar$ = Get value: .w, sgcwordlist.graph$
		endif
		if .translationColumn > 0
			.currentTrans$ = Get value: .w, sgcwordlist.transl$
		endif
		if .lessonColumn > 0
			.currentLesson$ = Get value: .w, "Lesson"
		endif
		select sgc.currentWordlist
		Append row
		.n = Get number of rows
		Set string value: .n, sgcwordlist.word$, .currentPinyin$
		Set string value: .n, sgcwordlist.audio$, .currentFile$
		Set string value: .n, sgcwordlist.graph$, .currentChar$
		Set string value: .n, sgcwordlist.transl$, .currentTrans$
		if .oldLessonColumn > 0
			if .currentLesson$ = "-" or .currentLesson$ = "" or .currentLesson$ = "?"
				Set string value: .n, "Lesson", .lessonPostfix$
			else
				Set string value: .n, "Lesson", .currentLesson$+" "+.lessonPostfix$
			endif
		endif
	endfor
endproc

procedure next_word
	if wordlist$ <> ""
		select sgc.currentWordlist
		.showCurrent$ = "-"
		if sgc.currentWord < 0 or sgc.currentWord > sgc.numberOfWords
	        if config.shuffleLists
			    Randomize rows
	        endif
			sgc.currentWord = 0
			sgc.currentWordNum = 0
		endif
		while .showCurrent$ = "-" and sgc.currentWord <= sgc.numberOfWords
			sgc.currentWord += 1
			if sgc.currentWord <= sgc.numberOfWords
				.showCurrent$ = Get value... 'sgc.currentWord' Show
			endif
		endwhile
		if sgc.currentWord > 0
			sgc.currentWordNum += 1
		else
			sgc.currentWordNum = 0
		endif
	endif
endproc

procedure previous_word
	if wordlist$ <> ""
		select sgc.currentWordlist
		.showCurrent$ = "-"
		if sgc.currentWord <= 0
	        if config.shuffleLists
			    Randomize rows
	        endif
			sgc.currentWord = sgc.numberOfWords + 1
			sgc.currentWordNum = sgc.numberOfDisplayedWords + 1
		endif
		while .showCurrent$ = "-" and sgc.currentWord > 0
			sgc.currentWord -= 1
			if sgc.currentWord > 0
				.showCurrent$ = Get value... 'sgc.currentWord' Show
			endif
		endwhile
		if sgc.currentWord > 0
			sgc.currentWordNum -= 1
		else
			sgc.currentWordNum = 0
		endif
	endif
endproc

procedure display_word_list_name
    .xtext = 50
    .ytext = 12
   call reset_viewport
    .displayWordList$ = replace_regex$(wordlistName$, "[_]", " ", 0)
    call wipeArea 'wipeWordlistArea$'
	call adjustFontSizeOnHeight 'defaultFont$' 'defaultFontSize' 5
	.currentFontSize = adjustFontSizeOnHeight.newFontSize

    demo Blue
	demo Text special... '.xtext' Centre '.ytext' Bottom 'defaultFont$' '.currentFontSize' 0 '.displayWordList$'
    demo Black
	demoShow()
	call set_font_size 'defaultFontSize'
endproc

procedure write_word_list
	# Write current Pinyin text
	call display_text Black
	
	# Write the current word list name
	call display_word_list_name
endproc

procedure paint_saveAudio_light
    select Table 'config$'
    .row = Search column... Label SaveAudio
	if .row < 1
		exit Button Table Config does not have a row with label SaveAudio
	endif
	# Get button values
    .leftX = Get value... '.row' LeftX
    .rightX = Get value... '.row' RightX
    .lowY = Get value... '.row' LowY
    .highY = Get value... '.row' HighY
    .buttonColor$ = Get value... '.row' Color
    # The button text and symbol
	.horWC = demo Horizontal mm to wc... 10.0
	.verWC = demo Vertical mm to wc... 10.0
	if .verWC > 0
		.verCoeff = .horWC / .verWC
	else
		.verCoeff = 1
	endif

    .centerX = (.leftX + .rightX)/2
    .centerY = .lowY + 0.6*(.highY-.lowY)
    .radius = min(.verCoeff * (.highY - .lowY ), (.rightX - .leftX))/3
    .wipeRadius = 1.1*.radius
    call DrawSaveAudio White '.centerX' '.centerY' '.wipeRadius'

    if sgc.saveAudioOn and sgc.saveAudio$ <> ""
		call DrawSaveAudio "DarkBlue" '.centerX' '.centerY' '.radius'
    endif
    demoShow()
endproc

# Uninstall word lists
procedure removeWordlist .deletedWordlistName$
    .targetDir$ = ""
    if fileReadable("'sgc2wordlists$'/'.deletedWordlistName$'") or fileReadable("'sgc2wordlists$'/'.deletedWordlistName$'/wordlist.txt") or fileReadable("'sgc2wordlists$'/'.deletedWordlistName$'/wordlist.Table")
        .targetDir$ = "'sgc2wordlists$'/'.deletedWordlistName$'"
    endif
	if .targetDir$ <> ""
    	Create Strings as file list... DeleteList '.targetDir$'
    	.numdeleteFiles = Get number of strings
    	for .i to .numdeleteFiles
        	.file$ = Get string... '.i'
        	deleteFile("'.targetDir$'/'.file$'")
    	endfor
    	filedelete '.targetDir$'
    	# Check whether the directory was actually deleted. Make sure this is a valid directory name!
    	# That is, it does not contain funny characters, nor funny names
    	if index_regex(.deletedWordlistName$, "[^a-zA-Z0-9_ .\-]") <= 0 and index(.deletedWordlistName$, "..") <= 0 and index_regex(.deletedWordlistName$, "[a-zA-Z]") > 0
			if windows
				nocheck system rmdir "'.targetDir$'" /s /q
			elsif fileReadable(.targetDir$)
				system bash -rc -- 'rm -r -- "'.targetDir$'"'
			endif
		endif
		# Remove deleted word list
		select Strings DeleteList
		plus Table 'wordlist$'
		Remove
		wordlist$ = ""
	endif
endproc

# Install word lists
procedure sgc2wordlist .sourceDir$
	if startsWith(.sourceDir$, "preferencesDirectory$")
		.sourceDir$ = replace$(.sourceDir$, "preferencesDirectory$", preferencesDirectory$)
	endif

	.targetDirectory$ = "'sgc2wordlists$'"
	if  fileReadable(.sourceDir$) or fileReadable("'.sourceDir$'/directory.txt")
		Create Strings as file list... PackageList '.sourceDir$'/*.sgc
		.numFiles = Get number of strings
		for .i to .numFiles
			select Strings PackageList
			.file$ = Get string... '.i'
			call readWordlist '.sourceDir$' '.file$'
		endfor

		select Strings PackageList
		Remove
	endif
endproc

# Debuggin remarks!!!
# fileReadable(<directory>) does not work in Windows
# The file paths / -> \\ must be performed on the filenames in Windows before the system_nocheck is given
# And yet only the 7z decompression has been implemented
windowsUnzipCommand$ = ""
if fileReadable("C:\Program Files\7-Zip\7z.exe")
	windowsUnzipCommand$ = """C:\Program Files\7-Zip\7z.exe"" -y e"
	windowsUnzipDestDir$ = " -o"
elsif fileReadable("C:\Program Files\WinZip\winzip32.exe")
	# !!! Find a way to include the output folder !!!
	windowsUnzipCommand$ = "C:\Program Files\WinZip\winzip32.exe"" -min -e -o -j "
	windowsUnzipDestDir$ = ""
endif
procedure readWordlist .sourceDir$ .file$
	# No use doing anything if the source does not exist
	if fileReadable("'.sourceDir$'/'.file$'") or fileReadable("'.sourceDir$'/'.file$'/wordlist.txt") or fileReadable("'.sourceDir$'/'.file$'/wordlist.Table")
		# What will be the target wordlist directory?
		.targetDirectory$ = "'sgc2wordlists$'"
		.dirname$ = left$(.file$, rindex(.file$, ".")-1)
		if .dirname$ = ""
			.dirname$ = .file$
		endif
		.wordlistDirectory$ = .targetDirectory$+"/"+.dirname$
		# Wordlist directory does not exist, neither locally nor in the preferences
		.wordListExists = 0
		.tmpDirs = nocheck Create Strings as directory list... TMPWORDLISTS '.targetDirectory$'/
		if .tmpDirs != undefined and .tmpDirs > 0
			.numDirs = Get number of strings
			for .d to .numDirs
				select .tmpDirs
				.currentString$ = Get string... '.d'
				if .currentString$ = .dirname$
					.wordListExists = 1
				endif
			endfor
			Remove
		endif
		if not (.wordListExists or fileReadable(.dirname$) or fileReadable("'.dirname$'/directory.txt") or fileReadable(.wordlistDirectory$) or fileReadable("'.wordlistDirectory$'/directory.txt"))
			.wasWordList = 0
			# Move source to destination
			# Use this if you want to allow direct loading of ZIP files. Could lead to problems.
			# if index(.file$, ".sgc") or index(.file$, ".zip")
			# 
			if index(.file$, ".sgc")
				if index_regex(.wordlistDirectory$, "[^a-zA-Z0-9_\.\- /~:]") <= 0 and index_regex(.dirname$, "[^a-zA-Z0-9_\.\- ]") <= 0 and not (windows and windowsUnzipCommand$ = "")
					# Create wordlist directory
					createDirectory(.wordlistDirectory$)
					.wasWordList = 1
					if macintosh or unix
						system bash -rc -- 'unzip -j "'.sourceDir$'/'.file$'" -d "'.wordlistDirectory$'"'
					elsif windows and windowsUnzipCommand$ <> ""
						.winWordListDirectory$ = replace_regex$(.wordlistDirectory$, "/", "\\", 0)
						.winSourceDirectory$ = replace_regex$("'.sourceDir$'\'.file$'", "/", "\\", 0)
						system mkdir "'.winWordListDirectory$'" & 'windowsUnzipCommand$' "'.winSourceDirectory$'" 'windowsUnzipDestDir$'"'.winWordListDirectory$'"
					endif
				elsif (windows and windowsUnzipCommand$ = "")
					# Warn to install 7Zip
					call get_feedback_text 'config.language$' InstallUnzip
					call convert_praat_to_latin1 'get_feedback_text.text$'
					.zipText$ = convert_praat_to_latin1.text$
					call write_text_popup 'defaultFont$' 14 '.zipText$'
					# Wait for confirmation
					demoWaitForInput()
					call Draw_config_page
				endif
				# Remove if not valid!
				if fileReadable("'.wordlistDirectory$'/wordlist.Table") or fileReadable("'.wordlistDirectory$'/wordlist.txt") or fileReadable("'.wordlistDirectory$'/LICENSE.txt")
					if fileReadable("'.wordlistDirectory$'/wordlist.Table")
						call readTable '.sourceDir$'/'.file$'
						if readTable.tableID > 0
							select readTable.tableID
							# Hack around odd behavior of column index
							.pinyinCol = 0
							.firstColumn$ = Get column label: 1
							.firstColumn$ = replace_regex$(.firstColumn$, "^[^!-~]", "", 0)
							if .firstColumn$ = sgcwordlist.word$
								.pinyinCol = 1
							else
								.pinyinCol = Get column index... Pinyin
							endif
							Remove
							# No Pinyin in table
							if .pinyinCol <= 0
								.wasWordList = 0
							endif
						else
							.wasWordList = 0
						endif
					endif
				else
					# None of wordlist.Table, wordlist.txt, nor LICENSE.txt
					.wasWordList = 0
				endif
				### REALLY DANGEROUS STUFF, SHOULD BE HANDLED BETTER
				if .wasWordList = 0
					# Remove newly created directory
					if index_regex(.wordlistDirectory$, "[^a-zA-Z0-9_\.\- /~:]") <= 0 and index_regex(.dirname$, "[^a-zA-Z0-9_\.\- ]") <= 0 and index(.dirname$, "..") <= 0
						if macintosh or unix
							system bash -rc -- 'rm -r "'.wordlistDirectory$'/"'
						elsif windows
							system rmdir /Q /S "'.winWordListDirectory$'/"
						endif
					endif
				endif
			elsif index_regex(.file$, "\.(Table|tsv|csv)")
				# Check whether this is a valid table
				call readTable '.sourceDir$'/'.file$'
				if readTable.tableID > 0
					select readTable.tableID
					# Hack around odd behavior of column index
					.pinyinCol = 0
					.firstColumn$ = Get column label: 1
					.firstColumn$ = replace_regex$(.firstColumn$, "^[^!-~]", "", 0)
					if .firstColumn$ = sgcwordlist.word$
						.pinyinCol = 1
					else
						.pinyinCol = Get column index... Pinyin
					endif
					
					if .pinyinCol > 0
						select readTable.tableID
						# Create wordlist directory
						createDirectory(.wordlistDirectory$)
						.wasWordList = 1
						.extension$ = "Table"
						.wordlistFilePath$ = "'.wordlistDirectory$'/wordlist.'.extension$'"
						if windows
							.wordlistFilePath$ = replace_regex$(.wordlistFilePath$, "/", "\\", 0)
						endif
						# Save
						select readTable.tableID
						Save as tab-separated file: .wordlistFilePath$
						if not fileReadable(.wordlistFilePath$)
							.wasWordList = 0
						endif
					endif
					select readTable.tableID
					Remove
				endif
			elsif index_regex(.file$, "\.(?itxt)")
				# Check whether this is a valid table
				readTable.tableID = nocheck Read Strings from raw text file: "'.sourceDir$'/'.file$'"
				if readTable.tableID > 0
					.wasWordList = 1
					# Create wordlist directory
					createDirectory(.wordlistDirectory$)
					.extension$ = "txt"
					.wordlistFilePath$ = "'.wordlistDirectory$'/wordlist.'.extension$'"
					if windows
						.wordlistFilePath$ = replace_regex$(.wordlistFilePath$, "/", "\\", 0)
					endif
					select readTable.tableID
					Save as raw text file: .wordlistFilePath$
					select readTable.tableID
					Remove
				endif
			elsif fileReadable("'.sourceDir$'/'.file$'/wordlist.Table") or fileReadable("'.sourceDir$'/'.file$'/wordlist.txt") or fileReadable("'.sourceDir$'/'.file$'/LICENSE.txt")
				# Copy wordlist directory
				if index_regex(.file$, "[^a-zA-Z0-9_\.\- ]") <= 0
					.wasWordList = 1
					if macintosh or unix
						system bash -rc -- 'cp -r "'.sourceDir$'/'.file$'" "'.wordlistDirectory$'"'
					elsif windows
						createDirectory(.wordlistDirectory$)
						.winWordListDirectory$ = replace_regex$(.wordlistDirectory$, "/", "\\", 0)
						.winSourceDirectory$ = replace_regex$("'.sourceDir$'\'.file$'", "/", "\\", 0)
						system xcopy "'.winSourceDirectory$'" "'.winWordListDirectory$'" /s /e /q
					endif
				endif
			endif
			
			# Set current word list to read list
			if .wasWordList
				wordlistName$ = .dirname$
			else
				.table$ = "Config"
				.label$ = "!NotAWordlist"

				# Get help text
				call findLabel '.table$' '.label$'
				.row = findLabel.row
				select Table '.table$'
				.helpText$ = Get value... '.row' Helptext
				.printablePath$ = "'.sourceDir$'/'.file$'"
				if windows
					.printablePath$ = replace$("'.sourceDir$'\'.file$'", "\", "\bs", 0)
				endif
				.filetext$ = replace_regex$(.printablePath$, "_", "\\_ ", 0)
        		call write_text_popup 'defaultFont$' 14 '.helpText$' "'.filetext$'"
				# Wait for confirmation
				demoWaitForInput()
			endif
		endif
	endif
endproc
