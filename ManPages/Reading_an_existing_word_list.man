ManPagesTextFile
"Reading an existing word list" "R.J.J.H. van Son" 20140901 0
<intro> "New word lists can be downloaded or @@Creating a new word list|created@. "

<entry> "Reading a word list"
<normal> "Go to the %%Settings page%. At the lower left hand side, there is a button with the label %%Open List%. 
After you click on this button, you can select the file with the word list. "

<normal> "Plain text wordlists (e.g., %%<name>.txt%) or full tables (e.g., %%<name>.Table% or %%<name>.tsv%) can directly be opened using the %%Open List% button on the %%Settings% page."

<normal> "When reading wordlists with included audio, users on %%OSX% (Mac) and %%Linux% should select the standard wordlists 
with the %%.sgc% extension for %%SpeakGoodChinese%. These are just standard ZIP files, but the %%.sgc% extension makes them 
easier to handle in %%SpeakGoodChinese%. "

<normal> "If you have 7-Zip (%%7-Zip.org%) or Winzip installed on MS Windows, the automatic installation of %%.sgc% files might work too."

<normal> "Windows users who cannot load the standard wordlists should download the %%ZIP% version and unpack the wordlist ZIP, 
i.e., extract the folder inside. Often this can be done by a double click on the downloaded file and 
then dragging the folder inside it to the place where you want to store it. "

<normal> "If the folder extracted from the %%ZIP% file contains scripts with the name(s) %%Windows_install.bat%, %%Mac_install.command% 
and %%Linux_install.sh%, these will do the installation for you. Just double click on the appropriate install script. The wordlists and 
if available, the %%SpeakGoodChinese% binary will be installed. "

<normal> "If the install scripts are not present, click on the %%Open List% button on the %%Settings% page and in the folder created after unpacking the ZIP file, locate the folders that store the wordlists (and audio). Open %%one% (and only %%one%) of the 
files in each wordlist folder. The wordlist folders can be located inside a separate %%wordlists% folder. Each separate wordlist folder will 
have to be loaded by opening a file inside it. It does not really matter which file is chosen 
(e.g., %%<name>.wav% or %%wordlist.txt% or %%wordlist.Table%). If all goes well, the wordlist will be loaded. This will 
work on %%OSX% (Mac) and %%Linux% too. You can delete the files you downloaded after opening the wordlist."

<entry> "Global installation of word lists"

<normal> "The wordlists a user installs are placed in her local settings directory:"
<list_item> "\bu MS Windows: %%C:\bsDocuments and Settings\bs<User name>\bsSpeakGoodChinese2\bswordlists%"
<list_item> "\bu Mac OSX: %%/Users/<User name>/Library/Preferences/SpeakGoodChinese2/wordlists%"
<list_item> "\bu Linux: %%/home/<User name>/.SpeakGoodChinese2/wordlists%"

<normal> "To install wordlists for all users, place them in:"
<list_item> "\bu MS Windows: %%C:\bsDocuments and Settings\bsAll Users\bsApplication Data\bsSpeakGoodChinese2\bswordlists%"
<list_item> "\bu Mac OSX: %%/Library/Preferences/SpeakGoodChinese2/wordlists%"
<list_item> "\bu Linux: %%/etc/SpeakGoodChinese2/wordlists%"

<entry> "Contents:"
<list_item> "\bu @@Creating a new word list@"
