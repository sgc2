ManPagesTextFile
"SpeakGoodChinese 2" "R.J.J.H. van Son" 20140901 0
<intro> "Speak Good Chinese/说好中文: A tool to practice Mandarin pronunciation"

<entry> "SpeakGoodChinese"
<normal> "Speak Good Chinese is a cross-platform application based on %%Praat% technology that allows you or your students to practice their Mandarin pronunciation. "
<normal> "SpeakGoodChinese 2.0 can use word lists in plain pinyin text form, and with Chinese characters, translations, and audio examples. 
The integrated eSpeak Mandarin Speech Synthesizer will generate example pronunciations when live audio examples are not available. 
See the Wordlists entry below for examples. SpeakGoodChinese 2.0 can be extended with standard speech synthesis applications. 
SpeakGoodChinese 2.0 integrates the free (FOSS) eSpeak Mandarin speech synthesis, but can also use other speech synthesis applications. "

<entry> "Contents:"
<list_item> "\bu @@Configuring practice sessions@"
<list_item> "\bu @@Reading an existing word list@"
<list_item> "\bu @@Creating a new word list@"
<list_item> "\bu @@Monitoring progress@"
<list_item> "\bu @@Speech Synthesis in SpeakGoodChinese@"
<list_item> "\bu @@SpeakGoodChinese Translation Guide@"
<list_item> "\bu @@What's new in SpeakGoodChinese?@"
