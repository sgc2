ManPagesTextFile
"Speech Synthesis in SpeakGoodChinese" "R.J.J.H. van Son" 20150916 0
<intro> "How to change the pinyin synthesis"

<entry> "Tone  and Speech synthesis in SpeakGoodChinese"
<normal> "%%SpeakGoodChinese% has three levels of tone and speech synthesis 
beside the option to add real spoken examples (%%Real examples% button). 
Which synthesis method is used is choosen on the %%Settings% page:"
<list_item> "\bu Humming tones - 
Deselect the %%Synthesis% and %%Real example% buttons"
<list_item> "\bu Rule-based Text-to-Speech (%%TTS%) synthesis (%%eSpeak%) -  
Select the %%Synthesis% button and select a %%Voice% other than ##*#"
<list_item> "\bu Native Text-to-Speech (%%TTS%) synthesis - Select the ##*# %%Voice%"
<normal> "The ""Humming"" option will stress the tone realizations. 
The two TTS options will also practice pronunciation, 
or at least phoneme perception. 
The phoneme pronunciation of the native Windows and Mac %%TTS% is much 
better than the build-in, rule-based, %%eSpeak% synthesis. 
However, the tone realizations of the high quality native %%TTS% are probably 
suboptimal for beginning students trying to practice tones. 
It is probably best when students switch to the native synthesis on 
Mac or Windows only when they have reached a basic level of tone perception. 
So, the prefered order is likely for students to start with the build-in 
%%eSpeak% voices or even the humming synthesis."

<entry> "Text-To-Speech synthesis in SpeakGoodChinese"
<normal> "SpeakGoodChinese uses the %%eSpeak% Text-To-Speech (%%TTS%) 
engine that is build into Praat. %%eSpeak% has several Mandarin voices. 
You cycle through them when you click on the %%Voice% button on the 
%%Settings% page. Voices that start with an %%F% are supposed to be 
""female"" voices, those starting with an %%M% are supposed to be ""male"" voices. "
<normal> "The voice labled ""*"" is the system's default synthesizer. 
On a Macintosh computer, this is the %%Say% program. On Linux this will 
be the %%spd-say% speech dispatcher, if it is installed. The Macintosh %%Say% 
program has a rather good synthesis quality. The Linux %%spd-say% program 
is more like another %%eSpeak% instance."

<entry> "Installing Chinese voices on the Macintosh (OSX)"
<normal> "If the ""*"" voice does not work on your Macintosh computer, 
you probably do not have the correct Mandarin voice installed. 
To install Mandarin speech synthesis on you Macintosh, open 
%%System Preferences...%. Locate and open the %%Dictation & Speech% item. 
Select the %%Text to Speech% tab. Click on the %%System voice% menu. 
At the bottom of the %%System voice% menu, select %%Customize...% and 
scroll down until you find %%Chinese (China)% and tick the box for 
%%Ting-Ting%."
<normal> "Your computer will download and install the selected voices. 
This can take a lot of time."

<entry> "Installing Chinese voices on Windows"
<normal> "If the ""*"" voice does not work on your Windows computer, 
you probably do not have the correct Mandarin voice installed. The 
installation process is different for Windows 7, 8, and 10. Please refer 
to the procedure for your system. When you select a Mandarin Chinese 
voice, make sure you install the %%simplified characters% and a Chinese 
%%zh-CN% voice! %%SpeakGoodChinese% uses a female voice. 
%%SpeakGoodChinese% is known to work with the %%Huihui% voice."

<entry> "Changing to another speech synthesis program"
<normal> "%%SpeakGoodChinese% can use external Text-To-Speech synthesis 
programs as long as they have a %%command-line interface% (%%CLI%). 
The command to call this program on the computer must be stored in a 
text file in the %%TTS% folder in the %%Preferences% directory. "
<normal> "%%Note: This has not yet been tested on Windows.%"

<normal> "First, locate the %%Preferences% directory on the computer. "

<normal> "For a single user %%<User name>%, the local %%Preferences% directory is:"
<list_item> "\bu MS Windows: %%C:\bsDocuments and Settings\bs<User name>\bsSpeakGoodChinese2\bsTTS%"
<list_item> "\bu Mac OSX: %%/Users/<User name>/Library/Preferences/SpeakGoodChinese2/TTS%"
<list_item> "\bu Linux: %%/home/<User name>/.SpeakGoodChinese2/TTS%"

<normal> "To install this for all users, place them in the global %%Preferences% directory:"
<list_item> "\bu MS Windows: %%C:\bsDocuments and Settings\bsAll Users\bsApplication Data\bsSpeakGoodChinese2\bsTTS%"
<list_item> "\bu Mac OSX: %%/Library/Preferences/SpeakGoodChinese2/TTS%"
<list_item> "\bu Linux: %%/etc/SpeakGoodChinese2/TTS%"

<normal> "The name of the text file containing the %%TTS% command depends on the computer platform:"
<list_item> "\bu MS Windows: %%TTS_WINDOWS_eSpeak_command.txt%"
<list_item> "\bu Mac OSX: %%TTS_OSX_eSpeak_command.txt%"
<list_item> "\bu Linux: %%TTS_UNIX_eSpeak_command.txt%"

<normal> "The command file should only contain a single line of text 
with the command to speak the text. The commands to call the TTS synthesis 
should be constructed in such a way that a pinyin string can be appended at 
the end. Note that the neutral tone will be indicated by the numeral %%5%."

<normal> "Often the (pinyin) words need to be placed at another position 
in the command string or the command even needs Chinese characters or 
expanded pinyin. Then the place of the pinyin or text should be marked 
by a placeholder: "
<list_item> "\bu ##\& \& \& #: will be replaced by the pinyin string."
<list_item> "\bu ##\@ \@ \@ #: will be replaced by an expanded pinyin string 
(Windows style) %%ni3hao3% becomes %%ni 3 - hao 3%."
<list_item> "\bu ##\$ \$ \$ #: will be replaced by the Chinese characters (not recommended)."

<normal> "Examples of such commands are:"
<list_item> "\bu Mac OSX: $$say -vTing-Ting$"
<list_item> "\bu Windows: $$mshta.exe vbscript:Execute(""CreateObject(""""SAPI.SpVoice"""").Speak
""""<voice gender='female' xml:lang='zh-CN'><rate speed='-5'>
<pron sym='\@ \@ \@ '/></rate></voice>"""":Close"")$"
<list_item> "\bu Linux: $$spd-say -l zh -t female1 -r -60$"

<normal> "While such a file is present for the operating system running 
%%SpeakGoodChinese%, the program will only use that to synthesize speech. "

