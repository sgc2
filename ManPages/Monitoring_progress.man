ManPagesTextFile
"Monitoring progress" "R.J.J.H. van Son" 20150107 0
<intro> "Keeping track of excecises and evaluating pronunciation. "

<entry> "Monitoring practicing"
<normal> "%%SpeakGoodChinese% keeps a table with practice statistics for each session. 
It tabulates which words and phrases have been practiced, how often they have been practiced 
and what the results were. This table can be inspected on the 
%%Settings page% under %%Overview% with the %%Performance% button. 
These data are lost between sessions."
      
<normal> "The tabulated practice statistics can be stored with the %%Save% button 
and loaded again with the %%Read% button. The extension of this file must 
be %%.tsv%. This file can be used to keep track of the words and phrases that have 
been practiced and how often they have been practiced between sessions."

<entry> "Monitoring and evaluating pronunciation"

<normal> "SpeakGoodChinese can store recorded exercises for pronunciation evaluation. On the 
%%Settings page%, press the %%Save Audio% button and select a folder 
to store the recordings. A blue dot will be displayed in the lower right corner of 
the %%Main page% as long as recordings are stored. While the blue dot is visible on the 
%%Main page%, all audio will be stored. Only the last version of each pinyin word will be 
kept. The recorded files have file-names constructed of the pinyin spelling of the word 
and the extension %%.wav%. Every recording will overwrite existing recordings of the same 
pinyin word. The storing of new recordings can be terminated by pressing the %%Save Audio% button 
again. When available, %%SpeakGoodChinese% will display and play the stored recordings 
for evaluation until a new one is recorded. The recordings are available even when the 
%%Save Audio% button has been deactivated."

<normal> "Practice statistics are automatically stored together with the speech recordings. 
During the time the %%Save Audio% button is active, practice tables cannot be manipulated 
(i.e., saved, read, or cleared) and the relevant buttons will be greyed out. "

<entry> "Grading"

<normal> "For evaluation and grading purposes, a student can upload a folder with stored 
practice recordings and a corresponding practice summary. When the teacher opens the student's 
practice summary in %%SpeakGoodChinese% using the %%Read% button, she can listen to 
the audio recordings using the %%Play% button while she steps through the list. 
A grading can be entered by pressing one of the number keys 1-0 (%%0% translates to %%10%). 
This grading will be stored in the practice summary. Gradings will be displayed in the comment 
area below the Pinyin. The average grading is visible as a small, 
blue numeral in the bottom right corner of the Main page. "

<normal> "The file with the practice summary can be send to the student 
(e.g., %%Save% to a suitable folder). When the student opens this practice file, 
and the file is not stored in its original folder, a window will open to ask for 
the associated directory with the recordings. The student can then evaluate her 
performance grading."
<normal> "%%Note: Do% not %%change the name of the file!% 
SpeakGoodChinese expects the practice summary to have the same name as the folder. "

<entry> "Debugging and testing"

<normal> "For testing SpeakGoodChinese there is a special mode where the recognition results 
of stored practice recordings can be displayed in the grading mode. To achieve this, add 
a file called %%TestSpeakGoodChinese2.txt% to the directory which contains the practice 
recordings. "
