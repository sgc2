ManPagesTextFile
"Configuring practice sessions" "R.J.J.H. van Son" 20140901 0
<intro> "Configure the display and examples during practicing"
<entry> "Selecting Lessons and words to practice"
<normal> "Students can select the words to practise on the Settings page. A wordlist can contain words from several lessons. After pressing the %%Select words% button at the bottom of the %%Settings page%, a window will appear where the words of the current wordlist are displayed. The words are sorted and displayed with lesson numbers (if available) in the word selection window. Students can select to practice only words from individual lessons, but they can also select only words with certain tones. In this %%Select words% window, individual words can be selected or %%de-%selected. Use the > and <  buttons to go to the next or previous page of words from the current wordlist. "
<normal> "%%Note: These selections are not preserved when the wordlist is closed%."

<normal> "Instead of manually selecting words to practice, there is an %%Adaptive% option. When the %Adaptive% button is activated, words that were correctly pronounced in the first two trials will not be presented another time for practice. A student can concentrate on those words that present problems. This option is part of the word selection feature above. It can be undone with the %%Select words% option. "

<entry> "Practicing using audio examples, pinyin, characters, and translations"
<normal> "The text that is displayed for each word or phrase during practicing 
can be selected on the %%Settings% page. Under the heading %%View and Sound%, 
provided they are available in the wordlist, you can select any combination of:"
<list_item> "\bu Either %%Tone Numbers% or %%Pinyin% representation"
<list_item> "\bu Chinese %%Characters%"
<list_item> "\bu Translation"
<normal> "When practicing, the ""hidden"" text can be displayed by clicking on the text area. The whole string 
of %%Pinyin%, %%Character%, and %%Translation% (when available) will be written 
in red. This will disappear again when the window is redrawn (use the %%Refresh% button or 
press the space bar)."
<normal> "The use of audio under the %%Example% button (Main page) is 
controlled with the two next buttons under the %%View and Sound% heading. 
To use any real audio examples available, press %%Real example%. 
To use the synthetic voice if no real example is available or selected, 
press %%Synthesis%. When both %%Synthesis% and 
%%Real example% are %%de%selected, only a humming sound is used 
for the tones."
<normal> "The word lists are shuffled before each presentation, unless the %%Shuffle% 
button is deselected. Shuffling is done before each round, and after selecting lessons 
or words using the %%Select words% option. When the %%Shuffle% button is 
%%de%selected, words will be presented in the last order used."

<entry> "Proficiency level"
<normal> "The difficulty of the pronunciation can be set with the %%Proficiency Level% button on the %%Settings% page. This setting runs from %%Level% 0 to 3. At %%Level% 0, the recognizer is very lenient. At %%Level% 3, the recognizer is very strict. Students should select a level that fits their proficiency.  The current recognizer will have more troubles recognizing longer phrases. So, it might be necessary to decrease the %%Proficiency Level% when a wordlist contains many long phrases, i.e., phrases with more than two syllables. "
