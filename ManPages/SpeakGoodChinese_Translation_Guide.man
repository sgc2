ManPagesTextFile
"SpeakGoodChinese Translation Guide" "R.J.J.H. van Son" 20150824 0
<intro> "Convert the User Interface of SpeakGoodChinese to your language."

<entry> "Introduction"
<normal> "SpeakGoodChinese has been designed to allow straightforward translation of the User Interface. All texts seen by the user have been isolated in separate tables and text files. These files have names that end in a language code name. All files are plain text (ASCII and  Unicode texts), using the character and formating codes of Praat."
<normal> "The SpeakGoodChinese user interface contains two main screens, or pages: The Main page and the Config, or Settings, page. Both contain buttons with text labels and associated short help texts. The Main page also will print out other information called Feedback. Feedback on tone pronunciation is contained in the ToneFeedback."
<normal> "In addition to these primary files, there are some other specialized page files: Credits (About page) and a Manual."
<normal> "We suggest translations start with the user interface files describing the Main and Config pages, as these are crucial for the use of SpeakGoodChinese, followed by the Evaluation, the Feedback and the ToneFeedback texts."
<normal> "Translation principles"


<normal> "The basic principle in the translations is to keep to common use of technical terms. If a certain term has common use as a menu option in the source language version of a widely used application, e.g., MS Windows or Firefox, a translator should use the corresponding term as used in the translated version of that application in the target language."

<entry> "Language naming standard"
<normal> "Translations are named after their shortest ISO 639-1 codes, or using one of the longer codes (ISO 693-2/3) when a language is not in the ISO 639-1 list. Use UPPERCASE letters. Each language specific file ends in “_<ISO 639-1 code>”."

<normal> "Examples are:"
<list_item> "\bu %%MainPage_EN% (English)"
<list_item> "\bu %%Config_DE% (German)"
<list_item> "\bu %%Feedback_NL% (Dutch)"
<list_item> "\bu %%ToneFeedback_JA% (Japanese)"
<list_item> "\bu %%Evaluation_ZH% (Chinese)"
<list_item> "\bu %%ToneFeedback_ES% (Spanish, not yet available)"


<entry> "How to obtain existing tables"
<normal> "Spreadsheet versions of the existing tables can be downloaded from the SpeakGoodChinese Google Drive. However, you can export the existing tables from the SpeakGoodChinese application itself."
<normal> "On the Settings page, click the Export Words button. Select the directory where you want the table to be saved. Then enter as a filename any of the existing table names with extension %%.Table% or %%.tsv%. That could be %%Config_ZH.Table%, %%ToneFeedback_EN.tsv%, or %%MainPage_NL.tsv%. SpeakGoodChinese will export the table and save it to the directory indicated."
<normal> "Note that SpeakGoodChinese uses the %%.Table% extension, but spreadsheet programs can often better handle files with the %%.tsv% extension."


<entry> "Main page"
<normal> "The labels used on the Main page are stored in a four column table like those of the Config page. The first row contains the column names. The first three rows, all starting with a ! are for internal use. The first column, Label, is for internal use (it identifies the button). Do not change these."
<normal> "The second column, Text, contains the button labels shown to the user. The font size is adapted to fit the label into the button area. This means that long labels can end up too small to be readable."
<normal> "The third column, Key, contains the keyboard shortcut keys. These should be unique and, preferably, also part of the label of that button. When displaying the button text, the shortcut character will be printed in italic if it is part of the text. So, in English, the shortcut key for the button with the Record text label is chosen as R, and will be printed italic."
<normal> "The fourth column, Helptext, contains a short phrase that describes the use or function of the button. It will be shown if Help is clicked. The shortcut character(s) are automatically added to the help text when it is displayed. "


<entry> "Config or Settings page"
<normal> "The labels used on the Config page are stored in a four column table like those of the Main page. The first row contains the column names. The first three rows, all starting with a ! are for internal use. The first column, Label, is for internal use (it identifies the button). Do not change these."
<normal> "The second column, Text, contains the button labels shown to the user. The font size is adapted to fit the label into the button area. This means that long labels can end up too small to be readable."
<normal> "The third column, Key, contains the keyboard shortcut keys. These should be unique and, preferably, also part of the label of that button. When displaying the button text, the shortcut character will be printed in italic if it is part of the text. So, in English, the shortcut key for the button with the Pinyin text label is chosen as P, and will be printed italic."
<normal> "The fourth column, Helptext, contains a short phrase that describes the use or function of the button. It will be shown if Help is clicked. The shortcut character(s) are automatically added to the help text when it is displayed. "

<normal> "There will be at least one line of the form:"
<code> "Language_EN   English E  English language version"
<normal> "Lines of this form should not be translated, as they define the language buttons. Obviously, students should find the label on the button indicating their own language in that language. For a new language, you have to change the language code from EN, or the code used in your source table, to the code of the target language. The name of the language should be entered instead of English. And the phrase “English language version” should be translated into a phrase indicating the target language. For instance, for Chinese as a target language, the line reads:"
<code> "Language_ZH  汉语 Z   Z   汉语版 "
<entry> "Feedback, ToneFeedback, and Evaluation"
<normal> "All messages to the user that are not a fixed part of the pages are collected in the Feedback tables. A Feedback table consists of two columns: Key and Text. The key part is for internal use and should not be changed. The Text part should be translated. All words ending in a ‘$’ character are internal variables which will be substituted with calculated or measured values. They should not be changed."
<normal> "There are three such feedback tables: Feedback, ToneFeedback, and Evaluation. Feedback and Evaluation are short and contain some common phrases as Continue and Correct. ToneFeedback contains three columns with a short comment on how to pronounce two tone combinations."


<entry> "##Other pages#"

<entry> "Credits page"
<normal> "The Credits (About) page contains information about contributions, copyrights, and the licensing of SpeakGoodChinese. It also contains information on the current version and date of the application."
<normal> "There are three columns: font, size and text. The first column, font, contains the name of the font family, e.g, Times. The second column, size, contains the point size of the font, e.g., 12. The last column, text, contains the actual text to be written. The standard Praat character and formating codes apply. The absolute font sizes will be scaled relative to the size of the window. Text between single ‘-quotes is interpreted as Praat substitution commands and should not be changed."

<entry> "Manual"
<normal> "The manual is a help text in the usual format of Praat Man Pages. These consist of a number of files which can be translated individually. The default language is English. Manual pages in other languages are identified by the same language code as other files in that language. All pages written in a language are chained together by links. "
<normal> "If you plan to translate the manual you should contact the maintainer of SpeakGoodChinese."


<entry> "Advanced: Testing translated user interface tables"
<normal> "Translated tables can be tested on a binary version of SpeakGoodChinese. For this you need to create a map with the name %GUI% in the preferences map of SpeakGoodChinese."
<normal> "Assume you have created a Spanish translation with the language label %%_ES%. " 
<normal> "The preferences directory can be found in your home directory, or home folder %%user name%."
<list_item> "\bu MS Windows: %%C:\bsDocuments and Settings\bs<User name>\bsSpeakGoodChinese2%"
<list_item> "\bu Mac OSX: %%/Users/<User name>/Library/Preferences/SpeakGoodChinese2%"
<list_item> "\bu Linux: %%/home/<User name>/.SpeakGoodChinese2%"

<normal> "Create a folder with the name %%GUI%: "
<list_item> "\bu MS Windows: %%C:\bsDocuments and Settings\bs<User name>\bsSpeakGoodChinese2\bsGUI%"
<list_item> "\bu Mac OSX: %%/Users/<User name>/Library/Preferences/SpeakGoodChinese2/GUI%"
<list_item> "\bu Linux: %%/home/<User name>/.SpeakGoodChinese2/GUI%"

<normal> "Export your translated tables as csv files (comma separated values) with <tab> as the separator. Change the extension to Table. Copy your tables into that Data map. Rename them to one of the other language codes, say, %%_ES% for %%Espa\n~ol% (Spanish). Go into the %%Config_ES.Table% file and change the language code in the Language_<your language> item into _ES (or the language code of the table you have chosen). Save the file."

<normal> "%%Google Documents%"
<list_item> "\bu File -> Download as -> Tab separated values (.tsv)"
<list_item> "\bu Change the extension of file Config_ES.tsv to Config_ES.Table"
<list_item> "\bu Make sure that the name is really only Config_ES.Table"
<normal> "%%OpenOffice% or %%LibreOffice%"
<list_item> "\bu File -> Save as -> Select Text CSV (.csv) "
<list_item> "\bu Change the name into Config_ES.Table (change Config into the relevant name, e.g., MainPage)"
<list_item> "\bu deselect automatic file name extension -> Save"
<list_item> "\bu Character set: Unicode (UTF-8)"
<list_item> "    File delimiter: {Tab}"
<list_item> "    Text delimiter: “"
<list_item> "    select Save cell content as shown"
<list_item> "-> Click OK"
<list_item> "%%MS Excel%"
<list_item> "\bu File -> Save as -> Select Tab separated text (.txt) "
<list_item> "\bu You cannot change the extension in this stage. Ignore the warning when you click Save"
<list_item> "\bu Change the name from %%Config_ES.txt% into %%Config_ES.Table% (change %%Config% into the relevant name, e.g., %%MainPage%)"

<normal> "After having created the files, which must include the config table, e.g., %%Config_ES.Table%, move it into the preferences GUI directory you created before. When you start up SpeakGoodChinese you should find that there is now a button Labelled Espaniol (or what the label was you chose) on the Config page. "
<normal> "You do not have to use all the table files, only %%Config_ES.Table% has to be present. When no table file is found with the label %%_ES%, the English equivalent is used instead."

<normal> "When you now start SpeakGoodChinese and select the substitute language, e.g., %%Espa\n~ol%, you will get the user interface of your target language. You can test this out with the tables of one of the existing languages."
<normal> "Note: When there is a serious error in the table, SpeakGoodChinese will crash. Removing the GUI folder for the Preferences map will cure that."
<normal> "Do not forget to remove the test GUI table when you are ready!"

<normal> "Important: If the file is defect, e.g., there are missing items, SpeakGoodChinese will crash. It is very well possible that you get no, or an incomprehensible error message. Compare your table(s) with the originals from another language and try to find the offending difference."
