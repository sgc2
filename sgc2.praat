#
# SpeakGoodChinese 2.0
# 
# Master Praat script
#
#     SpeakGoodChinese: sgc2.praat is the master GUI of SpeakGoodChinese
#     It is written in Praat script for the Demo window 
#     
#     Copyright (C) 2007-2010  R.J.J.H. van Son and 2010 the Netherlands Cancer Institute
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Koning, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
# The real application name
sgc2.demoAppName$ = "SpeakGoodChinese2"

# Define variable that might be reset in Initialise*.praat
if not variableExists("build_SHA$")
	build_SHA$ = "-"
endif

# These are simply "useful" defaults
# The local Table directory only exists when running SGC as a script
if build_SHA$ = "-"
	localTableDir$ = "Data"
else
	localTableDir$ = ""
endif
buttonsTableName$ = "MainPage"
configTableName$ = "Config"
buttons$ = ""
config$ = ""
recordedSound$ = ""
sgc.recordedSound = 0
te.recordedPitch = 0
te.buttonPressValue = 0
samplingFrequency = 44100
recordingTime = 2.5

# Pop-Up window colors
sgc2.popUp_bordercolor$ = "{0.5,0.5,1}"
sgc2.popUp_backgroundcolor$ = "{0.9,0.9,1}"

# If running in a packed script binary
if index_regex(preferencesDirectory$, "(?i'sgc2.demoAppName$')$")
	preferencesAppDir$ = preferencesDirectory$
elsif index_regex(preferencesDirectory$, "[pP]raat(\-dir| Prefs)?$")
	# If running as a Praat script, create a new preferences directory
	if unix
		preferencesAppDir$ = "'preferencesDirectory$'/../.'sgc2.demoAppName$'"
	else
		preferencesAppDir$ = "'preferencesDirectory$'/../'sgc2.demoAppName$'"
	endif
else
	# It has to go somewhere. Make a subdirectory in the current preferences directory
	preferencesAppDir$ = "'preferencesDirectory$'/'sgc2.demoAppName$'"
endif


# Parameters for isolating recorded speech from noise
# Should be mostly left alone unless you are using ultra clean
# or very noisy recordings
noiseThresshold = -30
minimumPitch = 60
soundMargin = 0.25
minimumIntensity = 30

# Set up button height
buttonbevel = 0

# Define canvas
viewportMargin = 5
defaultFontSize = 12
defaultFont$ = "Helvetica"
defaultLineWidth = 1

# Set up system
call reset_viewport

# Load supporting scripts
# Load tables in script format
include CreateTables.praat
include CreateWordlists.praat
# Set up system and load preferences
include InitialiseSGC2.praat
# Include the main page buttons and procedures
include MainPage.praat
# Include the configuration page buttons and procedures
include Config.praat

# A function:
# replaySGC2LogFunction$ = "replaySGC2LogFunction"
# procedure replaySGC2LogFunction
# 	....
# 	Test commands
# endproc
# 
# include sgc2.praat
# 
'replaySGC2Log$'

# Start instruction loop
while demoWaitForInput()
	.label$ = ""
	.clickX = -1
	.clickY = -1
	.pressed$ = ""
	if demoClicked()
		.clickX = demoX()
		.clickY = demoY()
		call buttonClicked 'buttons$' '.clickX' '.clickY'
		.label$ = buttonClicked.label$
	elsif demoKeyPressed()
		.pressed$ = demoKey$()
		call keyPressed 'buttons$' '.pressed$'
		.label$ = keyPressed.label$
	endif

	# You cannot select a text field
	if startsWith(.label$, "$")
		.label$ = ""
	endif
	
	# Do things
	if .label$ != ""
		te.buttonPressValue = 0
		# Push button down
		call Draw_button 'buttons$' '.label$' 1
		call process_label '.label$' '.clickX' '.clickY' '.pressed$'
		# push button up
		call Draw_button 'buttons$' '.label$' 'te.buttonPressValue'
	endif
endwhile

call end_program


########################################################
# 
# Definitions of procedures
# 
########################################################
#
# Do what is asked
procedure process_label .label$ .clickX .clickY .pressed$
	# Prcoess the command
	if .label$ <> "" and not startsWith(.label$,"!")
		.label$ = replace_regex$(.label$, "^[#]", "", 0)
		.label$ = replace$(.label$, "_", " ", 0)
		
		# log activity
		'sgc2.logging$' call process'buttons$''.label$' '.clickX' '.clickY' '.pressed$'
		
		call process'buttons$''.label$' '.clickX' '.clickY' '.pressed$'
	endif
endproc

# Intialize buttons
procedure init_buttons
	noerase = 1
	call Draw_all_buttons 'buttons$'
	noerase = 0
endproc

# Draw all buttons
noerase = 0
procedure Draw_all_buttons .table$
	.varPrefix$ = replace_regex$(.table$, "^(.)", "\l\1", 0)
	select Table '.table$'
	.numRows = Get number of rows
	
	for .row to .numRows
		.label$ = Get value... '.row' Label
        if not startsWith(.label$, "!")
			.pressed = 0
			# Determine the "pressed" state of a button
			# A variable with the same name as the button will act as a
			# "pressed state"
			.variableName$ = .varPrefix$+"."+(replace_regex$(.label$, "^(.)", "\l\1", 0))
			# Simple boolean variables
			if index(.variableName$, "_") <= 0 and variableExists(.variableName$)
				# True: Pressed
				if '.variableName$' > 0
					.pressed = 2
				# <0: Grayed out
				elsif '.variableName$' < 0
					.pressed = -1
				endif
			elsif index(.variableName$, "_") <= 0 and variableExists("'.variableName$'$")
				# Non zero: Pressed
				if '.variableName$'$ <> "" and '.variableName$'$ <> "0"
					.pressed = 2
				endif
			# Complex buttons with an variableName+'_'+value structure
			# varableName$ -> name of button, e.g., "language"
			elsif index(.variableName$, "_")
				.generalVar$ = left$(.variableName$, rindex(.variableName$, "_") - 1)
				.currentVariableName$ = .generalVar$
				# Is it a string?
				if variableExists(.generalVar$+"$")
					.currentVariableName$ = .generalVar$ + "$"
				endif
				# Remove one level of indirection
				if variableExists(.currentVariableName$)
					if index(.currentVariableName$, "$")
						.currentVariableName$ = '.currentVariableName$'
					else
						.currentValue = '.currentVariableName$'
						.currentVariableName$ = "'.currentValue'"
					endif
					# Remove next level of indirection
					.currentContent$ = "'.currentVariableName$'"
					if .currentContent$ = "_DISABLED_"
						.pressed = -1
					endif
					# Reconstruct label from current values
					.currentLabelValue$ = .generalVar$ + "_" + .currentContent$
					# Set PRESSED from label
					if .variableName$ = .currentLabelValue$
						.pressed = 2
					endif
				endif
			endif
			# You did erase everything before you started here? So do not do that again
		    call Draw_button_internal 0 '.table$' '.label$' '.pressed'
        endif
	endfor
endproc

# Draw a button from a predefined button table
# Normally, erase the area around a button
procedure Draw_button .table$ .label$ .push
	call Draw_button_internal 1 '.table$' '.label$' '.push'
endproc

# Use this function if you want to control erasing
procedure Draw_button_internal .erase_button_area .table$ .label$ .push
	# Do not draw invisible buttons starting with #
	goto NOBUTTON startsWith(.label$, "#")

	# Scale rounding of rounded rectangles
	.wc = 1
	.mm = demo Horizontal wc to mm... '.wc' 
    # Allow to overide ! skip directive
    .forceDraw = 0
    if startsWith(.label$, "+")
    	.label$ = right$(.label$, length(.label$)-1)
     	.forceDraw = 1
    endif

    select Table '.table$'
    .row = Search column... Label '.label$'
	if .row < 1
		call emergency_table_exit Button Table '.table$' does not have a row with label '.label$'
	endif
	
	# Perspective shift sizes
	.shiftDown = 0
	.shiftX = 0
	.shiftY = 0
	if buttonbevel <> 0
		.shiftDown = 0.1*buttonbevel
    	.shiftX = -0.2*buttonbevel
    	.shiftY = 0.40*buttonbevel
	endif
	
	# Set drawing parameters
	.topBackGroundColorUp$ = "{0.93,0.93,0.93}"
	.topLineColorUp$ = "Black"
	.topLineWidthUp = 1.5
	.topBackGroundColorDown$ = "{0.89,0.89,0.94}"
	.topLineColorDown$ = "{0.2,0.2,0.2}"
	.topLineWidthDown = 2.0
	.topBackGroundColorDisabled$ = "{0.85,0.85,0.85}"
	.topLineColorDisabled$ = "{0.70,0.70,0.70}"
	.topLineWidthDisabled = 1.5
	.flankBackGroundColorUp$ = "{0.6,0.6,0.6}"
	.flankLineColorUp$ = "{0.2,0.2,0.2}"
	.flankLineWidthUp = 1.5
	.flankBackGroundColorDown$ = "{0.75,0.75,0.75}"
	.flankLineColorDown$ = .flankLineColorUp$
	.flankLineWidthDown = 1.5
	.buttonFontSize = defaultFontSize
	
	# Get button values
    .leftX = Get value... '.row' LeftX
    .rightX = Get value... '.row' RightX
    .lowY = Get value... '.row' LowY
    .highY = Get value... '.row' HighY
    .buttonText$ = Get value... '.row' Text
    .buttonColor$ = Get value... '.row' Color
    .buttonDraw$ = Get value... '.row' Draw
    .buttonKey$ = Get value... '.row' Key
    
    .noDraw = startsWith(.label$, "!") or (.leftX < 0) or (.rightX < 0) or (.lowY < 0) or (.highY < 0)

	.rotation = 0
	if index_regex(.buttonText$, "^![0-9\.]+!")
		.rotation = extractNumber(.buttonText$, "!")
		.buttonText$ = replace_regex$(.buttonText$, "^![0-9\.]+!", "", 0)
	endif
	
	if .leftX = .rightX or .highY = .lowY
		.noDraw = 1
	endif
    
    goto NOBUTTON .noDraw and not .forceDraw

    # Replace button text with ALERT
    if .push = 3
        .buttonText$ = alertText$
    endif
	
	# Adapt font size to button size
	.maxWidth = (.rightX - .leftX) - 2
	.maxHeight = (.highY - .lowY) - 1
	if .rotation = 0
		# Adapt size of button to length of text if necessary
		call adjustFontSizeOnWidth 'defaultFont$' '.buttonFontSize' '.maxWidth' '.buttonText$'
		.buttonFontSize = adjustFontSizeOnWidth.newFontSize
		if adjustFontSizeOnWidth.diff > 0
			.rightX += adjustFontSizeOnWidth.diff/2
			.leftX -= adjustFontSizeOnWidth.diff/2
		endif
		call set_font_size '.buttonFontSize'

		# Adapt size of button to length of text
		call adjustFontSizeOnHeight 'defaultFont$' '.buttonFontSize' '.maxHeight'
		if adjustFontSizeOnHeight.diff > 0
			.lowY -= adjustFontSizeOnHeight.diff/2
			.highY += adjustFontSizeOnHeight.diff/2
		endif
		.buttonFontSize = adjustFontSizeOnHeight.newFontSize
	else
		# With non-horizontal text, only change font size
		call adjustRotatedFontSizeOnBox 'defaultFont$' '.buttonFontSize' '.maxWidth' '.maxHeight' '.rotation' '.buttonText$'
		.buttonFontSize = adjustRotatedFontSizeOnBox.newFontSize
	endif
	
	# Reset and erase button area
	call reset_viewport
    demo Line width... 'defaultLineWidth'
    .shiftLeftX = .leftX
    .shiftRightX = .rightX - .shiftX
    .shiftLowY = .lowY - .shiftY
    .shiftHighY = .highY
	if .erase_button_area
		# Make erase area minutely larger
		.eraseLeft = .shiftLeftX - 0.01
		.eraseRight = .shiftRightX + 0.01
		.eraseBottom = .shiftLowY - 0.01
		.eraseTop = .shiftHighY + 0.01
		demo Paint rectangle... White .eraseLeft .eraseRight .eraseBottom .eraseTop
	endif
	
    # If label starts with "$", it is a text field. Then do not draw the button
	if not startsWith(.label$, "$")
    	# Give some depth to button: Draw flank outline
		if .shiftDown or .shiftX or .shiftY
			if .push <= 0
    			demo Paint rounded rectangle... '.flankBackGroundColorUp$' .shiftLeftX .shiftRightX .shiftLowY .shiftHighY '.mm'
				demo Colour... '.flankLineColorUp$'
    			demo Line width... '.flankLineWidthUp'
			else
    			demo Paint rounded rectangle... '.flankBackGroundColorDown$' .shiftLeftX .shiftRightX .shiftLowY .shiftHighY '.mm'
				demo Colour... '.flankLineColorDown$'
    			demo Line width... '.flankLineWidthDown'
			endif
    		demo Draw rounded rectangle... .shiftLeftX .shiftRightX .shiftLowY .shiftHighY '.mm'
		endif

		# Button Down will shift the top perspective

    	# Draw the button top. Buttons with variable labels are treated differently
		if .push = 0 or (.push >= 2 and index(.buttonText$, "$$$"))
    		demo Paint rounded rectangle... '.topBackGroundColorUp$' '.leftX' '.rightX' '.lowY' '.highY' '.mm'
			demo Colour... '.topLineColorUp$'
    		demo Line width... '.topLineWidthUp'
		elsif .push < 0
    		demo Paint rounded rectangle... '.topBackGroundColorDisabled$' '.leftX' '.rightX' '.lowY' '.highY' '.mm'
			demo Colour... '.topLineColorDisabled$'
    		demo Line width... '.topLineWidthDisabled'
		else
			# Button Down
			.leftX += .shiftDown
			.rightX += .shiftDown
			.lowY -= .shiftDown
			.highY -= .shiftDown

    		demo Paint rounded rectangle... '.topBackGroundColorDown$' .leftX .rightX .lowY .highY '.mm'
			demo Colour... '.topLineColorDown$'
    		demo Line width... '.topLineWidthDown'
		endif
    	demo Draw rounded rectangle... '.leftX' '.rightX' '.lowY' '.highY' '.mm'
	endif
   
    # The button text and symbol
	.horWC = demo Horizontal mm to wc... 10.0
	.verWC = demo Vertical mm to wc... 10.0
	if .verWC > 0
		.verCoeff = .horWC / .verWC
	else
		.verCoeff = 1
	endif

    .centerX = (.leftX + .rightX)/2
    .centerY = .lowY + 0.6*(.highY-.lowY)
    .radius = min(.verCoeff * (.highY - .lowY ), (.rightX - .leftX))/3
	.buttonKey$ = replace$(.buttonKey$, "\", "\\", 0)
	.buttonKey$ = replace$(.buttonKey$, """", "\""""", 0)
	.keyText$ = replace$(.buttonKey$, "\", "", 0)
	.keyText$ = replace$(.keyText$, "-", "", 0)
	.newText$ = ""
	if .keyText$ <> ""
		.newText$ = replace_regex$(.buttonText$, "['.keyText$']", "#%&", 1)
	endif
	if .newText$ = ""
		.newText$ = .buttonText$
	endif
	# Variable text field, read corresponding variable
	if index(.newText$, "$$$")
		.fieldName$ = replace_regex$(.label$, "^[!$#]", "", 0)
		.fieldName$ = replace_regex$(.fieldName$, "^(.)", "\l\1", 0)
		.varPrefix$ = replace_regex$(.table$, "^(.)", "\l\1", 0)
		.newText$ = replace$(.newText$, "$$$", '.varPrefix$'.'.fieldName$'$, 0)
	endif
	if .push = 1 or .push = -1
		demo Grey
		if .buttonColor$ = "Red"
			.buttonColor$ = "Pink"
		elsif .buttonColor$ = "Blue"
			.buttonColor$ = "{0.5,0.5,1}"
		else
			.buttonColor$ = "Grey"
		endif
    elsif .push >= 2
        .buttonColor$ = "Maroon"
	else
    	demo Colour... Black
	endif

    call '.buttonDraw$' '.buttonColor$' '.centerX' '.centerY' '.radius' 
	call set_font_size '.buttonFontSize'
    demo Colour... '.buttonColor$'
	if .rotation = 0
		.anchorY = .lowY
		.verticalAlignment$ = "Bottom"
	else
		.anchorY = .lowY + 0.5*(.highY-.lowY)
		.verticalAlignment$ = "Half"
	endif
    demo Text special... '.centerX' Centre '.anchorY' '.verticalAlignment$' 'defaultFont$' '.buttonFontSize' '.rotation' '.newText$'
	demoShow()

	# Reset
	call set_font_size 'defaultFontSize'
    demo Black
    demo Line width... 'defaultLineWidth'
    
    label NOBUTTON
endproc

procedure set_window_title .table$ .addedText$
    select Table '.table$'
    .row = Search column... Label !WindowTitle
	if .row < 1
		call emergency_table_exit Button Table '.table$' does not have a row with label !WindowTitle
	endif
	.windowText$ = Get value... '.row' Text
	call convert_praat_to_latin1 '.windowText$'
	.windowText$ = convert_praat_to_latin1.text$

	if index(.windowText$, "$$$")
		.displayWindowText$ = replace$(.windowText$, "$$$", .addedText$, 0)
	else
		.displayWindowText$ = .windowText$ + .addedText$
	endif
	
    demoWindowTitle(.displayWindowText$ )
endproc

# Handle language setting 
procedure processLanguageCodes .table$ .label$
	.table$ = "Config"
    call Draw_button 'config$' Language_'config.language$' 0
    call Draw_button 'config$' '.label$' 2
    # Someone might have to use more than 2 chars for the language code
    .numChars = length(.label$) - length("Language_")
	.lang$ = right$(.label$, .numChars)
    # Load new tables
    call set_language '.lang$'
    call initialize_toneevaluation_tables
endproc

# Set the language
procedure set_language .lang$
	.redraw_config = 0
    # Remove old tables
    if buttons$ <> ""
        select Table 'buttons$'
        Remove
		.redraw_config = 1
    endif
    if config$ <> ""
        select Table 'config$'
        Remove
		.redraw_config = 1
    endif
    
    # See whether there is a custom language
    sgc.customLanguage$ = ""
    .langList = nocheck Create Strings as file list: "CustomLanguages", preferencesTableDir$+"/Config_*.Table"
    # NOTE: The list might not exist!!!
    if .langList = undefined
		.numLanguages = -1
	else
		.numLanguages = Get number of strings
	endif
    if .numLanguages <= 0
		if not .langList = undefined
			Remove
		endif
		.langList = nocheck Create Strings as file list: "CustomLanguages", globaltablelists$+"/Config_*.Table"
		if .langList = undefined
			.numLanguages = -1
		else
			.numLanguages = Get number of strings
		endif
	endif
    if .numLanguages > 0
		.configTable$ = Get string: 1
		.startChar = rindex(.configTable$, "_")
		sgc.customLanguage$ = right$(.configTable$, length(.configTable$) - .startChar)
		sgc.customLanguage$ = left$(sgc.customLanguage$, index(sgc.customLanguage$, ".") -1)
    endif
    if not .langList = undefined
		select .langList
		Remove
    endif
    
    # Set language
	call checkTable 'configTableName$'_'.lang$'
	if checkTable.available
		config.language$ = .lang$
	else
		config.language$ = "EN"
	endif
	
	if config.language$ = "JA"
		CJK font style preferences: "Japanese"
	else
		CJK font style preferences: "Chinese"
	endif
    
    # Load buttons tables
    call loadTable 'buttonsTableName$'
    buttons$ = selected$("Table")
    Append column... Text
    Append column... Key
    Append column... Helptext
    .numLabels = Get number of rows
    call testLoadTable 'buttonsTableName$'_'config.language$'
    if testLoadTable.table > 0   
		call loadTable 'buttonsTableName$'_'config.language$'
	else
		call loadTable 'buttonsTableName$'_EN
	endif
    .buttonsLang$ = selected$("Table")
    for .row to .numLabels
		select Table 'buttons$'
		.label$ = Get value... '.row' Label
        call findLabel '.buttonsLang$' '.label$'
	    if findLabel.row > 0
            select Table '.buttonsLang$'
        	.valueText$ = Get value... 'findLabel.row' Text
        	.valueKey$ = Get value... 'findLabel.row' Key
        	.valueHelp$ = Get value... 'findLabel.row' Helptext
        	select Table 'buttons$'
        	Set string value... '.row' Text '.valueText$'
        	Set string value... '.row' Key '.valueKey$'
        	Set string value... '.row' Helptext '.valueHelp$'
		elsif index(.label$, "_")
			# Load alternative language table
			.startChar = rindex(.label$, "_")
			.otherLanguage$ = right$(.label$, length(.label$) - .startChar)
			call loadTable 'buttonsTableName$'_'.otherLanguage$'
    		.otherbuttonsLang$ = selected$("Table")
        	call findLabel '.otherbuttonsLang$' '.label$'
	    	if findLabel.row > 0
            	select Table '.buttonsLang$'
        		.valueText$ = Get value... 'findLabel.row' Text
        		.valueKey$ = Get value... 'findLabel.row' Key
        		.valueHelp$ = Get value... 'findLabel.row' Helptext
        		select Table 'buttons$'
        		Set string value... '.row' Text '.valueText$'
        		Set string value... '.row' Key '.valueKey$'
        		Set string value... '.row' Helptext '.valueHelp$'
        	else
            	call emergency_table_exit Cannot find Label: '.otherbuttonsLang$' '.label$'
        	endif
			select Table '.otherbuttonsLang$'
			Remove
        else
            call emergency_table_exit Cannot find Label: '.buttonsLang$' '.label$'
        endif
    endfor
    select Table '.buttonsLang$'
    Remove
    
    # Load configuration table
    call loadTable 'configTableName$'
    config$ = selected$("Table")
    .configTable = selected()
	# Substitute or remove optional languages
	.optRow = Search column: "Label", "!Language_???"
	if .optRow > 0
		.row = -1
		if sgc.customLanguage$ <> ""
			.row = Search column: "Label", "Language_"+sgc.customLanguage$
		endif
		if .row <= 0 and sgc.customLanguage$ <> ""
			Set string value: .optRow, "Label", "Language_"+sgc.customLanguage$
		else
			.tableLength = Get number of rows
			# Should never ever happen
			if .tableLength > 1
				Remove row: .optRow
			else
				Set string value: .optRow, "Label", "Language_EN"
			endif
		endif
	endif
    
    select .configTable
    Append column... Text
    Append column... Key
    Append column... Helptext
    .numLabels = Get number of rows
    call testLoadTable 'configTableName$'_'config.language$'
    if testLoadTable.table > 0   
		call loadTable 'configTableName$'_'config.language$'
	else
		call loadTable 'configTableName$'_EN
	endif
    .configLang$ = selected$("Table")
    for .row to .numLabels
		select Table 'config$'
		.label$ = Get value... '.row' Label
        call findLabel '.configLang$' '.label$'
	    if findLabel.row > 0
            select Table '.configLang$'
        	.valueText$ = Get value... 'findLabel.row' Text
        	.valueKey$ = Get value... 'findLabel.row' Key
        	.valueHelp$ = Get value... 'findLabel.row' Helptext
        	select Table 'config$'
        	Set string value... '.row' Text '.valueText$'
        	Set string value... '.row' Key '.valueKey$'
        	Set string value... '.row' Helptext '.valueHelp$'
		elsif index(.label$, "_")
			.startChar = rindex(.label$, "_")
			.otherLanguage$ = right$(.label$, length(.label$) - .startChar)
			call loadTable 'configTableName$'_'.otherLanguage$'
    		.otherconfigLang$ = selected$("Table")
        	call findLabel '.otherconfigLang$' '.label$'
	    	if findLabel.row > 0
            	select Table '.otherconfigLang$'
        		.valueText$ = Get value... 'findLabel.row' Text
        		.valueKey$ = Get value... 'findLabel.row' Key
        		.valueHelp$ = Get value... 'findLabel.row' Helptext
        		select Table 'config$'
        		Set string value... '.row' Text '.valueText$'
        		Set string value... '.row' Key '.valueKey$'
        		Set string value... '.row' Helptext '.valueHelp$'
        	else
            	call emergency_table_exit Cannot find Label: '.otherconfigLang$' '.label$'
        	endif
			select Table '.otherconfigLang$'
			Remove
        else
            call emergency_table_exit Cannot find Label: '.configLang$' '.label$'
        endif
    endfor
    select Table '.configLang$'
    Remove

	# Make language change visible
	if .redraw_config
		call Draw_config_page
	endif

endproc

###############################################################
#
# Button Drawing Routines
#
###############################################################

# A stub for buttons that do not have a drawing routine (yet)
procedure DrawNull .color$ .x .y .size
endproc

procedure DrawHelp .color$ .x .y .size
	.currentFontSize = 24
	.y -= .size
	.maxHeight = 2*.size
	call adjustFontSizeOnHeight 'defaultFont$' '.currentFontSize' '.maxHeight'
	.currentFontSize = adjustFontSizeOnHeight.currentFontSize
	call set_font_size '.currentFontSize'
	demo Colour... '.color$'
	demo Text... '.x' Centre '.y' Bottom ?
	call set_font_size 'defaultFontSize'
endproc

###############################################################
#
# Button Processing Routines
#
###############################################################

# Search row in table on label
procedure findKey .table$ .label$
	.row = 0
	select Table '.table$'
	.to$ = selected$("Table")
	.to$ = "Table_"+.to$
	.numRows = Get number of rows
	for .i to .numRows
		.currentKey$ = '.to$'$[.i, "Key"]
		if .label$ = .currentKey$
			.row = .i
			goto KEYFOUND
		endif
	endfor
	label KEYFOUND
	if .row <= 0 and index(.label$, "_") <= 0
		printline "'.label$'" is not a key in '.table$'
	endif
endproc

procedure findLabel .table$ .label$
	.row = 0
	select Table '.table$'
	.to$ = selected$("Table")
	.to$ = "Table_"+.to$
	.numRows = Get number of rows
	for .i to .numRows
		.currentKey$ = '.to$'$[.i, "Label"]
		if .label$ = .currentKey$
			.row = .i
			goto LABELFOUND
		endif
	endfor
	label LABELFOUND
	if .row <= 0 and index(.label$, "_") <= 0
		call emergency_table_exit "'.label$'" is not a key in '.table$'
	endif
endproc

# Get the label
procedure buttonClicked table$ .x .y
	.label$ = ""
	select Table 'table$'
	.bo$ = selected$("Table")
	.bo$ = "Table_"+.bo$
	.numRows = Get number of rows
	for .i to .numRows
		if .label$ = ""
			.leftX = '.bo$'[.i, "LeftX"]
			.rightX = '.bo$'[.i, "RightX"]
			.lowY = '.bo$'[.i, "LowY"]
			.highY = '.bo$'[.i, "HighY"]
			if .x > .leftX and .x < .rightX and .y > .lowY and .y < .highY
				.label$ = '.bo$'$[.i, "Label"]
			endif
		endif
	endfor
endproc

procedure keyPressed table$ .pressed$
	.label$ = ""
	# Magic
	if .pressed$ = "" and not demoShiftKeyPressed()
		.label$ = "Refresh"
	endif
	.lowerPressed$ = replace_regex$(.pressed$, ".", "\L&", 0)
	.upperPressed$ = replace_regex$(.pressed$, ".", "\U&", 0)
	select Table 'table$'
	.bo$ = selected$("Table")
	.bo$ = "Table_"+.bo$
	.numRows = Get number of rows
	for .i to .numRows
		if .label$ = ""
			.key$ = '.bo$'$[.i, "Key"]
			if index(.key$, .lowerPressed$) or index(.key$, .upperPressed$)
				.label$ = '.bo$'$[.i, "Label"]
			endif
		endif
	endfor
endproc

procedure count_syllables
	.number = 0
	.pinyin$ = ""
	select sgc.currentWordlist
	if sgc.currentWord > 0 and sgc.currentWord <= sgc.numberOfWords
		.sound$ = Get value... 'sgc.currentWord' Sound
		call readPinyin 'sgc.currentWord'
		.pinyin$ = readPinyin.pinyin$
	endif
	call add_missing_neutral_tones '.pinyin$'
	.pinyin$ = add_missing_neutral_tones.pinyin$
	if index_regex(.pinyin$, "[0-9]") > 0
		.number = length(replace_regex$(.pinyin$, "[^\d]+([\d]+)", "1", 0))
	elsif .pinyin$ <> ""
		.number = 1
	endif
endproc

procedure play_sound .sound
    if .sound > 0
		if sgc.useAlternativePlayer and fileReadable(sgc.playCommandFile$)
			.scratchFile$ = "'sgc.scratchAudioDir$'SCRATCH.wav"
			select .sound
			Save as WAV file: .scratchFile$
			.command$ < 'sgc.playCommandFile$'
			.command$ = replace$(.command$, "[']", """", 0)
			.command$ = replace$(.command$, "'newline$'", " ", 0)
			if unix or macintosh
				system_nocheck bash -c -- ''.command$' "'.scratchFile$'"'
			elsif windows
				system_nocheck call '.command$' "'.scratchFile$'"
			endif
			deleteFile(.scratchFile$)
		else
			select .sound
			asynchronous Play
    	endif
    endif
endproc

procedure record_sound .recordingTime
	if .recordingTime <= 0
		.recordingTime = recordingTime
	endif
	call clean_up_sound
	
	# NOTE: Some sound can be playing! This will not be stopped.
	
	# There is a very nasty delay before the first recording starts, do a dummy record
	if not variableExists("recordingInitialized") and not sgc.useAlternativeRecorder
		call basic_sound_recording 'samplingFrequency' 0.1
		Remove
		recordingInitialized = 1
	endif
	# Recording light
    demo Paint circle... Red 5 95 2
    demoShow()
    
	# In Windows XP interaction between demoWaitForInput and Record Sound blocks drawing the feedback
	# This code might be removed #
	if windows and endsWith(build_SHA$, " XP")
		# Display a pause window to flush the graphics buffer
		beginPause ("DESTROY WINDOW ")
			comment (" ")
		endPause ("DESTROY WINDOW ", 1)
    	#call init_window
    	demo Paint circle... Red 5 95 2
    	demoShow()
	endif
	##############################
	call basic_sound_recording 'samplingFrequency' '.recordingTime'

    demo Paint circle... White 5 95 2.5
    call wipeArea 'wipeFeedbackArea$'

    # Feedback on recording level
    .extremum = Get absolute extremum... 0 0 None
    .radius = 2 * .extremum
    if .radius <= 0
		.radius = 0.02
    endif
    .blue = 0
    .green = 0
    .red = 0
    if .extremum >= 0.95
	    .red = 1
    elsif .extremum >= 0.49
	    .green = 1
    else
	    .green = .extremum / 0.5
    endif
    .color$ = "{'.red','.green','.blue'}"
    demo Colour... '.color$'
    demo Line width... 1
    demo Draw circle... 5 95 '.radius'
    # Reset
    demoShow()
    demo Colour... Black
    demo Line width... 'defaultLineWidth'
    # Process sound
    Rename... Tmp
    Resample... 10000 50
    Rename... Pronunciation
    recordedSound$ = selected$(sgcwordlist.audio$)
    sgc.recordedSound = selected(sgcwordlist.audio$)
    select Sound Tmp
    Remove
    select Sound 'recordedSound$'
    sgc.recordedSound = selected(sgcwordlist.audio$)
	
    # Cut out real sound from silences/noise
    call sound_detection 'recordedSound$' 'soundMargin'
    select Sound 'recordedSound$'
    sgc.recordedSound = selected(sgcwordlist.audio$)
    
    # Store audio if requested
    if sgc.saveAudioOn and sgc.saveAudio$ <> ""
		if sgc.savePerf$ <> "" and fileReadable(sgc.savePerf$)
			.pinyin$ = ""
			select sgc.currentWordlist
			if sgc.currentWord > 0 and sgc.currentWord <= sgc.numberOfWords
				call readPinyin 'sgc.currentWord'
				.pinyin$ = readPinyin.pinyin$
				.outputName$ = "'sgc.saveAudio$'/'.pinyin$'.wav"
				select sgc.recordedSound
				Save as WAV file: .outputName$
			endif
		else
			# The Audio directory disappeared
			sgc.savePerf$ = ""
			sgc.saveAudioOn = 0
			sgc.saveAudio$ = ""
			config.savePerf = 0
			config.openPerf = 0
			config.clearSummary = 0
			config.audioName$ = ""
		endif
    endif
    
endproc

# Uses global variables!!!
procedure basic_sound_recording .samplingFrequency .recordingTime
	# Use a different recorder program
    if sgc.useAlternativeRecorder and fileReadable(sgc.recordCommandFile$)
		.scratchFile$ = "'sgc.scratchAudioDir$'SCRATCH.wav"
		.command$ < 'sgc.recordCommandFile$'
		.command$ = replace$(.command$, "[']", """", 0)
		.command$ = replace$(.command$, "'newline$'", " ", 0)
		if unix or macintosh
			if unix and .recordingTime < 1
				.recordingTime = 1
			endif
			system_nocheck bash -c -- ''.command$' '.recordingTime''
		elsif windows
			system_nocheck call '.command$' '.recordingTime'
		endif
		.sound = Read from file: .scratchFile$
		deleteFile(.scratchFile$)
	else
		.sound = nocheck noprogress nowarn Record Sound (fixed time)... 'config.input$' 0.99 0.5 '.samplingFrequency' '.recordingTime'
		if .sound = undefined
			.sound = Create Sound: "Pronunciation", 0, .recordingTime, .samplingFrequency, "0"
		endif
	endif
	
	# The recorded sound should now be the selected object!!!
	select .sound
endproc

# Select real sound from recording
# Uses some global variable
procedure sound_detection .sound$ .margin
	select Sound '.sound$'
	.soundlength = Get total duration
	.internalSilence = 2*.margin
	
	# Silence and remove noise, DANGEROUS
	To TextGrid (silences)... 'minimumPitch' 0 'noiseThresshold' '.internalSilence' 0.1 silent sounding
	Rename... Input'.sound$'

	select TextGrid Input'.sound$'
	.numberofIntervals = Get number of intervals... 1
	if .numberofIntervals < 2
		.numberofIntervals = 0
	endif

	# The code below will suppress noise, but also weak third tones
	# This handles the problem that third tones can be realized with 
	# alternative cues, e.g, non-standard voice and very low levels 
	#
	# Remove buzzing and other obnoxious sounds (if switched on)
	for .i from 1 to .numberofIntervals
	   select TextGrid Input'.sound$'
	   .value$ = Get label of interval... 1 '.i'
	   .begintime = Get starting point... 1 '.i'
	   .endtime = Get end point... 1 '.i'
	
		# Remove noise
		if .value$ = "silent"
			select Sound '.sound$'
			Set part to zero... '.begintime' '.endtime' at nearest zero crossing
		endif
	endfor

	# Select target sound
	.maximumIntensity = -1
	.counter = 1
	for i from 1 to .numberofIntervals
	   select TextGrid Input'.sound$'

	   .value$ = Get label of interval... 1 'i'
	   .begintime = Get starting point... 1 'i'
	   .endtime = Get end point... 1 'i'

	   if .value$ != "silent"
    	   if .begintime > .margin
        	  .begintime -= .margin
    	   else
        	   .begintime = 0
    	   endif
    	   if .endtime + .margin < .soundlength
        	   .endtime += .margin
    	   else
        	   .endtime = .soundlength
    	   endif

    	   select Sound '.sound$'
    	   Extract part... '.begintime' '.endtime' Rectangular 1.0 no
    	   Rename... Tmp'.sound$'
    	   Subtract mean
    	   .newIntensity = Get intensity (dB)
    	   if .newIntensity > .maximumIntensity
        	   if .maximumIntensity > 0
            	   select Sound New'.sound$'
            	   Remove
        	   endif
        	   select Sound Tmp'.sound$'
        	   Rename... New'.sound$'
        	   .maximumIntensity = .newIntensity
    	   else
        	   select Sound Tmp'.sound$'
        	   Remove
    	   endif
    	   # 
	   endif
	endfor
	if .maximumIntensity > minimumIntensity
		select Sound '.sound$'
		Remove
		select Sound New'.sound$'
		Rename... '.sound$'
	elsif .maximumIntensity > -1
		select Sound New'.sound$'
		Remove
	endif
	select TextGrid Input'.sound$'
	Remove
	
	select Sound '.sound$'
endproc

procedure end_program
	call write_preferences "" 
	demo Erase all
	select all
	Remove
	exit
endproc

######################################################
#
# Configuration Page
#
######################################################
procedure config_page
    demo Erase all
    demoWindowTitle("Speak Good Chinese: Change settings")
    .label$ = ""
    call Draw_config_page
    
	goto GOBACK index_regex(replaySGC2Log$, "^\s*#") <= 0

    while (.label$ <> "Return") and demoWaitForInput() 
		.clickX = -1
		.clickY = -1
		.pressed$ = ""
	    .label$ = ""
	    if demoClicked()
		    .clickX = demoX()
		    .clickY = demoY()
		    call buttonClicked 'config$' '.clickX' '.clickY'
		    .label$ = buttonClicked.label$
	    elsif demoKeyPressed()
		    .pressed$ = demoKey$()
		    call keyPressed 'config$' '.pressed$'
		    .label$ = keyPressed.label$
	    endif

		# You cannot select a text field
		if startsWith(.label$, "$")
			.label$ = ""
		endif
		
	    # Do things
	    if .label$ != ""
		    # Handle push button in process_config
		    call process_config '.label$' '.clickX' '.clickY' '.pressed$'
	    endif
        
        if .label$ = "Return"
            goto GOBACK
        endif
    endwhile

    # Go back
    label GOBACK
    call init_window
endproc

procedure Draw_config_page
	demo Erase all
	# Draw background
	if config.showBackground
		call draw_background Background
	endif
	# Draw buttons
    call Draw_all_buttons 'config$'
	call set_window_title 'config$'  
    # Set correct buttons (alert)
	call setConfigMainPage
endproc

# Do what is asked
procedure process_config .label$ .clickX .clickY .pressed$
	if .label$ <> "" and not startsWith(.label$,"!")
		.label$ = replace_regex$(.label$, "^[#]", "", 0)
		.label$ = replace$(.label$, "_", " ", 0)
		
		# Log activity
		'sgc2.logging$' call process'config$''.label$' '.clickX' '.clickY' '.pressed$'
		
		call process'config$''.label$' '.clickX' '.clickY' '.pressed$'
	endif
endproc

###############################################################
#
# Presenting help texts
#
###############################################################

# Process Help
procedure help_loop .table$ .redrawProc$
	# General Help text
	call  write_help_title '.table$'
	

    .label$ = ""
    call Draw_button '.table$' Help 2
	goto HELPGOBACK index_regex(replaySGC2Log$, "^\s*#") <= 0
	
    .redrawScreen = 0
    while (.label$ <> "Help") and demoWaitForInput() 
	    .label$ = ""
	    if demoClicked()
		    .clickX = demoX()
		    .clickY = demoY()
		    call buttonClicked '.table$' '.clickX' '.clickY'
		    .label$ = buttonClicked.label$
	    elsif demoKeyPressed()
		    .pressed$ = demoKey$()
		    call keyPressed '.table$' '.pressed$'
		    .label$ = keyPressed.label$
	    endif

	    if .label$ != "" and .label$ <> "Help"
			# Redraw screen
			if .redrawScreen
				demo Erase all
				call '.redrawProc$'
			else
    			.redrawScreen = 1
			endif
			call Draw_button '.table$' Help 2
			call  write_help_title '.table$'

		    # Handle push button in process_config
		    call write_help_text '.table$' '.label$'
		    
			# Log activity
			'sgc2.logging$' demo Erase all
			'sgc2.logging$' call '.redrawProc$'
			'sgc2.logging$' call write_help_title '.table$'
			'sgc2.logging$' call write_help_text '.table$' '.label$'
	    endif
        
    endwhile
    
	label HELPGOBACK
	# Reset button
    call Draw_button '.table$' Help 0
	demo Erase all
	call '.redrawProc$'
endproc

# Write help text
procedure write_help_text .table$ .label$
	call findLabel '.table$' '.label$'
	.row = findLabel.row
	select Table '.table$'
	# Get text
	if .row <= 0
		call findLabel '.table$' Help
		.row = findLabel.row
		select Table '.table$'
	endif
	.helpText$ = Get value... '.row' Helptext
	.helpKey$ = Get value... '.row' Key
	.helpKey$ = replace$(.helpKey$, "\", "", 0)
	.helpKey$ = replace$(.helpKey$, "_", "\_ ", 0)
	if index_regex(.helpKey$, "\S")
		.helpText$ = .helpText$+" ("+.helpKey$+")"
	endif
	# Get button values
    .leftX = Get value... '.row' LeftX
    .rightX = Get value... '.row' RightX
    .lowY = Get value... '.row' LowY
    .highY = Get value... '.row' HighY
	
	# PopUp dimensions
	.currentHelpFontSize = defaultFontSize
    call set_font_size '.currentHelpFontSize'
	.helpTextSize = demo Text width (wc)... '.helpText$'
	.helpTextSize += 4
	if .leftX > 50
		.htXleft = 20
		.htXright = .htXleft + .helpTextSize + 5
		.xstart = .leftX
	else
		.htXright = 80
		.htXleft = .htXright - .helpTextSize - 5
		.xstart = .rightX
	endif
	if .lowY > 50
		.htYlow = 40
		.htYhigh = .htYlow + 7
		.ystart = .lowY
		.yend = .htYhigh
	else
		.htYhigh = 60
		.htYlow = .htYhigh - 7
		.ystart = .highY
		.yend = .htYlow
	endif

	# Adapt font size to horizontal dimensions
	.maxWidth = 90
	call adjustFontSizeOnWidth 'defaultFont$' '.currentHelpFontSize' '.maxWidth' '.helpText$'
	.currentHelpFontSize = adjustFontSizeOnWidth.newFontSize
	if .htXleft < 0 or .htXright > 100
		.htXleft = 0
		.htXright = .htXleft + adjustFontSizeOnWidth.textWidth + 5
	endif
	call set_font_size '.currentHelpFontSize'

	# Adapt vertical dimensions to font height
	call points_to_wc '.currentHelpFontSize'
	.lineHeight = points_to_wc.wc
	if .lineHeight > .htYhigh - .htYlow - 4
		.htYhigh = .htYlow + .lineHeight + 4
	endif

	# Determine arrow endpoints
	.xend = .htXleft
	if abs(.htXleft - .xstart) > abs(.htXright - .xstart)
		.xend = .htXright
	endif
	if abs((.htXleft+.htXright)/2 - .xstart) < min(abs(.htXright - .xstart),abs(.htXleft - .xstart))
		.xend = (.htXleft+.htXright)/2
	endif
	
	.xtext = .htXleft + 2
	.ytext = .htYlow + 1
	
	# Draw pop-up
	.mm2wc = demo Horizontal mm to wc... 1
	.lineWidth = 2/.mm2wc
	demo Line width... '.lineWidth'
	demo Arrow size... '.lineWidth'
 	demo Colour... 'sgc2.popUp_bordercolor$'
	demo Paint rectangle... 'sgc2.popUp_backgroundcolor$' '.htXleft' '.htXright' '.htYlow' '.htYhigh'
	demo Draw rectangle... '.htXleft' '.htXright' '.htYlow' '.htYhigh'
	demo Draw arrow... '.xstart' '.ystart' '.xend' '.yend'
	demo Line width... 'defaultLineWidth'
	demo Arrow size... 1
	demo Black
	demo Text... '.xtext' Left '.ytext' Bottom '.helpText$'
	demoShow()
	call set_font_size 'defaultFontSize'
	
endproc

procedure write_help_title .table$
	# Set help text title
	# General Help text
	call findLabel '.table$' Help
	.row = findLabel.row
	select Table '.table$'
	.helpTitle$ = Get value... '.row' Helptext
	.helpKey$ = Get value... '.row' Key
	.helpKey$ = replace$(.helpKey$, "\", "", 0)
	.helpKey$ = replace$(.helpKey$, "_", "\_ ", 0)
	.helpTitle$ = .helpTitle$+" ("+.helpKey$+")"
	
	call reset_viewport
	.helpTitleFontSize = 14
	# Adapt size of button to length of text
	.maxWidth = 80
	call adjustFontSizeOnWidth 'defaultFont$' '.helpTitleFontSize' '.maxWidth' '.helpTitle$'
	.helpTitleFontSize = adjustFontSizeOnWidth.newFontSize
	call set_font_size '.helpTitleFontSize'
	.helpTop = 100
	
	demo Select inner viewport... 0 100 0 100
	demo Axes... 0 100 0 100
	demo Text... 50 Centre '.helpTop' Top '.helpTitle$'
    call set_font_size 'defaultFontSize'
	call reset_viewport
endproc

###############################################################
#
# Miscelaneous procedures
#
###############################################################
procedure printPageToPrinter
	call print_window
	demo Print... 'printerName$' 'printerPresets$'
	call init_window
endproc

procedure points_to_wc .points
	.mm = .points * 0.3527777778
	.wc = demo Vertical mm to wc... '.mm'
endproc

procedure reset_viewport
	.low = viewportMargin
	.high = 100 - viewportMargin
	demo Select inner viewport... '.low' '.high' '.low' '.high'
	demo Axes... 0 100 0 100
endproc

procedure set_font_size .fontSize
	call reset_viewport
	demo Font size... '.fontSize'
	call reset_viewport
endproc

procedure wipeArea .areaCommand$
	call reset_viewport
	'.areaCommand$'
endproc

procedure adjustFontSizeOnWidth .font$ .currentFontSize .maxWidth .text$
	demo '.font$'
	call set_font_size '.currentFontSize'
	.textWidth = demo Text width (wc)... '.text$'
	while .textWidth > .maxWidth and .currentFontSize > 2
		.currentFontSize -= 0.5
		call set_font_size '.currentFontSize'
		.textWidth = demo Text width (wc)... '.text$'
	endwhile
	.diff = .textWidth - .maxWidth
	.newFontSize = .currentFontSize	
	demo 'defaultFont$'
endproc

procedure adjustRotatedFontSizeOnBox .font$ .currentFontSize .maxWidth .maxHeight .rotation .text$
	demo '.font$'
	.radians = .rotation/360 * 2 * pi
	.horWC = demo Horizontal mm to wc... 10.0
	.verWC = demo Vertical mm to wc... 10.0
	if .horWC > 0
		.verCoeff = .verWC / .horWC
	else
		.verCoeff = 1
	endif
	call set_font_size '.currentFontSize'
	.textLength = demo Text width (wc)... '.text$'
	while (.textLength * .verCoeff * sin(.radians) > .maxHeight or .textLength * cos(.radians) > .maxWidth) and .currentFontSize > 2
		.currentFontSize -= 0.5
		call set_font_size '.currentFontSize'
		.textLength = demo Text width (wc)... '.text$'
	endwhile
	.diff = .textLength - .maxHeight
	.newFontSize = .currentFontSize	
	demo 'defaultFont$'
endproc

procedure adjustFontSizeOnHeight .font$ .currentFontSize .maxHeight
	demo '.font$'
	call points_to_wc '.currentFontSize'
	.lineHeight = points_to_wc.wc
	while .lineHeight > .maxHeight and .currentFontSize > 2
		.currentFontSize -= 0.5
		call points_to_wc '.currentFontSize'
		.lineHeight = points_to_wc.wc
	endwhile
	.diff = .lineHeight - .maxHeight
	.newFontSize = .currentFontSize
	demo 'defaultFont$'
endproc

# Load a table with button info etc.
# Load local tables if present. Else load
# build-in scripted tables
procedure loadTable .tableName$
	.tableVariableName$ = replace_regex$(.tableName$, "\W", "_", 0);
	# Search for the table in local, preference, and global directories
	if localTableDir$ <> "" and fileReadable("'localTableDir$'/'.tableName$'.Table")
    	.table = Read from file... 'localTableDir$'/'.tableName$'.Table
	elsif fileReadable("'preferencesTableDir$'/'.tableName$'.Table")
    	.table = Read from file... 'preferencesTableDir$'/'.tableName$'.Table
	elsif fileReadable("'globaltablelists$'/'.tableName$'.Table")
    	.table = Read from file... 'globaltablelists$'/'.tableName$'.Table
	# Load them from script
	elsif variableExists("procCreate'.tableVariableName$'$")
		call Create'.tableVariableName$'
    	Rename: .tableName$
		.table = selected("Table")
	else
		call emergency_table_exit '.tableName$' cannot be found
	endif
	
	# Check whether this is a real table
	selectObject: .table
	.fullName$ = selected$ ()
	.type$ = extractWord$(.fullName$, "")
	if .type$ <> "Table"
		Remove
		.table = -1
	endif

	if .table <= 0
		call emergency_table_exit '.tableFileName$' corrupted or cannot be found
	endif
endproc

procedure testLoadTable .tableName$
	.table = 0
	.tableVariableName$ = replace_regex$(.tableName$, "\W", "_", 0);
	# Search for the table in local, preference, and global directories
	if localTableDir$ <> "" and fileReadable("'localTableDir$'/'.tableName$'.Table")
    	.table = 1
	elsif fileReadable("'preferencesTableDir$'/'.tableName$'.Table")
    	.table = 2
	elsif fileReadable("'globaltablelists$'/'.tableName$'.Table")
    	.table = 3
	# Load them from script
	elsif variableExists("procCreate'.tableVariableName$'$")
		.table = 4
	else
		.table = 0
	endif
endproc

procedure checkTable .tableName$
	.available = 0
	if localTableDir$ <> "" and fileReadable("'localTableDir$'/'.tableName$'.Table")
    	.available = 1
	elsif fileReadable("'preferencesTableDir$'/'.tableName$'.Table")
    	.available = 1
	elsif fileReadable("'globaltablelists$'/'.tableName$'.Table")
    	.available = 1
	# Load them from script
	elsif variableExists("procCreate'.tableName$'$")
    	.available = 1
	else
    	.available = 0
    endif
endproc

# Create a pop-up window with text from a Text Table
procedure write_text_table .table$
	.xleft = 10
 	.xright = 90
  	.ylow = 20
   	.yhigh = 85
	.lineHeight = 2.5

	# Get table with text and longest line
	.numLines = 0
	call testLoadTable '.table$'
	if testLoadTable.table > 0
		call loadTable '.table$'
		.instructionText = selected()
		.numLines = Get number of rows
	else
		goto ESCAPEwrite_text_table
	endif
	.instructionFontSize = 14
	.referenceText$ = ""
	.maxlenght = 0
	.maxLine = 0
	.maxFontSize = 0
	.maxWidth = 0
	for .l to .numLines
		select '.instructionText'
		.currentText$ = Get value... '.l' text
		# Expand variables, eg, 'praatVersion$'
		call expand_praat_variables '.currentText$'
		.currentText$ = expand_praat_variables.text$
		
		.font$ = Get value... '.l' font
		.fontSize = Get value... '.l' size
		call set_font_size '.fontSize'
		.textWidth = demo Text width (wc)... '.currentText$'
		if .fontSize > .maxFontSize
			.maxFontSize = .fontSize
		endif
		if .textWidth > .maxWidth
			.maxWidth = .textWidth
			.instructionFontSize = .fontSize
			.maxLine = .l
		endif
	endfor
	select '.instructionText'
	.referenceText$ = Get value... '.maxLine' text
	.maxLineFont$ = Get value... '.maxLine' font
	.instructionFontSize = Get value... '.maxLine' size
	call set_font_size '.maxFontSize'
	
	# Adapt size of button to length of text
	.maxWidth = (.xright - .xleft) - 4
	.origFontSize = .instructionFontSize
	call adjustFontSizeOnWidth 'defaultFont$' '.instructionFontSize' '.maxWidth' '.referenceText$'
	call adjustFontSizeOnHeight 'defaultFont$' '.maxFontSize' '.lineHeight'
	.instructionFontSize = min(adjustFontSizeOnWidth.newFontSize, adjustFontSizeOnHeight.newFontSize)
	if adjustFontSizeOnWidth.diff > 0
		.xright += adjustFontSizeOnWidth.diff/4
		.xleft -= 3*adjustFontSizeOnWidth.diff/4
	endif
	call set_font_size '.instructionFontSize'
	.fontSizeFactor = .instructionFontSize / .origFontSize

	.numRows = Get number of rows
	# Calculate length from number of lines.
 	.dy = .lineHeight
	.midY = .yhigh - (.yhigh - .ylow)/2
	.yhigh = .midY + (.numRows+1) * .dy / 2
	.ylow = .yhigh - (.numRows+1) * .dy
	.textleft = .xleft + 2
	
	demo Line width... 8
 	demo Colour... 'sgc2.popUp_bordercolor$'
	demo Paint rectangle... 'sgc2.popUp_backgroundcolor$' '.xleft' '.xright' '.ylow' '.yhigh'
	demo Draw rectangle... '.xleft' '.xright' '.ylow' '.yhigh'
	demo Line width... 'defaultLineWidth'
	demo Black
 	.ytext = .yhigh - 2 - .dy
	for .i to .numRows
		select '.instructionText'
		.font$ = Get value... '.i' font
		.fontSize = Get value... '.i' size
		.font$ = extractWord$(.font$, "")
		# Scale font
		.fontSize = floor(.fontSize*.fontSizeFactor)
		if .fontSize < 4
			.fontSize = 4
		endif
		.line$ = Get value... '.i' text
		# Expand variables, eg, 'praatVersion$'
		call expand_praat_variables '.line$'
		.line$ = expand_praat_variables.text$
		
		# Display text
		demo Text special... '.textleft' Left '.ytext' Bottom '.font$' '.fontSize' 0 '.line$'
		.ytext -= .dy
	endfor	
	demoShow()	
	call set_font_size 'defaultFontSize'
	
	select '.instructionText'
	Remove
	
	label ESCAPEwrite_text_table
endproc


# Create a pop-up window with text from an existing Table object
procedure write_tabbed_table .table$ .labelTextTable$
	.xleft = 0
 	.xright = 100
  	.ylow = 20
   	.yhigh = 85
	.lineHeight = 2.5

	# Get table with text and longest line
	call testLoadTable '.table$'
	if testLoadTable.table <= 0
		call loadTable '.labelTextTable$'
		.labelText$ = selected$("Table")
	endif
		
	select Table '.table$'
	.tabbedText = selected()
	.numLines = Get number of rows
	.numCols = Get number of columns
	.font$ = defaultFont$
	.fontSize = defaultFontSize
	# Standard width
	.widthCanvas = .xright - .xleft
	.dx = (.widthCanvas - 4) / (.numCols)

	# Get longest entry
	demo '.font$'
	call set_font_size '.fontSize'
	.maxWidth = 0
	for .i from 0 to .numLines
		.xtext = .xleft + .dx / 2
		for .j to .numCols
			select '.tabbedText'
			.currentLabel$ = Get column label... '.j'
			if .i > 0
				.line$ = Get value... '.i' '.currentLabel$'
			else
				.line$ = .currentLabel$
				select Table '.labelText$'
    			call findLabel '.labelText$' '.line$'
    			select Table '.labelText$'
    			.line$ = Get value... 'findLabel.row' Text
			endif
			# Expand variables, eg, 'praatVersion$'
			call expand_praat_variables '.line$'
			.line$ = expand_praat_variables.text$
			.textWidth = demo Text width (wc)... '.line$'
			if .textWidth > .maxWidth
				.maxWidth = .textWidth
			endif
		endfor
	endfor
	if .dx > 1.2 * .maxWidth
		.widthCanvas =  1.2 * .maxWidth * .numCols + 4
		.xleft = 50 - .widthCanvas / 2
		.xright = 50 + .widthCanvas / 2
		.dx = (.widthCanvas - 4) / (.numCols)
	else
		.maxWidth = .dx - 1
	endif
	
	# Calculate length from number of lines.
 	.dy = .lineHeight + 0.5
	.midY = .yhigh - (.yhigh - .ylow)/2
	.yhigh = .midY + (.numLines+2) * .dy / 2
	.ylow = .yhigh - (.numLines+2) * .dy
	.textleft = .xleft + 2
	
	demo Line width... 8
 	demo Colour... 'sgc2.popUp_bordercolor$'
	demo Paint rectangle... 'sgc2.popUp_backgroundcolor$' '.xleft' '.xright' '.ylow' '.yhigh'
	demo Draw rectangle... '.xleft' '.xright' '.ylow' '.yhigh'
	demo Line width... 'defaultLineWidth'
	demo Black
 	.ytext = .yhigh - 2 - .dy
	# First the column names, then the items
	for .i from 0 to .numLines
		.xtext = .textleft + .dx / 2
		for .j to .numCols
			select '.tabbedText'
			.currentLabel$ = Get column label... '.j'
			if .i > 0
				.line$ = Get value... '.i' '.currentLabel$'
			else
				.line$ = .currentLabel$
				select Table '.labelText$'
    			call findLabel '.labelText$' '.line$'
    			select Table '.labelText$'
    			.line$ = Get value... 'findLabel.row' Text
			endif
			# Expand variables, eg, 'praatVersion$'
			call expand_praat_variables '.line$'
			.line$ = expand_praat_variables.text$
			call adjustFontSizeOnWidth '.font$' '.fontSize' '.maxWidth' '.line$'
			.currentFontSize = adjustFontSizeOnWidth.newFontSize

			# Display text
			demo Text special... '.xtext' Centre '.ytext' Bottom '.font$' '.currentFontSize' 0 '.line$'
			.xtext += .dx
		endfor
		.ytext -= .dy
	endfor	
	demoShow()	
	call set_font_size 'defaultFontSize'
	select Table '.labelText$'
	Remove
	
	label ESCAPEwrite_tabbed_table
endproc

# Create a pop-up window with a given text
procedure write_text_popup .font$ .size .text$
	.xleft = 10
 	.xright = 90
  	.ylow = 20
   	.yhigh = 85
	.lineHeight = 3

	# Adapt size of button to length of text
	.maxWidth = (.xright - .xleft) - 4
	call adjustFontSizeOnWidth 'defaultFont$' '.size' '.maxWidth' '.text$'
	call adjustFontSizeOnHeight 'defaultFont$' '.size' '.lineHeight'
	.popupFontSize = min(adjustFontSizeOnWidth.newFontSize, adjustFontSizeOnHeight.newFontSize)
	if adjustFontSizeOnWidth.diff > 0
		.xright += adjustFontSizeOnWidth.diff/4
		.xleft -= 3*adjustFontSizeOnWidth.diff/4
	else
		.xleft = ((.xright + .xleft) - adjustFontSizeOnWidth.textWidth)/2 - 2
		.xright = ((.xright + .xleft) + adjustFontSizeOnWidth.textWidth)/2 + 2
	endif

	.numRows = 1
	# Calculate length from number of lines.
 	.dy = .lineHeight
	.midY = .yhigh - (.yhigh - .ylow)/2
	.yhigh = .midY + (.numRows+1) * .dy / 2
	.ylow = .yhigh - (.numRows+1) * .dy
	.textleft = .xleft + 2
	.xmid = (.textleft + .xright - 2)/2
	
	demo Line width... 8
 	demo Colour... 'sgc2.popUp_bordercolor$'
	demo Paint rectangle... 'sgc2.popUp_backgroundcolor$' '.xleft' '.xright' '.ylow' '.yhigh'
	demo Draw rectangle... '.xleft' '.xright' '.ylow' '.yhigh'
	demo Line width... 'defaultLineWidth'
	demo Black
 	.ytext = .yhigh - 2 - .dy
	# Write text
	demo Text special... '.xmid' Centre '.ytext' Bottom '.font$' '.popupFontSize' 0 '.text$'

	demoShow()	
	demo 'defaultFont$'
	call set_font_size 'defaultFontSize'
endproc

# Write the background from a Text Table
procedure draw_background .table$
	.xleft = 0
 	.xright = 100
  	.ylow = 0
   	.yhigh = 100
	.lineHeight = 5
	.defaultColour$ = "{0.9,0.9,0.9}"
	.defaultAlign$ = "centre"

	# Get table with text and longest line
	call loadTable '.table$'
	.backgroundText = selected()
	.numLines = Get number of rows
	.backgroundFontSize = 28
	.referenceText$ = ""
	.maxlenght = 0
	.maxLine = 0
	.maxFontSize = 0
	.maxWidth = 0
	.textLines = 0
	for .l to .numLines
		select '.backgroundText'
		.currentText$ = Get value... '.l' text
		# Expand variables, eg, 'praatVersion$'
		call expand_praat_variables '.currentText$'
		.currentText$ = expand_praat_variables.text$		
		
		.font$ = Get value... '.l' font
		.fontSize = Get value... '.l' size
		if .fontSize > .maxFontSize
			.maxFontSize = .fontSize
		endif
		if not startsWith(.font$, "!")
			call set_font_size '.fontSize'
			.textWidth = demo Text width (wc)... '.currentText$'
			if .textWidth > .maxWidth
				.maxWidth = .textWidth
				.backgroundFontSize = .fontSize
				.maxLine = .l
			endif

			.textLines += 1
		endif
	endfor
	if .maxLine > 0
		select '.backgroundText'
		.referenceText$ = Get value... '.maxLine' text
		.maxLineFont$ = Get value... '.maxLine' font
		.backgroundFontSize = Get value... '.maxLine' size
		.backgroundFontColour$ = Get value... '.maxLine' colour
		call set_font_size '.maxFontSize'
	else
		.maxFontSize = .backgroundFontSize
	endif
	
	# Adapt size of button to length of text
	.maxWidth = (.xright - .xleft) - 4
	.origFontSize = .backgroundFontSize
	call adjustFontSizeOnWidth 'defaultFont$' '.backgroundFontSize' '.maxWidth' '.referenceText$'
	.fontSizeFactor = adjustFontSizeOnWidth.newFontSize / .backgroundFontSize
	.backgroundFontSize = adjustFontSizeOnWidth.newFontSize
	call set_font_size '.backgroundFontSize'
	
	call adjustFontSizeOnHeight 'defaultFont$' '.backgroundFontSize' '.lineHeight'
	.lineHeight /= adjustFontSizeOnHeight.newFontSize / .backgroundFontSize
	if adjustFontSizeOnHeight.newFontSize >= .origFontSize and (.textLines+1) * .lineHeight > (.yhigh - .ylow - 4)
		.lineHeight = (.yhigh - .ylow - 4)/(.textLines + 1)
		call adjustFontSizeOnHeight 'defaultFont$' '.maxFontSize' '.lineHeight'
		.fontSizeFactor = adjustFontSizeOnHeight.newFontSize / .backgroundFontSize
	endif

	.numRows = Get number of rows
	# Calculate length from number of lines.
 	.dy = .lineHeight
	.midY = .yhigh - (.yhigh - .ylow)/2
	.yhigh = .midY + (.textLines+1) * .dy / 2
	.ylow = .yhigh - (.textLines+1) * .dy
	.textleft = .xleft + 2
	.textright = .xright - 2
	.textmid = (.xright - .xleft)/2
	
	demo Black
 	.ytext = .yhigh - 2 - .dy
	for .i to .numRows
		select '.backgroundText'
		.font$ = Get value... '.i' font
		.fontSize = Get value... '.i' size
		.fontColour$ = Get value... '.i' colour
		.fontColour$ = replace_regex$(.fontColour$, "^[\- ]$", ".defaultColour$", 1)
		.fontAlign$ = Get value... '.i' align
		.fontAlign$ = replace_regex$(.fontAlign$, "^[\- ]$", ".defaultAlign$", 1)
		.line$ = Get value... '.i' text
		# Expand variables, eg, 'praatVersion$'
		call expand_praat_variables '.line$'
		.line$ = expand_praat_variables.text$
				
		 # Scale font
		 .fontSize = floor(.fontSize*.fontSizeFactor)
		if not startsWith(.font$, "!")
			.font$ = extractWord$(.font$, "")

			if .fontAlign$ = "centre"
				.xtext = .textmid
			elsif .fontAlign$ = "right"
				.xtext = .textright
			else
				.xtext = .textleft
			endif
			if .fontSize < 4
				.fontSize = 4
			endif
			# Clean up text
			demo Colour... '.fontColour$'
			demo Text special... '.xtext' '.fontAlign$' '.ytext' Bottom '.font$' '.fontSize' 0 '.line$'
			.ytext -= .dy
		elsif .font$ = "!demo command"
			demo Colour... '.fontColour$'
			.line$ = replace_regex$(.line$, "\{FONTSIZE\$\}", "'.fontSize'", 0)
			.line$ = replace_regex$(.line$, "\{XTEXT\$\}", "'.xtext'", 0)
			.line$ = replace_regex$(.line$, "\{YTEXT\$\}", "'.ytext'", 0)
			.line$ = replace_regex$(.line$, "\{DY\$\}", "'.dy'", 0)
			.line$ = replace_regex$(.line$, "\{[^\}]*\}", "", 0)
			while index(.line$, "[[")
				.nextBracketOpen = index(.line$, "[[")
				.nextBracketOpen += 2
				.nextBracketClose = index(.line$, "]]")
				.bracketLength = .nextBracketClose - .nextBracketOpen
				.result$ = ""
				if .bracketLength > 0
					.expression$ = mid$(.line$, .nextBracketOpen, .bracketLength)
					.expression$ = replace_regex$(.expression$, "\s", "", 0)
					if length(.expression$) > 0
						# Test expression for security, only allow explicitely defined functions
						.allowedStrings$ = "e|pi|not|and|or|div|mod|abs|round|floor|ceiling"
						.allowedStrings$ = .allowedStrings$ + "|sqrt|min|max|imin|imax|sin|cos|tan|arcsin|arccos|arctan|arctan2|sinc|sincpi"
						.allowedStrings$ = .allowedStrings$ + "|exp|ln|log10|log2|sinh|cosh|tanh|arcsinh|arccosh|arctanh"
						.allowedStrings$ = .allowedStrings$ + "|sigmoid|invSigmoid|erf|erfc|randomUniform|randomInteger|randomGauss|randomPoisson"
						.allowedStrings$ = .allowedStrings$ + "|lnGamma|gaussP|gaussQ|invGaussQ|chiSquareP|chiSquareQ"
						.allowedStrings$ = .allowedStrings$ + "|invChiSquareP|invChiSquareQ|studentP|studentQ|invStudentP|invStudentQ"
						.allowedStrings$ = .allowedStrings$ + "|beta|besselI|besselK"
						.testExpression$ = replace_regex$(.expression$, "(^|\W)('.allowedStrings$')(?=$|\W)", "\1\3", 0)
						.testExpression$ = replace_regex$(.testExpression$, "[0-9\.,\-+/*^()<>= ]", "", 0)
						if .testExpression$ = ""
							.calc = '.expression$'
							.result$ = "'.calc'"
						endif
					endif
				endif
				
				# Replace expression by result
				.lastLeft = .nextBracketOpen - 3
				.newLine$ = left$(.line$, .lastLeft)  
				.newLine$ =  .newLine$ + .result$
				.numCopy = length(.line$) - .nextBracketClose - 1
				.newLine$ =  .newLine$ + right$(.line$, .numCopy)
				.line$ = .newLine$
			endwhile
			demo '.line$'
		endif
	endfor	
	demo Black
	demoShow()	
	call set_font_size 'defaultFontSize'
	
	select '.backgroundText'
	Remove
endproc

procedure convert_praat_to_utf8 .text$
	.text$ = replace_regex$(.text$, "\\a""", "\xc3\xa4", 0)
	.text$ = replace_regex$(.text$, "\\A""", "\xc3\x84", 0)
	.text$ = replace_regex$(.text$, "\\o""", "\xc3\xb6", 0)
	.text$ = replace_regex$(.text$, "\\O""", "\xc3\x96", 0)
	.text$ = replace_regex$(.text$, "\\u""", "\xc3\xbc", 0)
	.text$ = replace_regex$(.text$, "\\U""", "\xc3\x9c", 0)
	.text$ = replace_regex$(.text$, "\\i""", "\xc3\xaf", 0)
	.text$ = replace_regex$(.text$, "\\I""", "\xc3\x8f", 0)
	.text$ = replace_regex$(.text$, "\\e""", "\xc3\xab", 0)
	.text$ = replace_regex$(.text$, "\\E""", "\xc3\x8b", 0)
	.text$ = replace_regex$(.text$, "\\y""", "\xc3\xbf", 0)
	.text$ = replace_regex$(.text$, "\\Y""", "\xc3\x9f", 0)
	.text$ = replace_regex$(.text$, "\\e'", "\xc3\xa9", 0)
	.text$ = replace_regex$(.text$, "\\E'", "\xc3\x89", 0)
	.text$ = replace_regex$(.text$, "\\ss", "\xc3\x9f", 0)
	.text$ = replace_regex$(.text$, "\\bu", "\xc3\x95", 0)
endproc

procedure convert_praat_to_latin1 .text$
	.text$ = replace_regex$(.text$, "\\a""", "\xe4", 0)
	.text$ = replace_regex$(.text$, "\\A""", "\xc4", 0)
	.text$ = replace_regex$(.text$, "\\o""", "\xf6", 0)
	.text$ = replace_regex$(.text$, "\\O""", "\xd6", 0)
	.text$ = replace_regex$(.text$, "\\u""", "\xfc", 0)
	.text$ = replace_regex$(.text$, "\\U""", "\xdc", 0)
	.text$ = replace_regex$(.text$, "\\i""", "\xef", 0)
	.text$ = replace_regex$(.text$, "\\I""", "\xcf", 0)
	.text$ = replace_regex$(.text$, "\\e""", "\xeb", 0)
	.text$ = replace_regex$(.text$, "\\E""", "\xcb", 0)
	.text$ = replace_regex$(.text$, "\\y""", "\xff", 0)
	.text$ = replace_regex$(.text$, "\\Y""", "\x9f", 0)
	.text$ = replace_regex$(.text$, "\\e'", "\xe9", 0)
	.text$ = replace_regex$(.text$, "\\E'", "\xc9", 0)
	.text$ = replace_regex$(.text$, "\\ss", "\xdf", 0)
	.text$ = replace_regex$(.text$, "\\bu", "\x95", 0)
endproc

# Expand 'variable$' into the value of variable$.
# Eg, 'praatVersion$' becomes 5.1.45 or whatever is the current version
# Single quotes can be protected by \'
procedure expand_praat_variables .text$
	if index(.text$, "'")
		.tempText$ = replace_regex$(.text$, "(^|[^\\])'([a-zA-Z0-9_\$\.]+)'", "\1""+\2+""", 0)
		.tempText$ = replace_regex$(.tempText$, "[\\]'", "'", 0)
		.tempText$ = """"+.tempText$+""""
		# Check whether all the variables actually exist. Ignore any variable that does not exist
		.checkVars$ = .tempText$
		while length(.checkVars$) > 0 and index(.checkVars$, "+")
			.start = index(.checkVars$, "+")
			.checkVars$ = right$(.checkVars$, length(.checkVars$) - .start)
			.end = index(.checkVars$, "+")
			if .end
				.variable$ = left$(.checkVars$, .end - 1)
				if not variableExists(.variable$)
					.tempText$ = replace$(.tempText$, """+'.variable$'+""", "'"+.variable$+"'", 0)
				endif
				.checkVars$ = right$(.checkVars$, length(.checkVars$) - .end)
			else
				.checkVars$ = ""
			endif
		endwhile
		.text$ = '.tempText$'
	endif
endproc

# Get a time stamp in normalized format
procedure getTimeStamp
	.currentDateTime$ = date$()
	.string$ = replace_regex$(.currentDateTime$, "[A-Z][a-z]+\s+([A-Z][a-z]+)\s+(\d+)\s+(\d+)\W(\d+)\W(\d+)\s+(\d+)$", "\6-\1-\2T\3-\4-\5", 0)
endproc

# A table error, can be insiduously caused by an outdate preferences file!
procedure emergency_table_exit .message$
	# If you come here as a user, your preferences file is borked
	if preferencesAppFile$ <> "" and fileReadable(preferencesAppFile$)
		deleteFile(preferencesAppFile$)
	endif
	# Put out message
	call get_feedback_text 'config.language$' Cancel
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.cancelText$ = convert_praat_to_latin1.text$
	beginPause: "Fatal Error"
	comment: .message$
	endPause: .cancelText$, 1
	exit '.message$'
endproc

# Remove previous files from system
procedure clean_up_sound
	if recordedSound$ = ""
		sgc.recordedSound = 0
	endif
    if sgc.recordedSound > 0
        select sgc.recordedSound
        Remove
        recordedSound$ = ""
        sgc.recordedSound = 0
    endif
    if te.recordedPitch > 0
        select te.recordedPitch
        Remove
		te.recordedPitch = 0
    endif
	if sgc.alignedTextGrid > 0
		select sgc.alignedTextGrid
		Remove
		sgc.alignedTextGrid = -1
	endif
endproc

# Safely read a table
procedure readTable .filename$
	.tableID = -1
	if .filename$ <> "" and fileReadable(.filename$) and index_regex(.filename$, "(?i\.(tsv|table|csv))$") > 0
		if index_regex(.filename$, "(?i\.(csv))$") > 0
			.tableID = Read Table from comma-separated file: .filename$
		elsif index_regex(.filename$, "(?i\.(tsv))$") > 0
			.tableID = nocheck Read Table from tab-separated file: .filename$
		else
			.tableID = nocheck Read from file... '.filename$'
		endif
		if .tableID = undefined or .tableID <= 0
			.tableID = -1
		else
			.fullName$ = selected$ ()
			.type$ = extractWord$(.fullName$, "")
			if .type$ <> "Table"
				Remove
				.tableID = -1
			endif
		endif
	endif
endproc

# Read feedback table and get keyed text
procedure get_feedback_text .language$ .key$
	if not endsWith(feedbackTableName$, "_'.language$'")
		if feedbackTableName$ <> ""
			select Table 'feedbackTableName$'
			Remove
		endif
		call testLoadTable 'feedbackTablePrefix$'_'.language$'
		if testLoadTable.table > 0
			call loadTable 'feedbackTablePrefix$'_'.language$'
		else
			call loadTable 'feedbackTablePrefix$'_EN
		endif
		feedbackTableName$ = selected$("Table")
	endif
	call findKey 'feedbackTableName$' '.key$'
	.row = findKey.row
	select Table 'feedbackTableName$'
	.text$ = Get value... '.row' Text
	# Expand variables, eg, 'praatVersion$'
	call expand_praat_variables '.text$'
	.text$ = expand_praat_variables.text$	
endproc


# Read evaluation table and get keyed text. Only praat converted Text
procedure get_evaluation_text .language$ .key$
	if not endsWith(evaluationTableName$, "_'.language$'")
		if evaluationTableName$ <> ""
			select Table 'evaluationTableName$'
			Remove
		endif
		call testLoadTable 'evaluationTablePrefix$'_'.language$'
		if testLoadTable.table > 0
			call loadTable 'evaluationTablePrefix$'_'.language$'
		else
			call loadTable 'evaluationTablePrefix$'_EN
		endif
		evaluationTableName$ = selected$("Table")
	endif
	call findLabel 'evaluationTableName$' '.key$'
	.row = findLabel.row
	select Table 'evaluationTableName$'
	.text$ = Get value... '.row' Text
	# Expand variables, eg, 'praatVersion$'
	call expand_praat_variables '.text$'
	.rawtext$ = expand_praat_variables.text$
	call convert_praat_to_latin1 '.rawtext$'
	.text$ = convert_praat_to_latin1.text$
endproc

# Read all the relevant evaluation labels and put them in "eval.<label>$" variables
procedure get_evaluation_table_labels .language$
	call get_evaluation_text '.language$' Performance
	eval.performance$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Pinyin
	eval.pinyin$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Correct
	eval.correct$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Wrong
	eval.wrong$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Total
	eval.total$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' High
	eval.high$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Low
	eval.low$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Wide
	eval.wide$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Narrow
	eval.narrow$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Unknown
	eval.unknown$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Commented
	eval.commented$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Level
	eval.level$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Time
	eval.time$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Wordlist
	eval.wordlist$ = get_evaluation_text.text$
	call get_evaluation_text '.language$' Grade
	eval.grade$ = get_evaluation_text.text$
	
endproc

# log activity
procedure saveLogOfActivity .command$
	# Do not log in binary!
	if build_SHA$ = "-"
		createDirectory(preferencesLogDir$)
		appendFileLine: "'preferencesLogDir$'/'logtimeStamp$'.log", .command$
	endif
endproc

# Replay a log file with commands sgc.replaySleep inserts a pause
procedure replaySGC2LogFunction
	if not variableExists("sgc.replaySleep")
		sgc.replaySleep = -1
	endif
	# Do not replay in binary!
	if build_SHA$ = "-"
		# Ask for the input file
		.filename$ = chooseReadFile$ ("Select file to replay")
		if .filename$ <> "" and fileReadable(.filename$)
			.replayFile = Read Strings from raw text file: .filename$
			if .replayFile <> undefined
				# Pre-pause
				if sgc.replaySleep > 0
					call basic_sound_recording 'samplingFrequency' 'sgc.replaySleep'
					Remove
				endif
				select .replayFile
				.numStrings = Get number of strings
				for .l to .numStrings
					select .replayFile
					.line$ = Get string: .l
					if index_regex(.line$, "process(MainPage|Config)(Help|Config|Return|Quit)")
						if index(.line$, "processMainPageConfig")
							.line$ = "call Draw_config_page"
						elsif index_regex(.line$, "processConfigReturn")
							.line$ = "call init_window"
						elsif index_regex(.line$, "processMainPageHelp")
							.line$ = "call init_window"
						elsif index_regex(.line$, "processConfigHelp")
							.line$ = "call Draw_config_page"
						else
							.line$ = "# " + .line$
						endif
					endif
					if index_regex(.line$, "[a-zA-Z]") > 0 and index_regex(.line$, "\s*#") <= 0
						# Execute
						'.line$'
						
						# Pause
						if sgc.replaySleep > 0
							call basic_sound_recording 'samplingFrequency' 'sgc.replaySleep'
							Remove
						endif
					endif
				endfor
			endif
		endif
	endif
endproc
