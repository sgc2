#
# SpeakGoodChinese 2.0
# 
# Praat script handling buttons page
#
#     SpeakGoodChinese: MainPage.praat loads the code needed for the 
#     main, practice, page of SGC2 and the sound handling and recognition.
#     
#     Copyright (C) 2007-2010  R.J.J.H. van Son and 2010 the Netherlands Cancer Institute
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Koning, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
# Includes at the bottom
#
###############################################################
#
# Button Drawing Routines
#
###############################################################

procedure DrawPrevious .color$ .x .y .size
	demo '.color$'
	.size *= 2/3
	.fraction = 0
	if sgc.numberOfDisplayedWords > 0
		.fraction = sgc.currentWordNum/sgc.numberOfDisplayedWords
	endif
	call drawProgressTriangle -1 .x .y .size .fraction '.color$' Green
	.currentX = drawProgressTriangle.currentX
	.endX = .currentX - 0.5
	.lowY = .y - .size
	.highY = .y + .size
	.displayColor$ = .color$
	if sgc.currentWordNum >= sgc.numberOfDisplayedWords
		.displayColor$ = "Green"
	endif
	demo Paint rectangle... '.displayColor$' '.currentX' '.endX' '.lowY' '.highY'
endproc

procedure DrawNext .color$ .x .y .size
	demo '.color$'
	.size *= 2/3
	.fraction = 0
	if sgc.numberOfDisplayedWords > 0
		.fraction = sgc.currentWordNum/sgc.numberOfDisplayedWords
	endif
	call drawProgressTriangle 1 .x .y .size .fraction '.color$' Green
	.currentX = drawProgressTriangle.currentX
	.endX = .currentX + 0.5
	.lowY = .y - .size
	.highY = .y + .size
	.displayColor$ = .color$
	if sgc.currentWordNum >= sgc.numberOfDisplayedWords
		.displayColor$ = "Green"
	endif
	demo Paint rectangle... '.displayColor$' '.currentX' '.endX' '.lowY' '.highY'
endproc

procedure DrawSelectWords .color$ .x .y .size
	.currentFontSize = 48
	.y -= .size/2 + 0.5
	.maxHeight = 2*.size
	call adjustFontSizeOnHeight 'defaultFont$' '.currentFontSize' '.maxHeight'
	.currentFontSize = adjustFontSizeOnHeight.currentFontSize
	call set_font_size '.currentFontSize'
	demo Colour... '.color$'
	demo Text... '.x' Centre '.y' Bottom \bu\bu\bu
	call set_font_size 'defaultFontSize'
endproc

procedure DrawWordListUp .color$ .x .y .size
    .xleft = .x - .size
    .xright = .x + .size
    .xmidleft = .x + 0.1
    .xmidright = .x - 0.1
    .ylow = .y
    .yhigh = .y + .size
	demo '.color$'
	demo Line width... 3
	demo Draw line... .xleft .ylow .xmidleft .yhigh
	demo Draw line... .xright .ylow .xmidright .yhigh
	demo Line width... 'defaultLineWidth'
   	demo Black
endproc

procedure DrawWordListDown .color$ .x .y .size
    .xleft = .x - .size
    .xright = .x + .size
    .xmidleft = .x + 0.1
    .xmidright = .x - 0.1
    .yhigh = .y + .size
    .ylow = .y
	demo '.color$'
	demo Line width... 3
	demo Draw line... .xleft .yhigh .xmidleft .ylow
	demo Draw line... .xright .yhigh .xmidright .ylow
	demo Line width... 'defaultLineWidth'
   	demo Black
endproc

procedure drawTriangle .direction .x .y .size
	# Make sure direction = +/- 1
	if .direction = 0
		.direction = 1 
	endif
	.direction /= abs(.direction)
	
	.offset = 0.01
	.currentHeight = .size
	.currentX = .x - .direction*.size
	
	demo Line width... 2.0

	while .currentHeight> 0
		.ystart = .y + .currentHeight
		.yend = .y - .currentHeight
		demo Draw line... .currentX .ystart .currentX .yend
		.currentHeight -= .offset *3/4
		.currentX += .direction*.offset * 1.5
	endwhile
	demo Line width... 'defaultLineWidth'
endproc

procedure drawProgressTriangle .direction .x .y .size .fraction .color1$ .color2$
	# Make sure direction = +/- 1
	if .direction = 0
		.direction = 1 
	endif
	.direction /= abs(.direction)
	
	.offset = 0.01
	.currentHeight = .size
	.startX = .x - .direction*.size
	.currentX = .startX
	
	demo Line width... 2.0

	while .currentHeight> 0
		# Implement progress bar in .color2$
		if .direction*(.currentX - .startX)/.size  <= 2*.fraction
			demo '.color2$'
		else
			demo '.color1$'
		endif
		
		.ystart = .y + .currentHeight
		.yend = .y - .currentHeight
		demo Draw line... .currentX .ystart .currentX .yend
		.currentHeight -= .offset *3/4
		.currentX += .direction*.offset * 1.5
	endwhile
	demo Line width... 'defaultLineWidth'
	demo '.color1$'
endproc

###############################################################
#
# Obligatory button Drawing Routines
# 
# These MUST be defined
#
###############################################################

procedure DrawRecord .color$ .x .y .size
	.size /= 2
    demo Paint circle... '.color$' '.x' '.y' '.size'
endproc

procedure DrawPlay .color$ .x .y .size
	demo '.color$'
	call drawTriangle 1 .x .y .size
endproc

procedure DrawQuit .color$ .x .y .size
	demo Colour... '.color$'
	.lineWidth = 0.5*.size**2
	demo Line width... '.lineWidth'
	.xstart = .x - .size
	.ystart = .y + .size
	.xend = .x + .size
	.yend = .y - .size
	demo Draw line... .xstart .ystart .xend .yend
	.xstart = .x - .size
	.ystart = .y - .size
	.xend = .x + .size
	.yend = .y + .size
	demo Draw line... .xstart .ystart .xend .yend
	demo Line width... 'defaultLineWidth'
	demo Colour... Black
endproc

procedure DrawConfig .color$ .x .y .size
	.size *= 1
	.lineWidth = 0.4*.size
	demo Arrow size... '.lineWidth'
	.lineWidth = 0.4*.size**2
	demo Line width... '.lineWidth'
	.y += .size/2
	.xstart = .x - .size
	.xend = .x + .size
	demo Draw arrow... .xstart .y .xend .y
	demo Line width... 'defaultLineWidth'
endproc

procedure DrawRefresh .color$ .x .y .size
	.lineWidth = 0.5*.size**2
	.size /= 2
	demo Line width... '.lineWidth'
	demo Draw arc... '.x' '.y' '.size' 0 270
	demo Line width... 'defaultLineWidth'
endproc

###############################################################
#
# Button Processing Routines
#
###############################################################

procedure processMainPageExample .clickX .clickY .pressed$
	call generate_example
endproc

procedure processMainPagePrevious .clickX .clickY .pressed$
	call clean_up_sound
	call display_text Grey
	call previous_word
	# Draw the contour
	call wipeArea 'wipeFeedbackArea$'
	call init_window
	call display_text Black
	
	sgc.failedAttempts = 0
endproc

procedure processMainPageNext .clickX .clickY .pressed$
	call clean_up_sound
	call display_text Grey
	call next_word
	call wipeArea 'wipeFeedbackArea$'
	call init_window
	# Draw the contour
	call display_text Black
endproc

procedure processMainPageWordlistUp .clickX .clickY .pressed$
	call wipeArea 'wipeFeedbackArea$'
    call load_word_list "'localWordlistDir$'" -1
	call init_window
	call display_text Black
endproc

procedure processMainPageWordlistDown .clickX .clickY .pressed$
	call wipeArea 'wipeFeedbackArea$'
    call load_word_list "'localWordlistDir$'" 1
	call init_window
	call display_text Black
endproc

procedure processMainPageGRADE .clickX .clickY .pressed$
	call setGrade '.pressed$'
	# Redraw window
	call init_window
endproc

procedure processMainPagePinYinArea .clickX .clickY .pressed$
	# Redraw window
	sgc.writeAll = 1
	call display_text Red
	sgc.writeAll = 0
endproc

# Select the words to practise. This is quite a complex piece of code
procedure processMainPageSelectWords .clickX .clickY .pressed$
	.table$ = "MainPage"
	.label$ = "SelectWords"
    call Draw_button '.table$' '.label$' 1

 	# Get help text
	call findLabel '.table$' '.label$'
	.row = findLabel.row
	select Table '.table$'
	.helpText$ = Get value... '.row' Helptext
	call convert_praat_to_latin1 '.helpText$'
	.helpText$ = convert_praat_to_latin1.text$
   
    # Implement Cancel
    select sgc.currentWordlist
    .tmpOriginalWordlist = Copy: "Original_'wordlist$'"

	# Remove current list from All wordlists table
	select sgc.allWordLists
	.rowNum = Search column: "Name", wordlistName$
	if .rowNum > 0
		.numRows = Get number of rows
		if .numRows > 1
			Remove row: .rowNum
		else
			Set string value: 1, "Name", "---"
		endif
	endif
    
    # Set current word
	select sgc.currentWordlist
	sgc.numberOfWords = Get number of rows
    .currentWord = sgc.currentWord
    if .currentWord <= 0 or .currentWord > sgc.numberOfWords or config.shuffleLists
		.currentWord = 1
	endif
    
	# The texts
	call get_feedback_text 'config.language$' SelectWordlist
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.selectWordlistText$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' AddWordlist
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.wordlistText$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' Part
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.partText$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' Tones
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.toneText$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' Cancel
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.cancelText$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' Clear
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.clearText$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' All
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.allText$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' Apply
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.applySelection$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' Next
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.nextWord$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' Continue
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.continueText$ = convert_praat_to_latin1.text$
	call get_feedback_text 'config.language$' Show
	call convert_praat_to_latin1 'get_feedback_text.text$'
	.showText$ = convert_praat_to_latin1.text$

	call get_evaluation_text 'config.language$' Pinyin
	.pinyinText$ = get_evaluation_text.text$
	
	call get_evaluation_text 'config.language$' Character
	.characterText$ = get_evaluation_text.text$
	
	call get_evaluation_text 'config.language$' Example
	.exampleText$ = get_evaluation_text.text$
	
	call get_evaluation_text 'config.language$' Translation
	.translationText$ = get_evaluation_text.text$
	
	call get_evaluation_text 'config.language$' ManualEntry
	.manualEntryText$ = get_evaluation_text.text$
	
	# Get lesson names
	select sgc.currentWordlist
	.lessonCol = Get column index: "Lesson"
	if .lessonCol <= 0
		Append column: "Lesson"
		.lessonCol = Get column index: "Lesson"
		.numRows = Get number of rows
		for .w to .numRows
			Set string value: .w, "Lesson", "-"
		endfor
	endif

	# All shown keeps track whether all words are shown
	# If so, selecting a lesson is preceded by a Clear All
	.allShown = 0
	
	clicked = -1
	.numWordsPerScreen = 15
	while clicked <> 6 and clicked <> 1
		.lessonSelected = -1
		.toneSelected = -1
		.firstShown = -1
		
		# A new list might have been added!
		.lessonList$ = tab$
		.numLessons = 0
		.lessonName$ = ""
		# Get lesson names
		select sgc.currentWordlist
		.lessonCol = Get column index: "Lesson"
		if .lessonCol <= 0
			Append column: "Lesson"
			.lessonCol = Get column index: "Lesson"
			.numRows = Get number of rows
			for .w to .numRows
				Set string value: .w, "Lesson", "-"
			endfor
		endif
		.lessonList$ = tab$
		.numLessons = 0
		.lessonName$ = ""
		# All shown keeps track whether all words are shown
		# If so, selecting a lesson is preceded by a Clear All
		.allShown = 0
		# Sort words for consistent selection interface
		if config.shuffleLists
			# Allow subdivision in lessons
			if .lessonCol > 0
				Sort rows... Lesson Pinyin
			else
				Sort rows... Pinyin
			endif
		endif		
		if .lessonCol > 0
			for .i to sgc.numberOfWords
				.currentLesson$ = Get value: .i, "Lesson"
				.matchLesson$ = tab$+.currentLesson$+tab$
				if .currentLesson$ <> "" and .currentLesson$ <> "-" and .currentLesson$ <> "?" and index(.lessonList$, .matchLesson$) <= 0
					.lessonList$ = .lessonList$ + .currentLesson$ + tab$
					.numLessons += 1
					.lessonName$['.numLessons'] = .currentLesson$
				endif
				.shown$ = Get value: .i, "Show"
				if .shown$ = "-"
					.allShown = 0
				endif
			endfor
		endif


		# Sort words for consistent selection interface (watch out, List can be changed)
        if config.shuffleLists
			# Allow subdivision in lessons
			if .numLessons > 1
				Sort rows... Lesson Pinyin
			else
				Sort rows... Pinyin
			endif
        endif		
		.max = .numWordsPerScreen - 1
		if .currentWord + .max > sgc.numberOfWords
			.max = sgc.numberOfWords - .currentWord
		endif
		for .i from 0 to .numWordsPerScreen - 1
			if .i <= .max
				.currentPinyin$ = Get value: .currentWord+.i, sgcwordlist.word$
				if index_regex(.currentPinyin$, "[0-9]") <= 0
					call pinyin2numbers '.currentPinyin$'
					.currentPinyin$ = replace_regex$(pinyin2numbers.intermediatePinyin$, ".+", "\l&", 0)
					.currentPinyin$ = replace_regex$(.currentPinyin$, "[^a-zA-Z0-9]", "", 0)
				endif
				.pinyin$[.i] = .currentPinyin$

				.character$[.i] = Get value: .currentWord+.i, sgcwordlist.graph$
				.lessonNum$[.i] = ""
				if .numLessons > 1
					.lessonNum$[.i] = Get value: .currentWord+.i, "Lesson"
					.lessonNum$[.i] = " : " + .lessonNum$[.i]
				endif
				.showText$[.i] = .pinyin$['.i']
				if .character$['.i'] <> "-"
					.showText$[.i] = .pinyin$['.i']+" ("+.character$['.i']+.lessonNum$['.i']+")"
				elsif .lessonNum$['.i'] <> ""
					.showText$[.i] = .pinyin$['.i']+" ( -"+.lessonNum$['.i']+")"
				endif
				.showValueText$[.i] = Get value: .currentWord+.i, "Show"
				if .showValueText$[.i] = "-"
					.showValue[.i] = 0
				else
					.showValue[.i] = 1
				endif
				.showVariable$[.i] = .pinyin$[.i]
				# This prevents some nasty attempts to use language elements as variables
				if index_regex(.showVariable$[.i], "[^0-9r]$") or index_regex(.showVariable$[.i], "[0-9]") <= 0 
					.showVariable$[.i] = .pinyin$[.i]+"0"
				endif
				.tmp$ = replace_regex$(.showVariable$[.i], "^(.+)$", "\l\1", 0)
				.tmp$ = replace_regex$(.tmp$, "\s", "_", 0)
				'.tmp$' = .showValue[.i]
			else
				.showText$[.i] = "-"
				.showValue[.i] = 0
			endif
		endfor

		# The user text input window (beginPause .... endPause)
		beginPause(.helpText$)
			if sgc.allWordLists > 0
				select sgc.allWordLists
				.numWordlists = Get number of rows
				optionMenu: .selectWordlistText$, 1
					option: "---"
					for .w to .numWordlists
						select sgc.allWordLists
						.wordListName$ = Get value: .w, "Name"
						option: .wordListName$
					endfor
					# Open Empty list?
					# option: "*"
			endif
			
			if sgc.allWordLists > 0
				select sgc.allWordLists
				.numWordlists = Get number of rows
				optionMenu: .wordlistText$, 1
					option: "---"
					for .w to .numWordlists
						select sgc.allWordLists
						.wordListName$ = Get value: .w, "Name"
						option: .wordListName$
					endfor
					option: "*"+.manualEntryText$+"*"
			endif
			
			boolean (.showText$[0], .showValue[0])
			boolean (.showText$[1], .showValue[1])
			boolean (.showText$[2], .showValue[2])
			boolean (.showText$[3], .showValue[3])
			boolean (.showText$[4], .showValue[4])
			boolean (.showText$[5], .showValue[5])
			boolean (.showText$[6], .showValue[6])
			boolean (.showText$[7], .showValue[7])
			boolean (.showText$[8], .showValue[8])
			boolean (.showText$[9], .showValue[9])
			boolean (.showText$[10], .showValue[10])
			boolean (.showText$[11], .showValue[11])
			boolean (.showText$[12], .showValue[12])
			boolean (.showText$[13], .showValue[13])
			boolean (.showText$[14], .showValue[14])
			
			.selectMenu$ = .toneText$
			if .numLessons > 0
				.selectMenu$ = .selectMenu$ + " " + .partText$
			endif
			optionMenu: .selectMenu$, 1
				option: .toneText$
				option: "0"
				option: "1"
				option: "2"
				option: "3"
				option: "4"
			
			# Display Lesson text if available
			if .numLessons > 0
				option: "---"
				.j = 0
				for .j to .numLessons
					option: .partText$+": "+.lessonName$['.j']
				endfor
			endif
		clicked = endPause ("'.cancelText$'", "'.clearText$'", "'.allText$'", "'.applySelection$'", "'.nextWord$'", "'.continueText$'", 6, 1)
		
		select sgc.currentWordlist
		if clicked = 2
			for .i to sgc.numberOfWords
				Set string value: .i, "Show", "-"
			endfor
			.allShown = 0
		elsif clicked = 3
			for .i to sgc.numberOfWords
				Set string value: .i, "Show", "+"
			endfor
			.allShown = 1
		elsif clicked != 1
			# Switch to new wordlist
			.tmp$ = replace_regex$(.selectWordlistText$, "^(.)", "\l\1", 0)
			.tmp$ = replace_regex$(.tmp$, "\s", "_", 0)
			.tmp$ = '.tmp$'$
			if .tmp$ <> "---" and sgc.allWordLists > 0
				# Existing lessons
				select sgc.allWordLists
				.wordlistNum = Search column: "Name", .tmp$
				if .wordlistNum > 0
					wordlistName$ = .tmp$
					call load_word_list "'localWordlistDir$'" 0
					call init_window
					.currentWord = 1
					
					goto NEXTSELECTWORDLOOP
				endif
				
			endif
			
			# Handle added word lists
			.tmp$ = replace_regex$(.wordlistText$, "^(.)", "\l\1", 0)
			.tmp$ = replace_regex$(.tmp$, "\s", "_", 0)
			.tmp$ = '.tmp$'$
			if .tmp$ <> "---" and sgc.allWordLists > 0
				select sgc.currentWordlist
				# Add current wordlist name as  a lesson name
				.originalWordList = -1
				.numRows = Get number of rows
				for .w to .numRows
					select sgc.currentWordlist
					.currentLesson$ = Get value: .w, "Lesson"
					if .currentLesson$ = "" or .currentLesson$ = "-"
						Set string value: .w, "Lesson", wordlist$
					endif
				endfor
				
				# Get and merge the selected list
				# Enter words by hand
				if .tmp$ = "*"+.manualEntryText$+"*"
					.man_clicked = 3
					.currentFilePath$ = .exampleText$
					.manualText$ = .manualEntryText$
					.manPinyin$ = .pinyinText$
					.manCharacter$ = .characterText$
					.manTranslation$ = .translationText$
					.manSound$ = .currentFilePath$
					.manLesson$ = .manualText$
					while .man_clicked > 1
						beginPause: .manualEntryText$
							text: .pinyinText$, .manPinyin$
							text: .characterText$, .manCharacter$ 
							text: .translationText$, .manTranslation$ 
							text: .exampleText$, .manSound$
							text: .manualText$, .manualText$
						.man_clicked = endPause("'.cancelText$'", "'.exampleText$'", "'.continueText$'", 3, 1)
						if .man_clicked > 1
							.tmp$ = replace_regex$(.pinyinText$, "\s", "_", 0)
							.tmp$ = replace_regex$(.tmp$, "^(.+)$", "\l\1", 0)
							if '.tmp$'$ <> .pinyinText$ 
								.manPinyin$ = '.tmp$'$
							else
								# Quit
								.man_clicked = 0
							endif

							.tmp$ = replace_regex$(.characterText$, "\s", "_", 0)
							.tmp$ = replace_regex$(.tmp$, "^(.+)$", "\l\1", 0)
							if '.tmp$'$ <> .characterText$ 
								.manCharacter$ = '.tmp$'$
							else
								.manCharacter$ = ""
							endif
							
							.tmp$ = replace_regex$(.translationText$, "\s", "_", 0)
							.tmp$ = replace_regex$(.tmp$, "^(.+)$", "\l\1", 0)
							if '.tmp$'$ <> .translationText$ 
								.manTranslation$ = '.tmp$'$
							else
								.manTranslation$ = ""
							endif
							
							.tmp$ = replace_regex$(.exampleText$, "\s", "_", 0)
							.tmp$ = replace_regex$(.tmp$, "^(.+)$", "\l\1", 0)
							if '.tmp$'$ <> .characterText$ and '.tmp$'$ <> ""
								.manSound$ = '.tmp$'$
							endif
							
							.tmp$ = replace_regex$(.manualText$, "\s", "_", 0)
							.tmp$ = replace_regex$(.tmp$, "^(.+)$", "\l\1", 0)
							.manLesson$ = '.tmp$'$
							
							if .man_clicked = 2
								.manSound$ = chooseReadFile$(.exampleText$);
							elsif .man_clicked = 3
								# Clean up input string
								.manPinyin$ = replace_regex$(.manPinyin$, "^[^a-z]+", "", 0)
								.manPinyin$ = replace_regex$(.manPinyin$, "[^a-zA-Z0-9_']", "", 0)
								
								select sgc.currentWordlist
								Append row
								.numRows = Get number of rows
								if .manPinyin$ <> ""
									Set string value: .numRows, sgcwordlist.word$, .manPinyin$
									Set string value: .numRows, sgcwordlist.graph$, .manCharacter$
									Set string value: .numRows, sgcwordlist.transl$, .manTranslation$
									Set string value: .numRows, sgcwordlist.audio$, .manSound$
									Set string value: .numRows, "Lesson", .manLesson$
									Set string value: .numRows, "Show", "+"
								endif
								.manualText$ = .manualEntryText$
								.manPinyin$ = .pinyinText$
								.manCharacter$ = .characterText$
								.manTranslation$ = .translationText$
								.manSound$ = .currentFilePath$
								.manLesson$ = .manualText$							
							endif
						endif
					endwhile					
				else
					# Existing lessons
					select sgc.allWordLists
					.wordlistNum = Search column: "Name", .tmp$
					if .wordlistNum > 0
						.wordlistPath$ = Get value: .wordlistNum, "Directory"
						.wordlistPath$ = replace_regex$(.wordlistPath$, "[ ]", "&", 0)
						call read_wordlist "'.tmp$'" '.wordlistPath$'
						.newList = read_wordlist.wordlistID
						call merge_into_wordlist '.newList' '.tmp$'
						select .newList
						Remove
						# Add wordlistname to Lesson column
						select sgc.allWordLists
						.numRows = Get number of rows
						if .numRows > 1
							Remove row: .wordlistNum
						else
							Set string value: 1, "Name", "---"
						endif
					endif
				endif
				# Gather Lesson names
				select sgc.currentWordlist
				Sort rows... Lesson Pinyin
				if .lessonCol > 0
					sgc.numberOfWords = Get number of rows
					.lessonList$ = tab$
					.numLessons = 0
					.lessonName$ = ""
					for .i to sgc.numberOfWords
						.currentLesson$ = Get value: .i, "Lesson"
						.matchLesson$ = tab$+.currentLesson$+tab$
						if .currentLesson$ <> "" and .currentLesson$ <> "-" and .currentLesson$ <> "?" and index(.lessonList$, .matchLesson$) <= 0
							.lessonList$ = .lessonList$ + .currentLesson$ + tab$
							.numLessons += 1
							.lessonName$['.numLessons'] = .currentLesson$
						endif
						.shown$ = Get value: .i, "Show"
						if .shown$ = "-"
							.allShown = 0
						endif
					endfor
				endif
			else			
				for .i from 0 to .max
					.tmp$ = replace_regex$(.showVariable$['.i'], "^(.*)$", "\l\1", 0)
					.tmp$ = replace_regex$(.tmp$, "\s", "_", 0)
					
					.showValue['.i'] = '.tmp$'
					.showWord$['.i'] = "-"
					if .showValue['.i'] <> 0
						.showWord$['.i'] = "+"
					endif
					Set string value: .currentWord+.i, "Show", .showWord$['.i']
				endfor
			endif
			
			select sgc.currentWordlist
			.tmp$ = replace_regex$(.selectMenu$, "^(.)", "\l\1", 0)
			.tmp$ = replace_regex$(.tmp$, "\s", "_", 0)
			.toneSelected = '.tmp$' - 2
			if .toneSelected >= 0 and .toneSelected < 5
				select sgc.currentWordlist
				for .i to sgc.numberOfWords
					.currentPinyin$ = Get value: .i, sgcwordlist.word$
					if index_regex(.currentPinyin$, "[0-9]") <= 0
						call pinyin2numbers '.currentPinyin$'
						.currentPinyin$ = replace_regex$(pinyin2numbers.intermediatePinyin$, ".+", "\l&", 0)
						.currentPinyin$ = replace_regex$(.currentPinyin$, "[^a-zA-Z0-9]", "", 0)
					endif
					.tmp$ = Get value: .i, "Show"
					if .tmp$ <> "-" and index(.currentPinyin$, "'.toneSelected'") > 0
						Set string value: .i, "Show", "+"
					else
						Set string value: .i, "Show", "-"
						.allShown = 0
					endif
				endfor
			elsif .toneSelected > 5
				# .toneSelected = 5 is the Part text
				.lessonSelected = .toneSelected - 5
				if .lessonSelected > 0
					.allShown = 1
					.firstShown = -1
					select sgc.currentWordlist
					for .i to sgc.numberOfWords
						# Keep track of whether all are shown
						.shown$ = Get value: .i, "Show"
						if .shown$ = "-"
							.allShown = 0
						endif
					endfor

					for .i to sgc.numberOfWords
						# Lessons
						.currentLesson$ = Get value: .i, "Lesson"
						if .currentLesson$ = .lessonName$['.lessonSelected']
							Set string value: .i, "Show", "+"
						elsif .allShown = 1
							Set string value: .i, "Show", "-"
						endif
					endfor
					for .i to sgc.numberOfWords
						# Keep track of whether all are shown
						.shown$ = Get value: .i, "Show"
						if .firstShown <=0 and .shown$ <> "-"
							.firstShown = .i
						endif
					endfor
				endif
			endif
			
			if clicked = 4
				if .currentWord <= 0
					.currentWord = (sgc.numberOfWords div .numWordsPerScreen) * .numWordsPerScreen + 1
				endif
			elsif clicked = 5
				if .firstShown > 0
					.currentWord = (.firstShown div .numWordsPerScreen) * .numWordsPerScreen + 1
				endif
				.currentWord += .numWordsPerScreen
				if .currentWord > sgc.numberOfWords
					.currentWord = 1
				endif
			endif
		endif
		# Reset and go to the first selected word (can shuffle list)
		if clicked = 6
			sgc.currentWord = -1
			call next_word
			call init_window
		endif
		
		label NEXTSELECTWORDLOOP
	endwhile
	
    # Implement Cancel
    if clicked = 1
		select sgc.currentWordlist
		Remove
		select .tmpOriginalWordlist
		Copy: wordlist$
		sgc.currentWordlist = selected()
	else
		# Set the values of the number of words shown	
		select sgc.currentWordlist
		.numWords = Get number of rows
		sgc.currentWordNum = 1
		sgc.numberOfDisplayedWords = 0
		sgc.currentWord	= 0
		for .i to .numWords
			.show$ = Get value: .i, "Show"
			if .show$ = "+"
				if sgc.currentWord < 1
					sgc.currentWord = .i
				endif
				sgc.numberOfDisplayedWords += 1
			endif
		endfor
	endif
	select .tmpOriginalWordlist
	Remove

	label SELECTWORDSFAIL
	
    call Draw_button '.table$' '.label$' 0
endproc

###############################################################
#
# Obligatory button Processing Routines
# 
# These MUST be defined
#
###############################################################

procedure processMainPageQuit .clickX .clickY .pressed$
	call end_program
endproc

procedure processMainPageRefresh .clickX .clickY .pressed$
	call clean_up_sound
	call init_window
endproc

procedure processMainPageConfig .clickX .clickY .pressed$
	call config_page
endproc

procedure processMainPageHelp .clickX .clickY .pressed$
	call help_loop 'buttons$' init_window
endproc

procedure processMainPagePlay .clickX .clickY .pressed$
	.table$ = "MainPage"
	.label$ = "Play"
	if recordedSound$ <> ""
		call play_sound 'sgc.recordedSound'
		mainPage.play = 0
	endif
	te.buttonPressValue = mainPage.play
endproc

procedure Set_Play_Button
	mainPage.play = -1
	if recordedSound$ <> ""
		mainPage.play = 0
    endif 	
	call Draw_button MainPage Play 'mainPage.play'
endproc

procedure processMainPageRecord .clickX .clickY .pressed$
	call count_syllables
	.recordingTime = recordingTime
	if count_syllables.number > 2
		.recordingTime = recordingTime + 1.0*ceiling((count_syllables.number - 2)/2)
	endif
    call record_sound '.recordingTime'
    call recognizeTone
	mainPage.play = 0
	call Set_Play_Button

    # Wipe screen
    call wipeArea 'wipeContourArea$'
   
    call draw_tone_contour
    # Write text (again)
    call display_word_list_name
    call display_text Black
	call add_feedback_to_toneevaluation Feedback
    call write_feedback Feedback
	select Table Feedback
	Remove
	
	# Do not exercise words that are going well (autoSelect)
    if add_feedback_to_toneevaluation.result > 0
		if config.adaptiveLists > 0 and sgc.failedAttempts < 2
			# Deselect current word
			select sgc.currentWordlist
			.i = Search column: sgcwordlist.word$, sgc.pinyin$
			if .i > 0
				Set string value: .i, "Show", "-"
			endif
		endif
		sgc.failedAttempts = 0
    else
		sgc.failedAttempts += 1
    endif
endproc


###############################################################
#
# Miscelaneous supporting code
#
###############################################################

# The example
procedure generate_example
	select sgc.currentWordlist
	if sgc.currentWord > 0 and sgc.currentWord <= sgc.numberOfWords
		select sgc.currentWordlist
		.sound$ = Get value... 'sgc.currentWord' Sound
		call readPinyin 'sgc.currentWord'
		sgc.pinyin$ = readPinyin.pinyin$
		sgc.character$ = readPinyin.character$
		if .sound$ = "-" or .sound$ = ""
			.sound$ = sgc.pinyin$+".wav"
		endif
		if index_regex(.sound$, "^(/|~/|[A-Z]:\\)") > 0
			.soundFilePath$ = .sound$
		else
			.soundFilePath$ = localWordlistDir$+"/"+wordlistName$+"/"+.sound$
			.wordlistDirectory$ = ""
			if localWordlistDir$ <> "" and fileReadable("'localWordlistDir$'/'wordlistName$'")
				.wordlistDirectory$ = "'localWordlistDir$'/'wordlistName$'"
			elsif sgc2wordlists$ <> "" and fileReadable("'sgc2wordlists$'/'wordlistName$'")
				.wordlistDirectory$ = "'sgc2wordlists$'/'wordlistName$'"
			elsif globalwordlists$ <> "" and fileReadable("'globalwordlists$'/'wordlistName$'")
				.wordlistDirectory$ = "'globalwordlists$'/'wordlistName$'"
			endif
			if .wordlistDirectory$ <> ""
				.audioExampleList = Create Strings as file list... AudioList '.wordlistDirectory$'/'sgc.pinyin$'.*
				.number_of_examples = Get number of strings
				if .number_of_examples > 0
					Randomize
					.sound$ = Get string... 1
					.soundFilePath$ = "'.wordlistDirectory$'/'.sound$'"
				endif
				Remove
			endif
		endif
		if fileReadable(.soundFilePath$) and config.useSoundExample
			.tmp = -1
			.tmp = nocheck Read from file... '.soundFilePath$'
			if .tmp != undefined and .tmp > 0
				call play_sound '.tmp'
				select .tmp
				Remove
			endif
		elsif config.synthesis$ <> "" and config.synthesis$ <> "_DISABLED_"
			call synthesize_sound "'sgc.pinyin$'" "'sgc.character$'"
		else
			call humToneContour 'sgc.pinyin$' 'config.register'
			call reset_viewport
		endif
	endif
	demoShow()
endproc

# Draw a tone contour
procedure draw_tone_contour
	select sgc.currentWordlist
	if sgc.currentWord > 0 and sgc.currentWord <= sgc.numberOfWords
		.sound$ = Get value... 'sgc.currentWord' Sound
		call readPinyin 'sgc.currentWord'
		sgc.pinyin$ = readPinyin.pinyin$
		sgc.character$ = readPinyin.character$
		call drawToneContour 'sgc.pinyin$' 'config.register'
		call reset_viewport
		
		if te.recordedPitch > 0
			call drawSourceToneContour te.recordedPitch
		endif
	endif

endproc

procedure recognizeTone
	select sgc.currentWordlist
	if sgc.currentWord > 0 and sgc.currentWord <= sgc.numberOfWords
		.strict = 0
		if index_regex(config.strict$, "[^0-9]") <= 0
			.strict = ('config.strict$' >= sgc.highestLevel)
		endif
			
		.sound$ = Get value... 'sgc.currentWord' Sound
		call readPinyin 'sgc.currentWord'
		sgc.pinyin$ = readPinyin.pinyin$
		sgc.character$ = readPinyin.character$
		call align_recordedSound 'sgc.pinyin$'
        call sgc_ToneProt 'recordedSound$' 'sgc.pinyin$' 'config.register' '.strict' 'config.language$'
        # sgc_ToneProt manipulates the sound given. Reconnect
        select Sound 'recordedSound$'
        sgc.recordedSound = selected(sgcwordlist.audio$)
		call reset_viewport
	endif
endproc

procedure write_feedback .table$
    select Table '.table$'
    .line1$ = Get value... 1 Text
    .line2$ = Get value... 2 Text
    .label$ = Get value... 3 Text

	# convert numbers to Pinyin if needed
	if not config.displayNumbers
		call numbers2pinyin '.line1$'
		.line1$ = numbers2pinyin.pinyin$
	endif

    .color$ = "Red"
    if index(.line1$, "???") > 0
        .color$ = "Black"
    elsif .label$ = "Correct"
        .color$ = "Green"
	elsif config.strict$ = "'sgc.highestLevel'"
		.line2$ = .line2$ + " *"
    endif

	.currentFeedbackFontSize = 14
	.maxHeight = 21 - 17
	call adjustFontSizeOnHeight 'defaultFont$' '.currentFeedbackFontSize' '.maxHeight'
	.currentFeedbackFontSize = adjustFontSizeOnHeight.newFontSize

   	call wipeArea 'wipeFeedbackArea$'
    call set_font_size '.currentFeedbackFontSize'
    demo '.color$'
    demo Text... 50 Centre 21 Bottom '.line1$'
    demo Text... 50 Centre 17 Bottom '.line2$'
	demoShow()
	demo 'defaultFont$'
    call set_font_size 'defaultFontSize'
endproc

procedure write_grade .pinyin$
	if sgc2.performanceTable > 0 and .pinyin$ <> ""
		select sgc2.performanceTable
		call getGrade '.pinyin$'
		if getGrade.grade > 0
			call get_evaluation_text 'config.language$' 'getGrade.grade'
			.currentGrade$ = get_evaluation_text.text$
		    .line1$ = .currentGrade$
		
		    .color$ = "Blue"		
			.currentFeedbackFontSize = 40
			.maxHeight = 2* (21 - 17 - 1)
			call adjustFontSizeOnHeight 'defaultFont$' '.currentFeedbackFontSize' '.maxHeight'
			.currentFeedbackFontSize = adjustFontSizeOnHeight.newFontSize
			
		   	call wipeArea 'wipeFeedbackArea$'
		    call set_font_size '.currentFeedbackFontSize'
		    demo '.color$'
		    demo Text... 50 Centre 17 Bottom '.line1$'
			demoShow()
			demo 'defaultFont$'
		    call set_font_size 'defaultFontSize'
	    endif
    endif
endproc

# Text display
procedure display_text .color$
	select sgc.currentWordlist
	if sgc.currentWord < 0 or sgc.currentWord > sgc.numberOfWords+1
		call clean_up_sound
        if config.shuffleLists
		    Randomize rows
        endif
		if sgc.currentWord < 0
			sgc.currentWord = sgc.numberOfWords
		else
			sgc.currentWord = 1
		endif
	endif
	
	.changeCJKstyle = 0
	if sgc.currentWord > 0 and sgc.currentWord <= sgc.numberOfWords
		.displayText$ = ""
		call readDisplayPinyin 'sgc.currentWord'
		.displayPinyin$ = readDisplayPinyin.pinyin$
		.displayChar$ = Get value... 'sgc.currentWord' Character
		.displayTrans$ = Get value... 'sgc.currentWord' Translation
		if .displayPinyin$ <> "-" and (config.displayPinyin or sgc.writeAll)
			if not config.displayNumbers
				call numbers2pinyin '.displayPinyin$'
				.displayPinyin$ = numbers2pinyin.pinyin$
			endif
			# Insert u umlaut
			.displayPinyin$ = replace_regex$(.displayPinyin$, "v", "\\u\X22", 0)
			.displayText$ = .displayText$ + .displayPinyin$
		endif
		if .displayChar$ <> "-" and (config.displayChar or sgc.writeAll)
			.displayText$ = .displayText$ + "  "+ .displayChar$
			.changeCJKstyle = 1
		endif
		if .displayTrans$ <> "-" and (config.displayTrans or sgc.writeAll)
			.displayText$ = .displayText$ + "  \s{%%"+ .displayTrans$ + "%}"
		endif
	elsif sgc.currentWord = 0 or sgc.currentWord = sgc.numberOfWords+1
		call clean_up_sound
		.displayText$ = "---"
	endif

	# Adapt font size
	call adjustFontSizeOnHeight 'defaultFont$' 24 15
	.currentFontSize = adjustFontSizeOnHeight.newFontSize
	call adjustFontSizeOnWidth 'defaultFont$' '.currentFontSize' 95 '.displayText$'
	.currentFontSize = adjustFontSizeOnWidth.newFontSize

	# Clear the writing area
	call wipeArea 'wipePinyinArea$'
	# Switch back to Chinese style CJK when in Japanese language mode
	if .changeCJKstyle and config.language$ = "JA"
		CJK font style preferences: "Chinese"
	endif
	# Actually display text
	demo '.color$'
	call set_font_size '.currentFontSize'
	demo Text... 50 Centre 26 Bottom '.displayText$'
	demoShow()
	demo Black
	demo 'defaultFont$'
	call set_font_size 'defaultFontSize'
	if .changeCJKstyle and config.language$ = "JA"
		if config.language$ = "JA"
			CJK font style preferences: "Japanese"
		elsif config.language$ = "ZH"
			CJK font style preferences: "Chinese"
		endif
	endif
	# Switch back to Japanese style CJK when in Japanese language mode
	if .changeCJKstyle and config.language$ = "JA"
		CJK font style preferences: "Japanese"
	endif
endproc

procedure numbers2pinyin .numberstext$
	.intermediatePinyin$ = .numberstext$
	# Add a `-quote between vowels
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([aeuiov])([0-9])([aeuiov])", "\1\2'\3", 0)
	# Move numbers to the nucleus vowel
	# To the vowel
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([aeuiov])([^aeuiov0-9]*)([0-9])", "\1\3\2", 0)
	# Either a/e
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([ae])([aeuiov]*)([0-9])", "\1\3\2", 0)
	# Or the Oo in /ou/
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(ou)([0-9])", "o\2u", 0)
	# or the second vowel
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([uiov][aeuiov])([uiov])([0-9])", "\1\3\2", 0)
	
	# Convert all tones to special characters
	# Tone 1
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "a1", "ā", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "e1", "ē", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "u1", "ū", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "i1", "ī", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "o1", "ō", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "v1", "ǖ", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "1", "\\-^", 0)
	
	# Tone 2
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "v2", "ǘ", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([iaeou])2", "\\\1'", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "2", "\\'^", 0)
	
	# Tone 3
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "a3", "ǎ", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "e3", "ě", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "u3", "ǔ", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "i3", "ǐ", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "o3", "ǒ", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "v3", "ǚ", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "3", "\\N^", 0)

	# Tone 4
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "v4", "ǜ", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([iaeou])4", "\\\1`", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "4", "\\`^", 0)
	
	# Tone 0
	# Remove tone 0 symbol completely
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "0", "", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "a0", "å", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "e0", "e̊", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "u0", "ů", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "i0", "i̊", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "o0", "o̊", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "v0", "ü̊", 0)
	#.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "0", "\\0^", 0)
	
	# Pick best vowel symbols available in cases not caught before
	# Ugly clutch to get the 1, 3, 0 tone diacritics at least in the neighbourhood
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "i(\\[-N0]\^)", "i\\s{_ }\1", 0)
	# Insert u umlaut
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "v", "\\u\X22", 0)

	.pinyin$ = .intermediatePinyin$
endproc

# NEEDS WORK AND TESTING!!
# Convert unicode Pinyin into tone numbers
procedure pinyin2numbers .pinyin$
	.intermediatePinyin$ = .pinyin$
	# Convert all special characters to numbers
	# Tone 1
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iā)([iaeouü]*)", "a\11", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iē)([iaeouü]*)", "e\11", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iū)([iaeouü]*)", "u\11", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iī)([iaeouü]*)", "i\11", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iō)([iaeouü]*)", "o\11", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iǖ)([iaeouü]*)", "v\11", 0)
	
	# Tone 2
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iá)([iaeouü]*)", "a\12", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?ié)([iaeouü]*)", "e\12", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iú)([iaeouü]*)", "u\12", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?ií)([iaeouü]*)", "i\12", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?ió)([iaeouü]*)", "o\12", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iǘ)([iaeouü]*)", "v\12", 0)
	
	# Tone 3
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iǎ)([iaeouü]*)", "a\13", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iě)([iaeouü]*)", "e\13", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iǔ)([iaeouü]*)", "u\13", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iǐ)([iaeouü]*)", "i\13", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iǒ)([iaeouü]*)", "o\13", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iǚ)([iaeouü]*)", "v\13", 0)

	# Tone 4
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?ià)([iaeouü]*)", "a\14", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iè)([iaeouü]*)", "e\14", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iù)([iaeouü]*)", "u\14", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iì)([iaeouü]*)", "i\14", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iò)([iaeouü]*)", "o\14", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iǜ)([iaeouü]*)", "v\14", 0)
	
	# Tone 0
	# Add tone 0
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iå)([iaeouü]*)", "a\10", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "e̊([iaeouü]*)", "e\10", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "(?iů)([iaeouü]*)", "u\10", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "i̊([iaeouü]*)", "i\10", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "o̊([iaeouü]*)", "o\10", 0)
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "ü̊([iaeouü]*)", "v\10", 0)

	# Syllables without a tone symbol are tone 0
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([aeuiov]+)([^0-9aeuiov]|\W|$)", "\10\2", 0)

	# Move numbers to the end of the syllable.
	# Syllables ending in n and start with g. Note that a syllable cannot start with an u or i
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([aeuiov])([0-9])(n)(g[aeuiov])", "\1\3\2\4", 0)
	# Syllables ending in (ng?) followed by something that is not a valid vowel 
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([aeuiov])([0-9])(ng?)([^aeuiov]|\W|$)", "\1\3\2\4", 0)
	# Syllables ending in r
	.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "([aeuiov])([0-9])(r)([^aeuiov]|\W|$)", "\1\3\2\4", 0)
	# Remove quotes etc
	#.intermediatePinyin$ = replace_regex$(.intermediatePinyin$, "[\\'\\`]", "", 0)
	
	.numberstext$ = .intermediatePinyin$
endproc

procedure readDisplayPinyin .currentWord
	select sgc.currentWordlist
	.pinyin$ = Get value... '.currentWord' Pinyin
	.character$ = Get value... '.currentWord' Character
	# Everything to lowercase

	if index_regex(.pinyin$, "[0-9]") <= 0
		call pinyin2numbers '.pinyin$'
		.pinyin$ = pinyin2numbers.numberstext$
	endif 
endproc

procedure readPinyin .currentWord
	call readDisplayPinyin '.currentWord'
	.character$ = readDisplayPinyin.character$
	.pinyin$ = replace_regex$(readDisplayPinyin.pinyin$, ".+", "\L&", 0)
	.pinyin$ = replace_regex$(.pinyin$, "[\\'\\` ]", "", 0)
	# Remove anything that is objectionable
	.pinyin$ = replace_regex$(.pinyin$, "[^a-zA-Z0-9_']", "", 0)
endproc

# Includes
include ToneProt/SGC_ToneProt.praat
include ToneProt/DrawToneContour.praat
include ToneProt/HumToneContour.praat
include ToneProt/ToneRecognition.praat
include ToneProt/ToneScript.praat
include ToneProt/ToneRules.praat
