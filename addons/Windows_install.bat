cd "%~dp0"
if EXIST SGC2\SpeakGoodChinese2.exe (
	if NOT EXIST "%HOMEDRIVE%%HOMEPATH%\Desktop\SpeakGoodChinese2.exe" (
		copy SGC2\SpeakGoodChinese2.exe "%HOMEDRIVE%%HOMEPATH%\Desktop\SpeakGoodChinese2.exe"
		if ERRORLEVEL 1 (
			msg "%username%" Installation of SpeakGoodChinese.exe encountered an error
		)
) else (
		msg "%username%" SpeakGoodChinese.exe already exists at destination
	)
)

mkdir "%HOMEDRIVE%%HOMEPATH%\SpeakGoodChinese2\wordlists"
xcopy /q /s /Y wordlists\* "%HOMEDRIVE%%HOMEPATH%\SpeakGoodChinese2\wordlists\"
if ERRORLEVEL 1 (
	msg "%username%" Install encountered an error 
) else (
	msg "%username%" Install completed
)
