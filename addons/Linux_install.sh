#!/bin/bash
# 
# Copy wordlists to SpeakGoodChinese preferences directory
ZENITY=`which zenity`
if [[ `/bin/uname` != "Darwin" ]] ; then 
	cd "`dirname "$0"`"
	MACHINE="32"
	if [[ `/bin/uname -m` =~ "64" ]]; then
		MACHINE="64"
	fi
	if [[ (-s ~/Desktop/SpeakGoodChinese2 || -s ~/SpeakGoodChinese2) && -n ZENITY ]]; then
		zenity --info --text="SpeakGoodChinese2 already exists at destination"
	elif [[ -s ./SGC2/SpeakGoodChinese2_linux${MACHINE}.zip ]]; then
		if [[ -d ~/Desktop ]]; then
			unzip ./SGC2/SpeakGoodChinese2_linux${MACHINE}.zip -d ~/Desktop/
		else
			unzip ./SGC2/SpeakGoodChinese2_linux${MACHINE}.zip -d ~/
		fi
		if [[ $? != 0 && -n ZENITY ]]; then
			zenity --info --text='Installation of SpeakGoodChinese2 encountered an error'
		fi		
	fi
	mkdir -p ~/.SpeakGoodChinese2/wordlists
	cp -r wordlists/* ~/.SpeakGoodChinese2/wordlists/
	if [[ $? != 0 && -n ZENITY ]]; then
		zenity --info --text='Installation of the wordlists encountered an error'
	elif [[ -n ZENITY ]]; then
		zenity --info --text='Installation completed'
	fi		
elif [[ -n ZENITY ]]; then
	zenity --info --text="THIS INSTALL SCRIPT IS ONLY FOR LINUX, NOT FOR MACINTOSH COMPUTERS (OSX)"
fi
exit 0
