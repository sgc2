Distribution of SpeakGoodChinese2 application and wordlists

Wordlists and/or SpeakGoodChinese binaries are installed when executing
(double click on) the approriate script file. 

Construct a ZIP file from a map containing:

Maps:

wordlists - Map with the wordlist maps
       Add any number of wordlist maps. A wordlist map is a map 
       with the name of the wordlist and either:
       wordlist.txt, wordlist.Table or wordlist.tsv
       Audio recordings corresponding to the pinyin phrases
       used in the wordlist. The audio filenames must follow the
       format: <pinyin>.<extension>
       For instance:
       ni3hao3.wav
       da4jia1hao3.spx

SGC2 - Map with SpeakGoodChinese binaries (optional)
	   Add any of the following:
       SGC2/SpeakGoodChinese2.app.zip         - Mac version
       SGC2/SpeakGoodChinese2.app.zip.md5     - MD5 checksum
       SGC2/SpeakGoodChinese2.exe             - Windows version
       SGC2/SpeakGoodChinese2.exe.md5         - MD5 checksum
       SGC2/SpeakGoodChinese2_linux32.zip     - Linux 32 bit version
       SGC2/SpeakGoodChinese2_linux32.zip.md5 - MD5 checksum
       SGC2/SpeakGoodChinese2_linux64.zip     - Linux 64 bit version
       SGC2/SpeakGoodChinese2_linux64.zip.md5 - MD5 checksum
 

Installation scripts:
Double click on the appropriate file

Linux_install.sh    - Linux installation script

Mac_install.command - Mac installation script

Windows_install.bat - Windows installation script
