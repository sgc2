#!/bin/bash
# 
# Copy wordlists to SpeakGoodChinese preferences directory
if [[ `uname` == "Darwin" ]] ; then 
	APPLICATIONPATH=`dirname "$0"`
	cd "${APPLICATIONPATH}"
	if [[ ${APPLICATIONPATH} =~ "Mac_install.app/Contents/Resources/Scripts" ]]; then
		cd ../../../../
	fi
	if [[ -d ~/Desktop/SpeakGoodChinese2_32.app ]]; then
		osascript -e 'tell app "Finder" to display dialog "SpeakGoodChinese2_32.app already exists at destination"'
	elif [[ -s ./SGC2/SpeakGoodChinese2_32.app.zip ]]; then
		unzip ./SGC2/SpeakGoodChinese2_32.app.zip -d ~/Desktop/
		if [[ $? != 0 ]]; then
			osascript -e 'tell app "Finder" to display dialog "Installation of SpeakGoodChinese2_32.app encountered an error"'
		fi
	fi
	mkdir -p ~/Library/Preferences/SpeakGoodChinese2/wordlists
	echo `pwd` >> ~/Desktop/kanweg.txt
	cp -r wordlists/* ~/Library/Preferences/SpeakGoodChinese2/wordlists/
	if [[ $? == 0 ]]; then
		osascript -e 'tell app "Finder" to display dialog "Installation completed"'
	else
		osascript -e 'tell app "Finder" to display dialog "Installation of wordlists encountered an error"'
	fi
else
	echo "THIS INSTALL SCRIPT IS ONLY FOR MACINTOSH COMPUTERS (OSX)"
	osascript -e 'tell app "Finder" to display dialog "THIS INSTALL SCRIPT IS ONLY FOR MACINTOSH COMPUTERS (OSX)"'
fi
osascript -e 'tell application "Terminal" to quit'
exit 0
