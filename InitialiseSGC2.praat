#
# SpeakGoodChinese 2.0
# 
# Intializing Praat script
#
#     SpeakGoodChinese: InitializeSGC2.praat defines and sets the global variables
#     and loads general SGC2 code 
#     
#     Copyright (C) 2007-2010  R.J.J.H. van Son and 2010 the Netherlands Cancer Institute
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Koning, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 

# Do not store these variables as preferences
sgc.nonPreferences$ = " config.savePerf config.openPerf config.clearSummary config.audioName "

# Generalizing the wordlist code
sgcwordlist.word$ = "Pinyin"
sgcwordlist.graph$ = "Character"
sgcwordlist.transl$ = "Translation"
sgcwordlist.audio$ = "Sound"
# Future use
sgcwordlist.ipa$ = "IPA"
sgcwordlist.gender$ = "Gender"
sgcwordlist.n$ = "Nt"
sgcwordlist.f0$ = "F0"
sgcwordlist.f1$ = "F1"
sgcwordlist.f2$ = "F2"
sgcwordlist.f3$ = "F3"
sgcwordlist.t$ = "t"	

# Information for logging activity
currentDate$ = date$()
dateYear$ = right$(currentDate$, length(currentDate$) - rindex(currentDate$, " "))
logtimeStamp$ = replace_regex$(currentDate$, "[^a-zA-Z0-9\-_]", "-", 0)
currentLogDirectory$ = ""
feedbackTablePrefix$ = "Feedback"
feedbackTableName$ = ""
evaluationTablePrefix$ = "Evaluation"
evaluationTableName$ = ""
performanceTableName$ = "Performance"

# Define canvas
viewportMargin = 5
yWordlist = 11
yFeedback = yWordlist + 5
yPinyin = yFeedback + 10
yContour = yPinyin + 15
wipeContourArea$ = "demo Paint rectangle... White 20 80 'yContour' 100"
wipePinyinArea$ = "demo Paint rectangle... White 20 80 'yPinyin' 'yContour'"
wipeFeedbackArea$ = "demo Paint rectangle... White 0 100 'yFeedback' 'yPinyin'"
wipeWordlistArea$ = "demo Paint rectangle... White 20 80 'yWordlist' 'yFeedback'"

# Pop-Up window colors
sgc2.popUp_bordercolor$ = "{0.5,0.5,1}"
sgc2.popUp_backgroundcolor$ = "{0.9,0.9,1}"

# Initialize parameters
alertText$ = ""
sgc.currentWord = 1
sgc.currentWordlist = -1
sgc.numberOfWords = 0
sgc.currentWordNum = 1
sgc.numberOfDisplayedWords = 0
sgc.recordedSound = 0
sgc.saveAudioOn = 0
sgc.writeAll = 0
sgc.customLanguage$ = ""
pinyin$ = ""
sgc.pinyin$ = ""
character$ = ""
buttons$ = ""
config$ = ""
wordlistNum = 1
wordlistName$ = ""
wordlist$ = ""
sgc.failedAttempts = 0
mainPage.play = -1
config.deleteWordlist = -1
config.displayNumbers = 1
config.displayPinyin = 1
config.displayChar = 0
config.displayTrans = 1
config.useSoundExample = 1
config.synthesis$ = "_DISABLED_"
config.input$ = "Microphone"
config.showBackground = 1
sgc2.noiseThresshold = -30
config.strict$ = "1"
sgc.highestLevel = 3
config.voice = 4
config.voice$ = "f4"
config.voicesTTS$ = "default f1 f2 f3 f4 f5 m1 m2 m3 m4 m5 m6 m7"
sgc.lastVoice = 12
sgc.nativeOSXTTScommand$ = "say -vTing-Ting  --rate=95"
sgc.nativeLinuxTTScommand$ = "spd-say -l zh -t female1 -r -60"
#sgc.nativeWindowsTTScommand$ = "mshta.exe vbscript:Execute(""CreateObject(""""SAPI.SpVoice"""").Speak """"<voice gender='female' xml:lang='zh-CN'><rate speed='-5'>$$$</rate></voice>"""":Close"")"
sgc.nativeWindowsTTScommand$ = "mshta.exe vbscript:Execute(""CreateObject(""""SAPI.SpVoice"""").Speak """"<voice gender='female' xml:lang='zh-CN'><rate speed='-5'><pron sym='@@@'/></rate></voice>"""":Close"")"
sgc.nativeTTScommand$ = ""
config.savePerf = 0
config.openPerf = 0
config.clearSummary = 0
config.audioName$ = ""
sgc.savePerf$ = ""
sgc.saveAudio$ = ""
mainPage.saveAudio$ = ""
sgc.allWordLists = -1

sgc_ToneProt.minimumPitch = 40
sgc_ToneProt.maximumPitch = 600

# Platform dependent settings
if macintosh or windows
	config.displayChar = 1
	config.displayNumbers = 0
endif
if windows and endsWith(build_SHA$, " XP")
	config.displayChar = 0
	config.displayNumbers = 0
endif
if unix
	config.displayChar = 0
	config.displayNumbers = 1
endif

if unix
	samplingFrequency = 44100
elsif macintosh
	samplingFrequency = 44100
elsif windows
	samplingFrequency = 44100
endif
recordingTime = 2.5

# Be careful: Logging and Replaying can have side-effects
# sgc2.logging$ = "call saveLogOfActivity"
sgc2.logging$ = "# "
# When this is not "# ", the command 'replaySGC2Log$' will be run!
if not variableExists("replaySGC2Log$")
	replaySGC2Log$ = "# "
endif

config.language$ = "EN"
config.register = 249
preferencesAppFile$ = preferencesAppDir$+"/sgc2rc.txt"
preferencesLogDir$ = "'preferencesAppDir$'/log"
preferencesTableDir$ = "'preferencesAppDir$'/GUI"
sgc2wordlists$ = "'preferencesAppDir$'/wordlists"
localWordlistDir$ = sgc2wordlists$

# Global word lists must be installed BY THE ADMINISTRATOR
# This means, create the directory 'globalwordlists$' and
# fill it with wordlist directories containing wordlist.Table
# and audio files. They can be copied from a local directory.
globalwordlists$ = ""
globaltablelists$ = ""
globalTTSdir$ = ""
if unix
	globalwordlists$ = "/etc/'sgc2.demoAppName$'/wordlists"
	globaltablelists$ = "/etc/'sgc2.demoAppName$'/GUI"	
	globalTTSdir$ = "/etc/'sgc2.demoAppName$'/TTS"	
elsif macintosh
	globalwordlists$ = "/Library/Preferences/'sgc2.demoAppName$'/wordlists"
	globaltablelists$ = "/Library/Preferences/'sgc2.demoAppName$'/GUI"
	globalTTSdir$ = "/Library/Preferences/'sgc2.demoAppName$'/TTS"	
elsif windows
	globalwordlists$ = "C:/Documents and Settings/All Users/Application Data/'sgc2.demoAppName$'/wordlists"
	globaltablelists$ = "C:/Documents and Settings/All Users/Application Data/'sgc2.demoAppName$'/GUI"
	globalTTSdir$ = "C:/Documents and Settings/All Users/Application Data/'sgc2.demoAppName$'/TTS"
endif
sgc2.synthesizer = -1
sgc.alignedTextGrid = -1

##############################################################

config.shuffleLists = 1
config.adaptiveLists = 0
defaultFontSize = 12
defaultFont$ = "Helvetica"
defaultLineWidth = 1

# Set up directories if they do not exist already
call set_up_directories

# Get saved preferences
call read_preferences ""

# Set up TTS system
speakCommandDir$ = "'preferencesAppDir$'/TTS"
speakCommandFile$ = ""
# Define a Praat TTS command that will set sgc2.synthesizer
call set_TTS_parameters
call set_up_TTS
sgc.recordCommandFile$ = ""
sgc.playCommandFile$ = ""
if windows
	sgc.scratchAudioDir$ = "'preferencesAppDir$'\audio\"
else
	sgc.scratchAudioDir$ = "/tmp/SpeakGoodChinese/"
endif
sgc.useAlternativeRecorder = 0
call set_up_recorder
sgc.useAlternativePlayer = 0
call set_up_player

# Set inital language
call set_language 'config.language$'

# Get the word-list
call load_word_list "'localWordlistDir$'" 0

# Set up evaluation table
initialiseSGC2.toneevaluation_table$ = ""
call initialize_toneevaluation_tables

# Draw inital window
call init_window

###############################################################
#
# Obligatory procedures
#
###############################################################
# Initialize Demo Window
procedure init_window
    demo Erase all
	demo Line width... 'defaultLineWidth'
	demo 'defaultFont$'
	call set_font_size 'defaultFontSize'
	demo Black
	
	# If there is no recorded sound, try to read stored recording
	if recordedSound$ = "" and sgc.saveAudio$ <> ""
		sgc.pinyin$ = ""
		select sgc.currentWordlist
		if sgc.currentWord > 0 and sgc.currentWord <= sgc.numberOfWords
			call readPinyin 'sgc.currentWord'
			sgc.pinyin$ = readPinyin.pinyin$
			sgc.character$ = readPinyin.character$
			.outputName$ = "'sgc.saveAudio$'/'sgc.pinyin$'.wav"
			if fileReadable(.outputName$)
				sgc.recordedSound = Read from file: .outputName$
				recordedSound$ = selected$(sgcwordlist.audio$)
				call recognizeTone
				# See feedback on earlier recorded words
				if fileReadable("'sgc.saveAudio$'/TestSpeakGoodChinese2.txt")
					call write_feedback Feedback
				else
					call write_grade 'sgc.pinyin$'
				endif
				select Table Feedback
				Remove
				
				# Set config.audioName
				.tmp$ = replace_regex$(sgc.saveAudio$, "[^/\\]+[/\\]?$", "", 0)
				config.audioName$ = replace$(sgc.saveAudio$, .tmp$, "", 0)
				
			endif
		endif
	endif
	
	# Set mainPage.saveAudio$ grade
	call set_grade_display 'sgc.pinyin$'
	call write_grade 'sgc.pinyin$'
	
	# Update screen
	call reset_viewport
	.windowTitle$ = replace$(wordlistName$, "_", " ", 0)
	call set_window_title 'buttons$' '.windowTitle$'
	# Display the word-list
	call write_word_list
    # Wipe screen and draw background
    call wipeArea 'wipeContourArea$'
 	if config.showBackground
		call draw_background Background
	endif
    # Define buttons in a table
    call init_buttons
    # Set Play button
	call Set_Play_Button
    
	# Draw the contour
	call draw_tone_contour
	# SaveAudio light
  	call paint_saveAudio_light
  	# Set play button
  	
  	sgc.failedAttempts = 0
endproc

# Make sure all Preferences directories are available
procedure set_up_directories
	.dirPath$ = "'preferencesAppDir$'"
	createDirectory(.dirPath$)
	.dirPath$ > '.dirPath$'/directory.txt
	createDirectory("'.dirPath$'/wordlists")
	.dirPath$ > '.dirPath$'/wordlists/directory.txt
	createDirectory("'.dirPath$'/pitchmodels")
	.dirPath$ > '.dirPath$'/pitchmodels/directory.txt
	createDirectory("'.dirPath$'/log")
	.dirPath$ > '.dirPath$'/log/directory.txt
	createDirectory("'.dirPath$'/TTS")
	.dirPath$ > '.dirPath$'/TTS/directory.txt
endproc

# 
# Retrieve and store setting between sessions
#
procedure read_preferences .preferencesFile$
	if not fileReadable(.preferencesFile$)
		.preferencesFile$ = preferencesAppFile$
	endif
	if fileReadable(.preferencesFile$)
		Read from file... 'preferencesAppFile$'
		.preferenceTable$ = selected$("Table")
		.numPrefKeys = Get number of rows
		for .row to .numPrefKeys
			.variableName$ = Get value... '.row' Key
			if variableExists(.variableName$)
				.variableValue = Get value... '.row' Value
				if .variableValue <> undefined
					'.variableName$' = '.variableValue'
				endif
			elsif variableExists(.variableName$+"$")
				.variableValue$ = Get value... '.row' Value
				.variableName$ = .variableName$+"$"
				'.variableName$' = "'.variableValue$'"
			endif
		endfor
		
		select Table '.preferenceTable$'
		Remove
	endif
endproc

procedure write_preferences .preferencesFile$
	Create Table with column names... Preferences 0 Key Value
	if index_regex(.preferencesFile$, "[a-zA-Z0-9]") <= 0
		.preferencesFile$ = preferencesAppFile$
	endif
	.row = 0
	
	for .tableNum from 1 to 2
		if .tableNum = 1
			.table$ = "Config"
		elsif .tableNum = 2
			.table$ = "MainPage"
		endif
		.varPrefix$ = replace_regex$(.table$, "^(.)", "\l\1", 0)

		select Table '.table$'
		.numTableRows = Get number of rows
		for .tablerow to .numTableRows
			select Table '.table$'
			.label$ = Get value... '.tablerow' Label
			.variableName$ = .varPrefix$+"."+(replace_regex$(.label$, "^(.)", "\l\1", 0))
			.keyName$ = .variableName$
			.value$ = ""
			if sgc.nonPreferences$ <> "" and index(sgc.nonPreferences$, " '.variableName$' ")
				.value$ = ""
			elsif variableExists(.variableName$)
				.value = '.variableName$'
				.value$ = "'.value'"
			elsif variableExists(.variableName$ + "$")
				.variableName$ = .variableName$ + "$"
				.value$ = '.variableName$'
			elsif index(.variableName$, "_") > 0
				.variableName$ = left$(.variableName$, rindex(.variableName$, "_") - 1)	
				.keyName$ = .variableName$
				select Table Preferences
				.prefRow = Search column... Key '.keyName$'
				if .prefRow <= 0
					if variableExists(.variableName$)
					   .value = '.variableName$'
					   .value$ = "'.value'"
					elsif variableExists(.variableName$ + "$")
					   .variableName$ = .variableName$ + "$"
					   .value$ = '.variableName$'
					endif
				endif
			endif

			if .value$ <> ""
				select Table Preferences
				Append row
				.row += 1
				Set string value... '.row' Key '.keyName$'
				Set string value... '.row' Value '.value$'
			endif
		endfor
	endfor
	
	# Some extra settings
	select Table Preferences
	Append row
	.row += 1
	Set string value... '.row' Key wordlistDir
	Set string value... '.row' Value 'localWordlistDir$'

	select Table Preferences
	Append row
	.row += 1
	Set string value... '.row' Key wordlistName
	Set string value... '.row' Value 'wordlistName$'

	select Table Preferences
	Write to table file... '.preferencesFile$'
	Remove

endproc

###############################################################
#
# TTS (speech synthesis). Place commands in user info
# 
# Prerequisites
# speakCommandDir$ = "'preferencesAppDir$'/TTS"
# speakCommandFile$ = ""

# Make sure sgc2.synthesizer is set!
procedure create_default_TTS .language$ .voice$ .samplefreq .gap .pitch_adj .pitch_range .words_per_minute .use_data$ .espeak_phones$
	sgc2.synthesizer = Create SpeechSynthesizer: .language$, .voice$
	Set speech output settings... .samplefreq .gap .pitch_adj .pitch_range .words_per_minute '.use_data$' '.espeak_phones$'
endproc

procedure set_up_TTS
	if sgc2.synthesizer > 0
		select sgc2.synthesizer
		Remove
		sgc2.synthesizer = -1
	endif
	# Note, the TTScommand must set sgc2.synthesizer!
	if config.strict$ = "'sgc.highestLevel'"
		'sgc2.advancedTTScommand$'
	else
		'sgc2.normalTTScommand$'
	endif

	if macintosh
		.ttscommand$ = "speak"
		.osName$ = "OSX"
		.quote$ = ""
	elsif unix
		.ttscommand$ = "espeak"
		.osName$ = "UNIX"
		.quote$ = ""
	elsif windows
		.ttscommand$ = "C:/Program Files/eSpeak/command_line/espeak.exe"
		.osName$ = "WIN"
		.quote$ = """"
	endif
	# Global TTS command has precedence
	if fileReadable("'globalTTSdir$'/TTS_'.osName$'_command.txt")
		speakCommandFile$ = "'globalTTSdir$'/TTS_'.osName$'_command.txt"
		if config.synthesis$ = "_DISABLED_"
			# Default is ON
			config.synthesis$ = "eSpeak"
		endif
	endif

	# Local TTS command, if there is no global one
	# Command does not exist yet, create it if eSpeak is installed
	# Make sure to quote the path!
	if speakCommandFile$ = ""
		# Autodetect synthesizer command
#		if not fileReadable("'speakCommandDir$'/TTS_'.osName$'_eSpeak_command.txt")
#			.command_path$ = ""
#			if macintosh
#				system_nocheck  PATH=${PATH}:/opt/local/bin; bash -c -- 'which '.ttscommand$' > "'speakCommandDir$'/command_path.txt"'
#				.command_path$ < 'speakCommandDir$'/command_path.txt
#			elsif unix
#				system_nocheck  PATH=${PATH};bash -c -- 'which '.ttscommand$' > "'speakCommandDir$'/command_path.txt"'
#				.command_path$ < 'speakCommandDir$'/command_path.txt
#			elsif windows
#				if fileReadable(.ttscommand$)
#					.command_path$ = .ttscommand$
#				endif
#			endif
#			# Remove any newlines
#			.command_path$ = replace$(.command_path$, "'newline$'", "", 0)
#
#			# Command path found
#			if sgc2.synthesizer <= 0 and .command_path$ <> "" and fileReadable(.command_path$)
#				deleteFile("'speakCommandDir$'/TTS_'.osName$'_eSpeak_command.txt")
#				fileappend "'speakCommandDir$'/TTS_'.osName$'_eSpeak_command.txt"  
#				...'.quote$''.ttscommand$''.quote$' -v zh+f4 -s 100 'newline$'
#				speakCommandFile$ = "'speakCommandDir$'/TTS_'.osName$'_eSpeak_command.txt"
#			endif
#		endif
		if fileReadable("'speakCommandDir$'/TTS_'.osName$'_eSpeak_command.txt")
			speakCommandFile$ = "'speakCommandDir$'/TTS_'.osName$'_eSpeak_command.txt"
		endif
		deleteFile("'speakCommandDir$'/command_path.txt")
	endif
	
	# Close off (other TTS maybe a later option)
	if sgc2.synthesizer > 0 or (speakCommandFile$ <> "" and fileReadable(speakCommandFile$))
		config.synthesis$ = "eSpeak"
	endif
endproc

procedure set_TTS_parameters
	if config.voice > 0
		.voice$ = replace_regex$(config.voicesTTS$, "^(\S+\s+){'config.voice'}(\S+)(\s+.*)?$", "\2", 0)
	else
		.voice$ = replace_regex$(config.voicesTTS$, "^(\S+)(\s+.*)?$", "\1", 0)
	endif
	if .voice$ <> "" and index_regex(.voice$, "\s") <= 0
		if .voice$ = "default"
			config.voice$ = "*"
		else
			config.voice$ = replace_regex$(.voice$, ".", "\u&", 0)
		endif
		sgc2.normalTTScommand$ = "call create_default_TTS ""Chinese (Mandarin)"" '.voice$' 10000 0.01 50 100 95 no IPA"
		sgc2.advancedTTScommand$ = "call create_default_TTS ""Chinese (Mandarin)"" '.voice$' 10000 0.01 50 70 125 no IPA"
	endif
	if macintosh and fileReadable("/usr/bin/say")
		sgc.nativeTTScommand$ = sgc.nativeOSXTTScommand$
	elsif unix and fileReadable("/usr/bin/spd-say")
		sgc.nativeTTScommand$ = sgc.nativeLinuxTTScommand$
	elsif windows
		sgc.nativeTTScommand$ = sgc.nativeWindowsTTScommand$
	endif
endproc

# Alternative audio recorder
procedure set_up_recorder
	if macintosh
		.osName$ = "OSX"
		.quote$ = ""
	elsif unix
		.osName$ = "UNIX"
		.quote$ = ""
	elsif windows
		.osName$ = "WIN"
		.quote$ = """"
	endif
	# Global TTS command has precedence
	sgc.useAlternativeRecorder = 0
	if fileReadable("'globalTTSdir$'/TTS_'.osName$'_record_command.txt")
		createDirectory(sgc.scratchAudioDir$)
		sgc.recordCommandFile$ = "'globalTTSdir$'/TTS_'.osName$'_record_command.txt"
		sgc.useAlternativeRecorder = 1
	elsif fileReadable("'speakCommandDir$'/TTS_'.osName$'_record_command.txt")
		createDirectory(sgc.scratchAudioDir$)
		sgc.recordCommandFile$ = "'speakCommandDir$'/TTS_'.osName$'_record_command.txt"
		sgc.useAlternativeRecorder = 1
	endif
endproc

# Alternative audio player
procedure set_up_player
	if macintosh
		.osName$ = "OSX"
		.quote$ = ""
	elsif unix
		.osName$ = "UNIX"
		.quote$ = ""
	elsif windows
		.osName$ = "WIN"
		.quote$ = """"
	endif
	# Global TTS command has precedence
	sgc.useAlternativePlayer = 0
	if fileReadable("'globalTTSdir$'/TTS_'.osName$'_play_command.txt")
		createDirectory(sgc.scratchAudioDir$)
		sgc.playCommandFile$ = "'globalTTSdir$'/TTS_'.osName$'_play_command.txt"
		sgc.useAlternativePlayer = 1
	elsif fileReadable("'speakCommandDir$'/TTS_'.osName$'_play_command.txt")
		createDirectory(sgc.scratchAudioDir$)
		sgc.playCommandFile$ = "'speakCommandDir$'/TTS_'.osName$'_play_command.txt"
		sgc.useAlternativePlayer = 1
	endif
endproc

###############################################################
#
# Evaluation tables
#
# Prerequisites
# initialiseSGC2.toneevaluation_table$ = ""
procedure initialize_toneevaluation_tables
	.invalidperformanceTable = 0
	.oldPerformanceTable = -1
	# Mark old table for removal
	if initialiseSGC2.toneevaluation_table$ != ""
		.oldPerformanceTable = sgc2.performanceTable
		sgc2.performanceTable = -1
	endif
	initialiseSGC2.toneevaluation_table$ = ""
	# Get the name of the table
	call get_evaluation_table_labels 'config.language$'
	performanceTableName$ = eval.performance$
	
	if sgc.savePerf$ <> "" and fileReadable(sgc.savePerf$)
		sgc2.performanceTable = Read Table from tab-separated file: sgc.savePerf$
		Rename: performanceTableName$
		.columnNameList$ = "'eval.pinyin$' 'eval.grade$' 'eval.correct$' 'eval.wrong$' 'eval.total$' 'eval.high$' 'eval.low$' 'eval.wide$' 'eval.narrow$' 'eval.unknown$' 'eval.commented$' 'eval.level$' 'eval.time$' 'eval.wordlist$'"
		.numPerfColumns = 14
		# Check to make sure all columns are initialized, but only if Pinyin exists!
		select sgc2.performanceTable
		.numColumns = Get number of columns
		# Find Pinyin column on 1
		if index("Pinyin 拼音 ピンイン", eval.pinyin$) > 0
			.column = Get column index: sgcwordlist.word$
			if .column <= 0
				.column = Get column index: "拼音"
			endif
			if .column <= 0
				.column = Get column index: "ピンイン"
			endif
		else
			.column = Get column index: eval.pinyin$
		endif
		# This is not a valid Performance table, remove it
		if .column != 1 or .numColumns != .numPerfColumns
			select sgc2.performanceTable
			Remove
			
			# Set ERROR and remove file read
			.invalidperformanceTable = 1
			sgc2.performanceTable = .oldPerformanceTable
			.oldPerformanceTable = -1
		endif

		# ALWAYS make sure the column headers are translated!
		select sgc2.performanceTable
		.numColumns = Get number of columns
		.nameList$ = .columnNameList$
		for .col to .numColumns
			.nameList$ = replace_regex$(.nameList$, "^\s+", "", 1)
			.columnHeader$ = extractWord$(.nameList$, "")
			.nameList$ = replace_regex$(.nameList$, "^\s*'.columnHeader$'\s*", "", 1)
			select sgc2.performanceTable
			Set column label (index): .col, .columnHeader$
		endfor

		# Performance table has been read. Set the wordlist to the first row
		select sgc2.performanceTable
		.numWords = Get number of rows
		.row = 1
		wordlistName$ = ""
		.wordlistLabel$ = eval.wordlist$
		.col = Get column index: .wordlistLabel$
		if .col <= 0
			.wordlistLabel$ = "Wordlist"
			.col = Get column index: .wordlistLabel$
		endif
		while .col > 0 and .row <= .numWords and (wordlistName$ = "" or index_regex(wordlistName$, "[^\?\- ]") <= 0)
			wordlistName$ = Get value: .row, .wordlistLabel$
			.row += 1
		endwhile
		
		call load_word_list: localWordlistDir$, 0
		
		# If this is a graded list, look for the audio
		if config.audioName$ = ""
			# Check if there is a grade
			select sgc2.performanceTable
			.grade$ = Get value: get_toneevaluation_row.row, eval.grade$
			# It is graded! Look for the audio
			if index_regex(.grade$, "[\d]") > 0
				.noAudio = 1
				select sgc2.performanceTable
				.numRows = Get number of rows
				for .row to .numRows
					select sgc2.performanceTable
					.pinyin$ = Get value: .row, eval.pinyin$
					.strings = Create Strings as file list: "Files", "'sgc.saveAudio$'/'.pinyin$'.*"
					.numAudio = Get number of strings
					Remove
					if .numAudio > 0
						.noAudio = 0
					endif
				endfor
				
				if .noAudio > 0
					.fileName$ = right$(sgc.savePerf$, length(sgc.savePerf$) - index_regex(sgc.savePerf$, "[/\\][^/\\]+$"))
					.fileName$ = replace_regex$(.fileName$, "(?i\.tsv$)", "", 0)
					call findLabel Config $AudioName
					.row = findLabel.row
					select Table Config
					.openDialogue$ = Get value... '.row' Helptext
					call convert_praat_to_latin1 '.openDialogue$'
					.openDialogue$ = convert_praat_to_latin1.text$
					.defaultName$= ""
					sgc.saveAudio$ = chooseDirectory$ (.fileName$+": "+.openDialogue$)
					
				endif
			endif
		endif
	else
		# Create new table
		sgc2.performanceTable = Create Table with column names... "'performanceTableName$'" 0 'eval.pinyin$' 'eval.grade$' 'eval.correct$' 'eval.wrong$' 'eval.total$' 'eval.high$' 'eval.low$' 'eval.wide$' 'eval.narrow$' 'eval.unknown$' 'eval.commented$' 'eval.level$' 'eval.time$' 'eval.wordlist$'
	endif
	initialiseSGC2.toneevaluation_table$ = performanceTableName$
	call get_toneevaluation_row 'eval.total$'
	
	# Remove marked old table
	if .oldPerformanceTable > 0
		select .oldPerformanceTable
		Remove
		.oldPerformanceTable = -1
	endif
endproc

procedure get_toneevaluation_row .pinyin$
	select sgc2.performanceTable
	.colName$ = Get column label: 1
	# Catch and rename Totals. Make sure it works when you stay within a new language
	if index("Total Totaal 总计 合計", .pinyin$) > 0
		.row = Search column: .colName$, "Total"
		if .row < 1
			.row = Search column: .colName$, "Totaal"
		endif
		if .row < 1
			.row = Search column: .colName$, "总计"
		endif
		if .row < 1
			.row = Search column: .colName$, "合計"
		endif
		if .row > 0
			Set string value: .row,  .colName$, .pinyin$
		endif
	else
		.row = Search column: .colName$ , .pinyin$
	endif
	if .row < 1
		Append row
		.row = Get number of rows
		Set string value: .row, eval.pinyin$, .pinyin$
		Set numeric value... '.row' 'eval.correct$' 0
		Set numeric value... '.row' 'eval.wrong$' 0
		Set numeric value... '.row' 'eval.total$' 0
		Set numeric value... '.row' 'eval.high$' 0
		Set numeric value... '.row' 'eval.low$' 0
		Set numeric value... '.row' 'eval.wide$' 0
		Set numeric value... '.row' 'eval.narrow$' 0
		Set numeric value... '.row' 'eval.unknown$' 0
		Set numeric value... '.row' 'eval.commented$' 0
		Set numeric value... '.row' 'eval.level$' 0
		Set numeric value... '.row' 'eval.time$' 0
		Set string value: .row, eval.wordlist$, wordlistName$
		Sort rows... 'eval.pinyin$'
    	.row = Search column: eval.pinyin$, .pinyin$
	endif
endproc

procedure increment_toneevaluation_in_row .row .column$
	select sgc2.performanceTable
	.tmp = Get value... '.row' '.column$'
	if .tmp = undefined
		.tmp = 0
	endif
	.tmp += 1
	Set numeric value... '.row' '.column$' '.tmp'
	
	# Set Level
	Set string value: .row, eval.level$, config.strict$
	
	# Set time stamp and wordlist
	.dateTime$ = date$()
	Set string value: .row, eval.time$, .dateTime$
	Set string value: .row, eval.wordlist$, wordlistName$
	
endproc

# Update existing performance file with toneevaluation
procedure update_toneevaluation_file
	if sgc.savePerf$ <> "" and fileReadable(sgc.savePerf$) and initialiseSGC2.toneevaluation_table$ <> ""
		select sgc2.performanceTable
		Write to table file... 'sgc.savePerf$'
	endif
endproc

procedure increment_toneevaluation_value .pinyin$ .column$
	call get_toneevaluation_row '.pinyin$'
	.row = get_toneevaluation_row.row
	call increment_toneevaluation_in_row '.row' '.column$'
	
	call get_toneevaluation_row 'eval.total$'
	.row = get_toneevaluation_row.row
	call increment_toneevaluation_in_row '.row' '.column$'
endproc

procedure add_feedback_to_toneevaluation .table$
    select Table '.table$'
    .line1$ = Get value... 1 Text
    .line2$ = Get value... 2 Text
    .label$ = Get value... 3 Text
	
	.tones$ = replace_regex$(left$(.line1$, rindex(.line1$, ":")), "[^0-9]", "", 0)
	.recognized$ = replace_regex$(right$(.line1$, length(.line1$)-rindex(.line1$, ":")), "[^0-9]", "", 0)
	
	# Set evaluation
	.result = 0
	call increment_toneevaluation_value 'sgc.pinyin$' 'eval.total$'
	if .tones$ = .recognized$
		call increment_toneevaluation_value 'sgc.pinyin$' 'eval.correct$'
		.result = 1
	else
		call increment_toneevaluation_value 'sgc.pinyin$' 'eval.wrong$'
	endif
	if index_regex(.label$, "(Correct|Wrong)") <= 0
		call increment_toneevaluation_value 'sgc.pinyin$' 'eval.commented$'
	endif
	
	if .label$ = "6"
		call increment_toneevaluation_value 'sgc.pinyin$' 'eval.unknown$'
	endif
	if .label$ = "Low"
		call increment_toneevaluation_value 'sgc.pinyin$' 'eval.low$'
	endif
	if .label$ = "High"
		call increment_toneevaluation_value 'sgc.pinyin$' 'eval.high$'
	endif
	if .label$ = "Narrow"
		call increment_toneevaluation_value 'sgc.pinyin$' 'eval.narrow$'
	endif
	if .label$ = "Wide"
		call increment_toneevaluation_value 'sgc.pinyin$' 'eval.wide$'
	endif
	
	
	# Update performance table when SaveAudio is on
	if sgc.saveAudioOn
		call update_toneevaluation_file
	endif
endproc

procedure setGrade .grade$
	select sgc.currentWordlist
	if recordedSound$ <> "" and sgc.pinyin$ <> ""		
		call get_toneevaluation_row 'sgc.pinyin$'
		.row = get_toneevaluation_row.row
		select sgc2.performanceTable
		.col = Get column index: eval.grade$
		if .col <= 0
			Insert column: 2, eval.grade$
		endif
		.gradeNum = '.grade$'
		if .gradeNum = 0
			.gradeNum = 10
		endif

		Set numeric value: .row, eval.grade$, .gradeNum
		.numRows = Get number of rows
		.sumGrade = 0
		.sumN = 0
		for .r from 2 to .numRows
			.tmp = Get value: .r, eval.grade$
			if .tmp <> undefined
				.sumGrade += .tmp
				.sumN += 1
			endif
		endfor
		.meanGrade = -1
		if .sumN > 0
			.meanGrade = .sumGrade / .sumN
		endif
		
		call get_evaluation_text '.language$' Total
		.totalTxt$ = get_evaluation_text.text$
		call get_toneevaluation_row '.totalTxt$'
		.row = get_toneevaluation_row.row
		Set numeric value: .row , eval.grade$, .meanGrade
		
		# Store current performance table
		call update_toneevaluation_file
	endif
endproc

procedure getGrade .pinyin$
	.grade = -1
	if .pinyin$ <> ""
		call get_toneevaluation_row '.pinyin$'
		.row = get_toneevaluation_row.row
		
		select sgc2.performanceTable
		.gradeLabel$ = eval.grade$
		.col = Get column index: eval.grade$
		if .col <= 0
			.col = Get column index: "Grade"
			.gradeLabel$ = "Grade"
		endif
		if .col <= 0
			Insert column... 2 Grade
		else
			.grade = Get value: .row, .gradeLabel$
			if .grade = undefined
				.grade = -1
			endif	
		endif
	endif
endproc

# Set mainPage.saveAudio$ to Grade
procedure set_grade_display .pinyin$
	mainPage.saveAudio$ = ""
	select sgc2.performanceTable
	.grade$ = ""
	.col = Get column index: eval.grade$
	if .pinyin$ <> "" and .col > 0
		call get_toneevaluation_row 'eval.total$'
		.row = get_toneevaluation_row.row
		if .row > 0
			.grade$ = Get value: .row, eval.grade$
			if index_regex(.grade$, "[0-9]") <= 0
				.grade$ = ""
			endif
		endif
	endif
	if .grade$ <> ""
		.gradeNum = '.grade$'
		mainPage.saveAudio$ = fixed$(.gradeNum, 1)
	endif
endproc
	
###############################################################
#
# Miscelaneous procedures
#
###############################################################

# TTS synthesis
procedure synthesize_sound .pinyin$ .character$
	.pinyin$ = replace_regex$(.pinyin$, "(^""|""$)", "", 0)
	.character$ = replace_regex$(.character$, "(^""|""$)", "", 0)
	.pinyinText$ = replace_regex$(.pinyin$, "0", "5", 0)
	.pinyinExpanded$ = replace_regex$(.pinyin$, "([0-5])", " \1 - ", 0)
	.pinyinExpanded$ = replace_regex$(.pinyinExpanded$, " - $", "", 0)
	
	if .character$ = "-" or .character$ = "" or .character$ = "?"
		.character$ = .pinyinText$
	endif
	.currentCommand$ = ""
	# Files need more checks!
	if speakCommandFile$ <> "" and fileReadable(speakCommandFile$)
		.currentCommand$ < 'speakCommandFile$'
		.currentCommand$ = replace$(.currentCommand$, "'newline$'", " ", 0)
		if index(.currentCommand$, "$$$") and .character$ <> .pinyinText$
			# Find the character representation
			.currentCommand$ = replace$(.currentCommand$, "$$$", .character$, 0)
		elsif index(.currentCommand$, "&&&")
			# Find the character representation
			.currentCommand$ = replace$(.currentCommand$, "&&&", .pinyinText$, 0)
		elsif index(sgc.nativeTTScommand$, "@@@")
			.currentCommand$ = replace$(sgc.nativeTTScommand$, "@@@", .pinyinExpanded$, 0)
		else
			.currentCommand$ = .currentCommand$ + " ""'.pinyinText$'"""
		endif
		if macintosh
			system_nocheck PATH=${PATH}:/usr/local/bin:/opt/local/bin; bash -rc -- ''.currentCommand$''
		elsif unix
			system_nocheck bash -rc -- ''.currentCommand$''
		elsif windows
			runSystem_nocheck: .currentCommand$
		endif
	elsif sgc.nativeTTScommand$ <> "" and config.voice <= 0
		# Get the pinyin text on the correct place
		if index(sgc.nativeTTScommand$, "$$$")
			.currentCommand$ = replace$(sgc.nativeTTScommand$, "$$$", .character$, 0)
		elsif index(sgc.nativeTTScommand$, "&&&")
			.currentCommand$ = replace$(sgc.nativeTTScommand$, "&&&", .pinyinText$, 0)
		elsif index(sgc.nativeTTScommand$, "@@@")
			.currentCommand$ = replace$(sgc.nativeTTScommand$, "@@@", .pinyinExpanded$, 0)
		else
			.currentCommand$ = sgc.nativeTTScommand$ + " ""'.pinyinText$'"""
		endif
		runSystem_nocheck: .currentCommand$
	elsif sgc2.synthesizer > 0
		select sgc2.synthesizer
		Play text... '.pinyin$'
	endif
endproc

procedure align_recordedSound .pinyin$
	if 0 and sgc2.synthesizer > 0 and recordedSound$ <> ""
		select Sound 'recordedSound$'
		.recordedTextGrid = To TextGrid... pinyin
		Set interval text... 1 1 '.pinyin$'
		select sgc2.synthesizer
		plus Sound 'recordedSound$'
		plus .recordedTextGrid
		sgc.alignedTextGrid = nowarn noprogress To TextGrid (align,trim)... 1 1 1 -35 0.1 0.1 0.08
		Rename... AlignedTextGrid
		select .recordedTextGrid
		Remove
	endif
endproc
