#
# SpeakGoodChinese 2.0
# 
# Script to replay stored (log) files with Praat commands.
# Use:
# Read into Praat (with all the other scripts in the same directory) and Run
# You will be asked to select a log file to execute
# 
# This is a debugging option that will lead to security issues when not
# used with care!
#
#     SpeakGoodChinese: ReplaySGC2.praat is a script to debug, test, or 
#     demonstrate SpeakGoodChinese2
#     It is written in Praat script for the Demo window 
#     
#     Copyright (C) 2007-2010  R.J.J.H. van Son and 2010 the Netherlands Cancer Institute
#     The SpeakGoodChinese team are:
#     Guangqin Chen, Zhonyan Chen, Stefan de Koning, Eveline van Hagen, 
#     Rob van Son, Dennis Vierkant, David Weenink
# 
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 

# Insert a pause between commands. Pause in seconds
sgc.replaySleep = 0.5
# The replay function
replaySGC2Log$ = "call replaySGC2LogFunction"

# Load the SGC script
include sgc2.praat
